# SECA -- Symbolic ECA

If you want to check out the repository, you have to recurse submodules.

    $ git clone --recurse-submodules git@gitlab.science.ru.nl:mklinik/eca-symbolic-execution.git


## Contents

- code/app: Contains the main program.
- code/src: Contains the source code of the compiler, the analysis itself, the
  merge algorithm, and the graph generator.
- code/test: Contains unit tests; mostly interesting for developers
- code/programs: Contains various test programs to demonstrate the analyzer


## Compiling SECA

To build the code, use haskell stack.
https://docs.haskellstack.org/
When running this command for the first time, it will take a long time to compile all dependencies.

The build needs the development version of cairo installed. On debian, install with:

    $ apt install libcairo2-dev

First make sure that you have the latest version of stack and cabal installed.

    $ stack update && stack upgrade
    $ stack install Cabal
    $ which stack
    /home/mkl/.local/bin/stack
    $ which cabal
    /home/mkl/.local/bin/cabal

Then you can build SECA

    $ cd code
    $ stack build


## Running SECA

To run eca, you need z3 in your `$PATH`.
For build instructions see https://github.com/Z3Prover/z3/wiki

    $ export PATH=$PATH:/home/mkl/radboud/src/symbolic-execution/z3/build/
    $ stack run -- --help

Concrete execution

    $ stack run -- -c programs/bug.eca

Symbolic execution

    $ stack run -- -s programs/bug.eca

The generated skylines can be found in the subdirectory `out`.

Export JSON

    $ stack run -- -s programs/bug.eca --export-json foo.json



## Random Notes

This is the command I use to generate diagrams for the paper

    $ stack run -- --width 200 --height 300 --no-title --latex -s programs/paper/bug.eca
    $ ./svg2tex.sh out/bug-symbolic.svg

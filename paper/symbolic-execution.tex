\section{Energy-Aware Symbolic Execution}
\label{sec:symbolicExecution}

The energy-aware symbolic execution semantics tracks path constraints and energy skylines for each execution path.
The result is a set of skylines for each function together with their path constraints.


In the symbolic semantics it is undesirable for component calls to perform I/O, because exploring all execution paths causes component calls to be executed multiple times.
The symbolic semantics therefore uses component models where component calls return \emph{symbolic values} with constraints.
Symbolic values \textrm{SVal} are syntax trees whose leaves are constants or \emph{symbolic inputs} $\id{any}_i$ (variables that stand for an arbitrary integer).
\textrm{SVal} is given by the grammar:
\begin{IEEEeqnarray*}{c}
\id{sv} ::= \seca{true} \mid \seca{false} \mid i \mid \id{any}_i \mid \id{sv}\mathbin{\id{op}}\id{sv} \mid \id{un}\ \id{sv}
\end{IEEEeqnarray*}

A \emph{symbolic component model} $\tuple{S, L, \delta, o}$ consists of a set of states $S$, a finite set of labels $L$, a transition function $\delta : L \times S \to \textrm{SVal} \times \textrm{SVal} \times S$, and a power draw function $o : S \to \N$.
As opposed to the concrete models in \cref{sec:semantics}, $\delta$ can not perform I/O.
The first returned $\textrm{SVal}$ of $\delta$ is typically a constant or symbolic input, and the second is a constraint on that input.
For example, where the concrete model of \seca{TERM.readInt()} asks for input and returns the user's answer, the symbolic model returns a fresh symbolic input $\id{any}_j$, with the constraint \seca{true}, as this input can be any integer.
A temperature sensor in a cold room can return a fresh $\id{any}_j$, together with a constraint $13 \leq \id{any}_j \wedge \id{any}_j \leq 17$.
A symbolic LED returns constant $0$, with the constraint \seca{true}.

\subsection{The Energy-Aware Symbolic Execution Semantics}

We now study the algorithm that computes all possible executions of SECA programs, together with their corresponding skylines.
The algorithm records skylines of each function separately.
This results in one skyline for each function call, for each execution path on which the call lies.
The algorithm is defined by case distinction on the abstract syntax.
We present the whole algorithm in \cref{fig:step,fig:eval}, but provide a detailed description only for a few clauses%, to give an impression of how the algorithm works
.
A complete description of the algorithm, as well as a formal definition of the semantics in the style common in programming language research, can be found in the technical report \cite{KlinikGKE2020TechReport}.
An implementation is available online \cite{SecaSourceCodeRepository}.


\begin{figure}%
\newcommand{\myindent}{\enskip}%
\begin{minipage}[t]{0.49\textwidth}%
\setlength{\abovedisplayskip}{0pt}%
\setlength{\belowdisplayskip}{0pt}%
\begin{IEEEeqnarray*}{l}%
   \id{E} : \textbf{Expr} \times \Sigma \to \mathcal{P}(\textbf{Val} \times \Sigma)
\\ \id{E}\sembrackets{x}(\sigma) = \set{ \tuple{lookup(x, \sigma), \sigma} } \IEEEyesnumber\label{fig:eval:var}
\\ \id{E}\sembrackets{e_1 \mathbin{\id{op}} e_2}(\sigma) = \set{ \tuple{v_1 \mathbin{\id{op}} v_2, \sigma''} \IEEEyesnumber\label{fig:eval:binop}
\\ \myindent \mid \tuple{v_1, \sigma'} \in \id{E}\sembrackets{e_1}(\sigma)
\\ \myindent , \tuple{v_2, \sigma''} \in \id{E}\sembrackets{e_2}(\sigma') }
\\ \id{E}\sembrackets{\id{un}\ e}(\sigma) = \set{ \tuple{\id{un}\ v, \sigma'} \IEEEyesnumber\label{fig:eval:unop}
\\ \myindent \mid \tuple{v, \sigma'} \in \id{E}\sembrackets{e}(\sigma) }
\\ \id{E}\sembrackets{f(\many{e})}(\sigma) = \IEEEyesnumber\label{fig:eval:app}
\\ \myindent \set{ \tuple{lookup(\#return, \sigma'''), \sigma'''[\id{pc} \mapsto \sigma.\id{pc}]}
\\ \myindent \mid \tuple{\many{v}, \sigma'} \in \many{\id{E}}\sembrackets{\many{e}}(\sigma)
\\ \myindent , \sigma'' = \id{call}\sembrackets{f(\many{v})}(\sigma'[\id{pc}\mapsto []])
\\ \myindent , \sigma''' \in \id{X}(\sigma'') }
\\ \id{E}\sembrackets{c.f()}(\sigma) = \set{ \tuple{v, \sigma'} } \IEEEyesnumber\label{fig:eval:compcall}
\\ \myindent \textrm{where}
\\ \myindent \sigma' = \sigma[\id{cstate} \mapsto \id{cstate}', \id{sky} \mapsto \id{sky}'
\\ \myindent \myindent , \varphi \mapsto \varphi']
\\ \myindent \tuple{v, \psi, s_c'} = \delta_c(f, s_c)
\\ \myindent \id{cstate}' = \sigma.\id{cstate}[c \mapsto s_c']
\\ \myindent s_c = \sigma.\id{cstate}[c]
\\ \myindent p = \id{powerDraw}(\id{cstate}')
\\ \myindent l = \id{lineOfCompCall}
\\ \myindent \id{sky}' = \sigma.\id{sky} \mdoubleplus [F(l), E(p)]
\\ \myindent \varphi' = \sigma.\varphi \wedge \psi
\end{IEEEeqnarray*}%

\begin{IEEEeqnarray*}{l}
   \id{call} : \textbf{Expr} \times \Sigma \to \Sigma
\\ \id{call}\sembrackets{f(\many{v})}(\sigma) = \sigma' \IEEEyesnumber\label{fig:eval:call}
\\ \myindent \textrm{where}
\\ \myindent \sigma' = \sigma[\id{env} \mapsto \id{env}', \id{sky} \mapsto \id{sky}', \id{pc} \mapsto \many{s}
\\ \myindent \myindent , \id{stack} \mapsto \id{stack}']
\\ \myindent \id{env}' = [ \many{x} \mapsto \many{v} ]
\\ \myindent p = \id{powerDraw}(\sigma.\id{cstate})
\\ \myindent l = \id{lineOfOpeningBrace}
\\ \myindent \id{sky}' = [S(l,p)]
\\ \myindent \many{s} = \id{functionBody}\sembrackets{f}
\\ \myindent \id{stack}' = \id{push}(\sigma, \id{stack})
\end{IEEEeqnarray*}%

\begin{IEEEeqnarray*}{l}
   \id{X} : \Sigma \to \mathcal{P}(\Sigma)
\\ \id{X}(\sigma) =
\left\{ \begin{IEEEeqnarraybox*}[][c]{l}
   \set{\sigma} \quad \textrm{if } \sigma.\id{pc} = []
\\ \bigcup\set{ \id{X}(\sigma')
\\ \myindent \mid \sigma' \in \id{S}\sembrackets{s}(\sigma[\id{pc}\mapsto\id{rest}])}
\\ \myindent \myindent \textrm{if } \sigma.\id{pc} = [s] \mdoubleplus \id{rest}
\end{IEEEeqnarraybox*}\right.
\end{IEEEeqnarray*}%
\caption{The function \emph{E} for expressions
and \emph{X} to execute whole programs}
\label{fig:eval}
\end{minipage}
\begin{minipage}[t]{0.50\textwidth}
\setlength{\abovedisplayskip}{0pt}%
\setlength{\belowdisplayskip}{0pt}%
\setcounter{equation}{0}
\begin{IEEEeqnarray*}{l}
   \id{S} : \textbf{Stmt} \times \Sigma \to \mathcal{P}(\Sigma)
\\ \id{S}\sembrackets{x = e}(\sigma) = \set{\id{assign(x,v,\sigma')} \IEEEyesnumber\label{fig:step:asgn}
\\ \myindent \mid \tuple{v,\sigma'}\kern-0.8pt\in\kern-0.8pt\id{E}\sembrackets{e}(\sigma[sky\kern-0.8pt\mapsto\kern-0.8pt\sigma.sky \mdoubleplus [F(l)]])
\\ \myindent \textrm{where}\ l = \id{lineNumberOfAssignment} }
%
\\ \id{S}\sembrackets{\seca{if}(e)\,\{\,\many{s}_1\,\}\,\seca{else}\,\{\,\many{s}_2\,\}}(\sigma) = \bigcup \set{ \IEEEyesnumber\label{fig:step:cond}
\\ \myindent \set{\sigma'[\id{pc} \mapsto \id{pc}_1, \varphi \mapsto \varphi_1]
\\ \myindent , \sigma'[\id{pc} \mapsto \id{pc}_2, \varphi \mapsto \varphi_2]}
\\ \myindent \mid \tuple{v, \sigma'}\kern-0.5pt\in\kern-0.5pt\id{E}\sembrackets{e}(\sigma[\id{sky}\kern-0.5pt\mapsto\kern-0.5pt\sigma.\id{sky} \mdoubleplus [F(l)]])
\\ \myindent \textrm{where}
\\ \myindent l = \id{lineOfIfKeyword}
\\ \myindent \varphi_1 = \sigma'.\varphi \wedge v
\\ \myindent \varphi_2 = \sigma'.\varphi \wedge \neg v
\\ \myindent \id{pc}_1 = \many{s}_1 \mdoubleplus \sigma'.\id{pc}
\\ \myindent \id{pc}_2 = \many{s}_2 \mdoubleplus \sigma'.\id{pc} }
%
\\ \id{S}\sembrackets{\seca{while}(e)\,\{\,\many{s}\,\}\,i}(\sigma) = \bigcup \set{ \IEEEyesnumber\label{fig:step:while}
\\ \myindent \set{\sigma'[\id{pc} \mapsto \id{loop}, \varphi \mapsto \varphi_1], \sigma'[\varphi \mapsto \varphi_2]}
\\ \myindent \mid \tuple{v, \sigma'} \in \id{E}\sembrackets{e}(\sigma[\id{sky} \mapsto \id{sky}'])
\\ \myindent \textrm{where}
\\ \myindent \varphi_1 = \sigma'.\varphi \wedge v
\\ \myindent \varphi_2 = \sigma'.\varphi \wedge \neg v
\\ \myindent \id{sky}' = \left\{ \begin{IEEEeqnarraybox*}[][c]{.l?s}
   \sigma.\id{sky} \mdoubleplus [F(l)]        & if $i = 0$
\\ \sigma.\id{sky} \mdoubleplus [F(m), J(l)]  & otherwise
\end{IEEEeqnarraybox*}\right.
\\ \myindent \id{loop} = \many{s} \mdoubleplus [\seca{while}(e)\{\many{s}\}(i+1)] \mdoubleplus \sigma'.\id{pc}
\\ \myindent l = \id{lineOfWhileKeyword}
\\ \myindent m = \id{lineOfClosingBrace} }
%
\\ \id{S}\sembrackets{\seca{return}\ e}(\sigma) = \IEEEyesnumber\label{fig:step:return}
\\ \myindent \set{ \id{registerSkyline}(f, \id{sky}'', \sigma'')
\\ \myindent \mid \tuple{v, \sigma'} \in \id{E}\sembrackets{e}(\sigma[\id{sky} \mapsto \id{sky}'])
\\ \myindent \textrm{where}
\\ \myindent \id{sky}' = \sigma.\id{sky} \mdoubleplus [F(l)]
\\ \myindent \id{sky}'' = \sigma'.\id{sky} \mdoubleplus [F(m)]
\\ \myindent \id{sky}_c' = \sigma_c.\id{sky} \mdoubleplus [F(n), E(p)]
\\ \myindent \tuple{\sigma_c, \id{stack}_c} = \id{pop}(\sigma.\id{stack})
\\ \myindent \sigma'' = \sigma'[\id{env} \mapsto \sigma_c.\id{env}[\#return \mapsto v]
\\ \myindent \myindent , \id{pc}\kern-0.5pt\mapsto\kern-0.5pt\sigma_c.\id{pc}, \id{sky}\kern-0.5pt\mapsto\kern-0.5pt\id{sky}_c', \id{stack}\kern-0.5pt\mapsto\kern-0.5pt\id{stack}_c ]
\\ \myindent l = \id{lineOfReturnKeyword}
\\ \myindent m = \id{lineOfClosingBrace}
\\ \myindent n = \id{lineOfCallSite}
\\ \myindent p = \id{powerDraw}(\sigma') }
\\ \id{S}\sembrackets{e}(\sigma) = \set{ \sigma' \mid \tuple{v, \sigma'} \in \id{E}\sembrackets{e}(\sigma) } \IEEEyesnumber\label{fig:step:expr}
\end{IEEEeqnarray*}
\caption{The function \emph{S} for statements}
\label{fig:step}
\end{minipage}
\end{figure}

\Cref{fig:eval,fig:step} show pseudocode for the functions \emph{E} and \emph{S} that compute symbolic skylines for expressions and statements respectively.
We elaborate on some of the clauses below.
Application of a function to syntactic arguments is denoted with double brackets $\sembrackets{-}$, which have no further special meaning.
%
A program state $\sigma \in \Sigma$ is a record with all information needed to execute a statement.
It contains the values of local program variables \emph{env} and global program variables \emph{genv}, the current skyline \emph{sky}, the current path constraint $\varphi$, the program counter \emph{pc} (a list of statements to be executed after the current statement), the function call stack \emph{stack}, and the CState \emph{cstate}.
The helper functions \emph{lookup} and \emph{assign} (not shown here) ensure that the scoping rules are respected, which means they prefer variables in \emph{env} over \emph{genv}.

Each clause of the semantics specifies how a single statement or expression together with a given program state produces the set of all possible immediate successor program states.
Hence, a statement can be seen as a state transformer $\Sigma \to \mathcal{P}(\Sigma)$.
To compose two functions of this type, we need glue code that applies the second function to every result of the first function. %, and takes the union of the resulting set of sets.
This is implemented by the function \emph{X} in \cref{fig:eval}, which executes whole programs.

\newcommand{\refLine}[1]{\textbf{(\ref{#1})}}

\subsection{Evaluation of Expressions}

The evaluation function \emph{E} (\cref{fig:eval}) takes an expression $e$ and a program state and returns the set of all possible values that $e$ can evaluate to, together with the updated program states.
Clauses \refLine{fig:eval:var} and \refLine{fig:eval:unop} are not explained here.

Clause \refLine{fig:eval:binop}: To evaluate a binary operator, all possible values $v_1$ for $e_1$, and all possible values for $e_2$ are calculated.
The evaluation of $e_2$ happens in the result state of the evaluation of $e_1$.
The result is the set of the symbolic values $v_1 \mathbin{\id{op}} v_2$ for all combinations $(v_1,v_2)$.
These values are subject to constant folding (e.g. $1 + 2$ becomes $3$), which is not shown here.

Clause \refLine{fig:eval:app}: To evaluate a function call, first all arguments are evaluated.
This is done by the sequential extension $\many{\id{E}}$, which chains the state through the evaluation of the argument vector $\many{e}$ and results in the set of all possible value vectors $\many{v}$.
For each vector $\many{v}$, the helper function $\id{call}$, described below, prepares the function call, and $\id{X}$ executes it.
This execution will eventually end with a return statement.
The return statement restores the program state so that $\sigma'''$ can be used as the result state at the call site.
The resulting value of the function call is the value of the \#return register in $\sigma'''$.

Clause \refLine{fig:eval:compcall}: To evaluate a component call, first the transition function $\delta_c$ of the component $c$ is invoked, with the function name $f$ and the component's current state $s_c$ as arguments.
This yields a return value $v$, a constraint $\psi$ on $v$, and a new component state $s_c'$.
%In the new CState, component $c$ is now in state $s_c'$.
The total power draw $p$ after the call is computed.
The skyline is extended with a forward segment $F(l)$ to the location $l$ of the call site, followed by an edge $E(p)$ to the new power draw.
The result of evaluating $c.f()$ is the return value $v$ of $\delta_c$, together with the updated program state.

Clause \refLine{fig:eval:call}: The helper function \emph{call} prepares the program state for execution of the function.
It first initializes the environment $\id{env}'$ for the function body with the actual arguments.
It then starts a new skyline for the call to $f$ with the current power draw $p$ at the location $l$ of the opening brace of the function definition of $f$.
It uses the function body $\many{s}$ as program counter, and creates a new stack frame for the function call.


\subsection{Execution of Statements}

The function \emph{X} (\cref{fig:eval}) recursively executes all statements of the program counter $\sigma.\id{pc}$, and collects the results.
Execution of a SECA program starts in a program state that contains the body of the \seca{main} function as program counter.

The function \emph{S} (\cref{fig:step}) executes a single statement in a given program state, and returns all possible successor program states.

Clause \refLine{fig:step:asgn}: Assignments are executed by first extending the current skyline to the line of the assignment.
Then $e$ is evaluated to all its possible values, and the final results are all successor states where $x$ has value $v$.
Expressions can have side effects, so the successor states may have different skylines.

Clause \refLine{fig:step:cond}: Execution of conditionals starts with extending the current skyline with a forward segment $F(l)$ to the line $l$ of the \emph{if} keyword.
Then, all possible values $v$ of the condition $e$ are computed.
This results in paths into both branches, for each $v$.
The path constraint for the \emph{then} branch is extended with $v$, for the \emph{else} branch with $\neg v$.
The program counter $\id{pc}_1$ specifies that first the statements $\many{s}_1$ of the \emph{then} branch are executed, and after that the original continuation $\sigma'.\id{pc}$.
Similarly for the \emph{else} branch.
If the SMT solver sees that $\varphi_1$ or $\varphi_2$ is unsatisfiable, their states are pruned (not shown here).



Clause \refLine{fig:step:while}:
For the first iteration of while loops, we need to generate a different skyline than for subsequent iterations.
The first loop iteration can be recognized by the loop counter $i$ being 0.
If this is the case, the current skyline comes from outside the loop body, and we extend it with a forward segment $F(l)$ to the line of the \emph{while} keyword.
Otherwise, the current skyline comes from inside the loop body, and is extended with a forward segment $F(m)$ to the line $m$ of the closing brace, followed by a backwards jump $J(l)$ to the beginning of the loop.
In both cases, the condition $e$ is evaluated to all possible values $v$.
For every $v$, we generate two continuations: one for entering the loop with path constraint $\sigma'\kern-3pt.\varphi \wedge v$ and one for exiting the loop with constraint $\sigma'\kern-3pt.\varphi \wedge \neg v$.
The program counter $\id{loop}$ for entering the loop consists of the loop body $\many{s}$, followed by the loop itself with incremented loop counter, then by what comes after the loop $\sigma'\kern-3pt.\id{pc}$.
The program counter for exiting %the loop
needs no change, as $\sigma'\kern-3pt.\id{pc}$ already contains the instructions following the loop.
Our implementation uses the loop counter to bound the number of %loop
iterations.
This is not shown here.

Clause \refLine{fig:step:return}: The clause for return statements is more complicated than the others, as it has to deal with two different skylines: the one from the function that is about to return, and the one from the caller.
Let $f$ be the name of the current function.
To execute a return statement, the current skyline is first extended with $F(l)$ to the location $l$ of the return keyword.
Then, the returned expression is evaluated.
Next the skyline is extended with $F(m)$, to the location $m$ of the closing brace of the function body.
Then, the program state from before the function call is restored, but updated with all the changes made by $f$.
For this, the topmost element $\sigma_c$ of the call stack is removed; this is the program state of the caller.
A new state $\sigma''$ is constructed, which the caller should use to resume execution;
$\sigma''$ has the caller's original $\id{env}$, but with the \#return register holding the return value.
The program counter and call stack are restored to the ones from before the call.
The caller's skyline $\sigma_c.\id{sky}$ is extended with a forward line $F(n)$ to the call site, and an edge $E(p)$ to the power draw $p$.
Finally, the skyline of the function call is recorded in the list of all skylines of $f$.
This is done with the function \emph{registerSkyline}, which stores the given skyline in the given state, and returns the thus updated state.

\section{Merging Skylines}
\label{sec:merging}

The skylines of a program often have many identical parts.
Take for example the program in \cref{fig:commonInitialSegment}.
Most of its execution paths have identical energy behaviour.
To merge skylines, we use a three-phase algorithm: \emph{preparation}, \emph{merging} and \emph{finalization}.
It is executed independently for every function.

\begingroup
%\setlength{\textfloatsep}{0.0pt}
%\captionsetup{farskip=0pt}%
\begin{figure}[tb]
\begin{minipage}{0.37\textwidth}
\lstinputlisting[language=SECA,numbers=left,xleftmargin=2em,basicstyle=\small]{programs/nested-loops.eca}
\end{minipage}%
\begin{minipage}{0.315\textwidth}%
  \includegraphics[width=1\textwidth]{nested-loops-main-no-merge.pdf}%
\end{minipage}%
\begin{minipage}{0.315\textwidth}%
  \includegraphics[width=1\textwidth]{nested-loops-main-yes-merge.pdf}%
\end{minipage}
\caption{A program with many execution paths, and its unmerged and merged skylines.}
\label{fig:commonInitialSegment}
\end{figure}
\endgroup


\begin{wrapfigure}{r}{0.5\textwidth}
%\setlength{\belowcaptionskip}{-2\baselineskip}
\begin{algorithmic}
\Procedure{Colourize}{$\id{frags}$}
  \For{$i \gets 1$ to $\id{Nfrags}$}
      \If{$(i > 1) \wedge (i \in \id{frags}[i-1].\id{conts}) \wedge {}$ \\
        \INDENT\INDENT $(\id{frags}[i-1].\id{end} = \id{frags}[i].\id{start})$}
        \State $\id{colour}[i] \gets \id{colour}[i-1]$
      \Else
        \State $\id{colour}[i] \gets$ a fresh colour
      \EndIf
  \EndFor
\EndProcedure
\end{algorithmic}
\caption{Assigning colours
%\mkl{make sure this figure is placed directly under \cref{fig:commonInitialSegment}}
}
\label{alg:visualization}
\end{wrapfigure}
\paragraph{Preparation}
First all skylines are split into \emph{fragments} and stored in an array, giving each a unique index.
Fragments represent single horizontal or vertical lines with explicit start and end points.
Every fragment has a set of continuations: indexes of the fragments that follow it.
Merging deletes explicit jumps $J(l)$: they are kept implicitly as fragments whose start point does not coincide with the end point of their predecessor.
Initially each fragment has at most one continuation, but more may be added later.
Preparation is shown in \cref{alg:preparation}.


\paragraph{Merging}
Whenever two fragments indexed $i$ and $j$ are equal, we can merge them by first combining their continuations, and then replacing all occurrences of $j$ in continuations of other fragments by $i$.
This is formally described by \cref{alg:merging}.

\paragraph{Visualization}
Finally, fragments are grouped into skylines, by assigning the same colour to directly connected fragments.
\Cref{alg:visualization} implements this.
It then assigns a small diagonal offset to each colour group (not shown here),
to avoid drawing lines on top of each other.

\begingroup
%\setlength{\textfloatsep}{0.0pt}
%\setlength{\belowcaptionskip}{-10pt}
\begin{figure}[t]
\begin{minipage}{0.5\textwidth}
\begin{algorithmic}
\Procedure{Prepare}{$\id{skies}(f)$}
\State \Comment{input: all skylines of function $f$}
\State \Comment{output: $\id{frags}$, an array of fragments,}
\State \Comment{each with at most one continuation}
  \State $\id{Nfrags} \gets 0$
  \ForAll{$\id{Skyline}\ \id{sky} \in \id{skies}(f)$}
    \State \Comment{all skylines begin with S(l,p)}
    \State let $\tuple{l,p}$ be such that $\id{sky}[1] = S(l,p)$
    \For{$i \gets 2$ to $\id{length}(\id{sky})$}
      \State $\id{sky}$[i] is either $F(l')$ or $J(l')$ or $E(p')$
      \State in the first two cases, let $p' = p$
      \State in the last case, let $l' = l$
      \If{$\id{sky}$[i] is $F(l')$ or $E(p')$}
        \State $\id{Nfrags} \gets \id{Nfrags} + 1$
        \State $\id{frags}$[$\id{Nfrags}$].$\id{start} \gets \tuple{l,p}$
        \State $\id{frags}$[$\id{Nfrags}$].$\id{end} \gets \tuple{l',p'}$
        \State $\id{frags}$[$\id{Nfrags}-1$].$\id{conts} \gets \set{\id{Nfrags}}$
      \EndIf
      \State $\tuple{l,p} \gets \tuple{l',p'}$
    \EndFor
    \State \Comment{last fragment has no continuation}
    \State $\id{frags}$[$\id{Nfrags}$].$\id{conts} \gets \emptyset$
  \EndFor
\EndProcedure
\end{algorithmic}
\caption{Initializing the $\id{frags}$ array}
\label{alg:preparation}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\begin{algorithmic}
\Procedure{Merge}{$\id{frags}, \id{Nfrags}$}
  \State \Comment{$\id{frags}$, $\id{Nfrags}$ as produced by prepare}
  \State \Comment{output: modified $\id{frags}$ with equal}
  \State \Comment{fragments merged}
  \For{$i \gets 1$ to $\id{Nfrags} - 1$}
    \State \textbf{if} $\id{frags}[i]$ = null \textbf{then} continue
    \For{$j \gets i + 1$ to $\id{Nfrags}$}
      \State \textbf{if} $\id{frags}[j]$ = null \\
      \INDENT\INDENT\INDENT or $\id{frags}[i].\id{start} \neq \id{frags}[j].\id{start}$\\
      \INDENT\INDENT\INDENT or $\id{frags}[i].\id{end} \neq \id{frags}[j].\id{end}$
        \State \INDENT \textbf{then} continue
      \State $\id{frags}[i].\id{conts} \gets$ \\
      \INDENT\INDENT\INDENT\INDENT\INDENT $\id{frags}[i].\id{conts} \cup \id{frags}[j].\id{conts}$
      \State $\id{frags}[j] \gets null$
      \For{$k \gets 1$ to $\id{Nfrags}$}
        \If{$\id{frags}[k] \neq null \wedge {}$ \\
        \INDENT\INDENT\INDENT\INDENT\INDENT\INDENT $j \in \id{frags}[k].\id{conts}$}
          \State $\id{frags}[k].\id{conts} \gets$ \\
          \INDENT\INDENT\INDENT\INDENT\INDENT\INDENT\INDENT $(\id{frags}[k].\id{conts} \setminus \set{j}) \cup \set{i}$
        \EndIf
      \EndFor
    \EndFor
  \EndFor
\EndProcedure
\end{algorithmic}
\caption{Merging fragments}
\label{alg:merging}
\end{minipage}
\end{figure}
\endgroup

\paragraph{Statement markers}
Between two consecutive horizontal lines, a $+$ indicates that a statement was executed at that point.
Continuations are drawn as coloured bullets or circles: if $j \in \id{frags}[i].\id{conts}$ and $\id{colour}[i] \neq \id{colour}[j]$, then
if $\id{frags}[i].\id{end} = \id{frags}[j].\id{start}$ then a bullet in $\id{colour}[j]$ is drawn at the end of fragment $i$.
Otherwise, the continuation is a jump backwards; this is indicated by drawing an open circle in $\id{colour}[j]$ at the end of fragment $i$.
Dotted lines in the diagram indicate the beginning and end points of loops.

\section{The SECA Language}
\label{sec:ecaLanguage}

SECA (Symbolic Energy Consumption Analysis) is a small imperative programming language.
We designed SECA to look like a simple form of C, without features like raw memory access and pointer arithmetic.
Such features complicate the analysis and are not the focus of this paper.
We believe that with some engineering effort, the analysis can be extended to support the style of C programs common in embedded and safety-critical systems.
%
SECA is a variant of ECA \cite{GastelKE2015}, which is itself a variant of Nielson's \textbf{While} \cite{NielsonN1992}.
SECA programs can control external hardware through \emph{component calls}.
For example, the component call \seca{LED1.switchOn()} invokes the switchOn functionality of the component \seca{LED1}.
Component calls can perform I/O and have return values, but no arguments.
While it would be simple to allow arguments to the component calls for the \emph{concrete} component models, it would require signifcant changes to the \emph{symbolic} component models.  This would complicate the symbolic execution.
For this paper we decided to keep this can of worms closed.


\paragraph{Assumptions}
We provide no typing rules, but do require that programs are well-typed in the usual sense.
We assume that all code paths of a function end in a return statement of the correct type, no references to undefined variables occur, and programs run on devices with all occurring hardware components.
Void functions are allowed to end without return statements.
In this case, the execution engine inserts an implicit return statement.



\subsection{Syntax}

A program is a list of function- and global variable declarations.
There must be one function \seca{main}.
The abstract syntax of SECA is shown in \cref{fig:secaSyntax}.
Overlined symbols stand for lists of that symbol; for example $\many{s}$ is a list of statements.

Expressions are Boolean or integer constants, program variables, applications of binary or unary operators, function calls, and component calls.
Operators are the usual Boolean connectives, comparisons and arithmetic.
Function calls have a list of expressions, the parameters.
Component calls have the form \emph{name}.\emph{function}() and invoke the specified function of the specified component.
Statements are conditionals, while loops, assignments, returns, or expressions.

While loops are annotated with a loop counter $i$, which the semantics uses to limit loop iterations, and to draw skylines differently in the first loop iteration.
This is further discussed in \cref{sec:semantics}.
The loop counter is not part of the concrete syntax, the programmer cannot access it, and it is initialised with zero.
Assignments have a variable on the left hand side and an expression on the right hand side.
Return statements end the current function call, and yield the given expression as the function's return value.

\begingroup
\begin{figure}[tb]
\begin{minipage}{0.5\textwidth}
\center
\begin{IEEEeqnarraybox*}{rCl}
e & ::= & \seca{true} \mid \seca{false} \mid i \mid x \mid e\ \id{op}\ e
\\ & & \mid \id{un}\ e \mid \id{id}( \many{e} ) \mid \id{id} . \id{id}()
%
\\ \id{op} & ::= & \seca{\&\&} \mid \seca{\|\|} \mid \seca{<=} \mid \seca{<} \mid + \mid - \mid \seca{*}
%
\\ \id{un} & ::= & - \mid \seca{!}
%
\\ s & ::= & \seca{if}(e)\,\{\,\many{s}\,\}\,\seca{else}\,\{\,\many{s}\,\}
\\ & & \mid \seca{while}(e)\,\{\,\many{s}\,\}\ i
\\ & & \mid x\ \seca{=}\ e \mid \seca{return}\ e \mid e
%
\\ \IEEEeqnarraymulticol{3}{l}{\id{funcDef} ::= \id{id}(\many{x}) \{\ \many{s}\ \}}
\end{IEEEeqnarraybox*}
\caption{Abstract syntax of SECA}
\label{fig:secaSyntax}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
\begin{tikzpicture}
  [ ->
  , >=stealth'
  , shorten >=1pt
  , auto
  , node distance=2.1cm
  , semithick
  ]
  \tikzstyle{every state}=[draw]

  \node[state] (A)              {off/0};
  \node[state] (B) [right of=A] {on/10};

  \path (A) edge [bend left]  node[above=3pt] {switchOn} (B)
        (B) edge [bend left]  node[below=3pt] {switchOff} (A)
        (B) edge [loop right] node[above=7pt] {switchOn} (B)
        (A) edge [loop left]  node[above=7pt] {switchOff} (A)
        ;
\end{tikzpicture}
\caption{Hardware component model for an LED.
In state \emph{on} it has a power draw of 10, in state \emph{off} a power draw of 0.
The transitions correspond to component functions.
}
\label{fig:ledComponentModel}
\end{minipage}

\end{figure}
\endgroup


\subsection{Semantics}
\label{sec:semantics}


SECA comes with four semantics, for different purposes.
The \emph{standard semantics} defines how programs are executed.
The \emph{energy-aware semantics} additionally traces the energy consumption during program execution in a skyline.
The \emph{symbolic execution semantics} executes all possible paths through a program.
The \emph{energy-aware symbolic execution semantics} traces all possible skylines a program can produce.
%Only the energy-aware semantics and the energy-aware symbolic execution semantics are covered in this paper.
The focus of this paper is the last one;
the others are formally defined in a technical report \cite{KlinikGKE2020TechReport}.
Below, we will informally discuss the energy-aware semantics, as it is a useful foundation to understand the energy-aware symbolic execution semantics.




\paragraph{Components}
%Before we talk about program semantic, we have to
To start, we must
define the semantics of component calls.
In order to analyse the energy consumption of programs, we need an estimation of how much energy their hardware components consume.
Such an estimation is called a \emph{hardware component model}, or \emph{component model} for short.

%The component models we use %for SECA
%are slightly simplified from those in the earlier work on ECA.
Component models are labelled transition systems, not necessarily finite, where each state has a power draw.
Transitions are labelled with \emph{component functions} (e.g. \seca{switchOn}).
%This is slightly simplified from the component models in ECA.
Formally,
a \emph{hardware component model} $\tuple{S, L, \delta, o}$ consists of a set of states $S$, a finite set of labels $L$, a transition function $\delta : L \times S \to \id{IO} (\Z \times S)$ and a power draw function $o : S \to \N$.
A \emph{configuration} of a model is an element of $S$: the current state.
Every component has a start state.
%
We borrow Haskell's notation $\id{IO}(\Z \times S)$ to indicate that to produce the return value $\Z \times S$, the function may perform arbitrary I/O.
Input-producing hardware like sensors or terminals use the return value $\Z$ to return the input.
Actuators like motors should return 0.
The power draw function $o$ specifies how much power the component consumes in each state.

Let us consider an example.
A component model of an LED is shown in \cref{fig:ledComponentModel}.
LEDs have two states, \emph{on} and \emph{off}, and transitions \emph{switchOn} and \emph{switchOff} between them.
In the \emph{on} state an LED has a power draw of 10, in the \emph{off} state it has a power draw of 0.
The component functions do not return values.

SECA programs always run in contexts where a number of component models are present.
Such contexts are called \emph{component states}, or CStates for short.
A CState is a partial mapping from names to configurations.
If the CState contains an LED, say under the name of \seca{LED1}, programs in this context can contain the component calls \seca{LED1.switchOn()} and \seca{LED1.switchOff()}.

\paragraph{Skylines}
The energy-aware semantics generates \emph{skylines}.
A skyline is a list of \emph{segments}.
A segment is either a start point $S(l,p)$ at line $l$ and power draw $p$, a forwards line $F(l)$, a backwards jump $J(l)$, or an edge $E(p)$.
Every skyline has exactly one start point, which must be its first segment.
Other segments are interpreted relative to their predecessors.

The y-axis of a skyline refers to line numbers.
Using line numbers to identify program points requires the source code to be formatted so that every skyline-producing program point is on its own line, to avoid segments
being drawn over each other. This concerns
the left-hand side of assignments, \seca{if} keywords, \seca{while} keywords, the closing brace of the body of while loops, \seca{return} keywords, and the closing parentheses of function- and component calls.
Expressions that contain function- or component calls as subexpressions should be split over multiple lines.
Even with these restrictions, segments may end up on top of each other when the same lines of code are executed more than once.

\paragraph{The energy-aware standard semantics}
We explain by example how the semantics executes a program and constructs its skyline on the way.
The program in \cref{fig:exampleMain} switches \seca{LED1} on five times in a loop, and then switches it off.
The skyline fragments generated during execution are shown in \cref{fig:skylineFragmentsExample}.

\begingroup
%\setlength{\intextsep}{1\baselineskip plus 2.0pt minus 2.0pt}
%\setlength{\belowcaptionskip}{-2pt}
\setlength{\belowcaptionskip}{-10pt}
\captionsetup{farskip=0pt}%
\begin{figure}[t]
  \centering
  \begin{minipage}{0.32\textwidth}
  \lstinputlisting[language=SECA,numbers=left,xleftmargin=2em,basicstyle=\small]{programs/example.eca}
  \end{minipage}
\subfloat[]{%
  \begin{minipage}{0.155\textwidth}
  \def\svgwidth{1\textwidth}%
  \input{example-main-1.pdf_tex}%
  \label{fig:exampleMain1}%
  \end{minipage}
}%
\subfloat[]{%
  \begin{minipage}{0.155\textwidth}
  \def\svgwidth{1\textwidth}%
  \input{example-main-2.pdf_tex}%
  \label{fig:exampleMain2}%
  \end{minipage}
}%
\subfloat[]{%
  \begin{minipage}{0.155\textwidth}
  \def\svgwidth{1\textwidth}%
  \input{example-main-3.pdf_tex}%
  \label{fig:exampleMain3}%
  \end{minipage}
}%
\subfloat[]{%
  \begin{minipage}{0.155\textwidth}
  \def\svgwidth{1\textwidth}%
  \input{example-main-4.pdf_tex}%
  \label{fig:exampleMain4}%
  \end{minipage}
}%
\caption{Stepwise construction of a skyline
\protect\subref{fig:exampleMain1} After switching on LED1
\protect\subref{fig:exampleMain2} After the first loop iteration
\protect\subref{fig:exampleMain3} After the second iteration
\protect\subref{fig:exampleMain4} The final skyline
}
\label{fig:exampleMain}
\end{figure}
\endgroup
Execution of this program starts in a CState where LED1 is in state \emph{off}.
\Cref{fig:exampleMain1} shows the skyline just after executing line 4, where the LED has been
\begin{wrapfigure}{r}{0.48\textwidth}
%\setlength{\belowcaptionskip}{-3\baselineskip}
%\vspace{-1.8\baselineskip}
\begin{IEEEeqnarraybox*}{l's}
   [S(1,0), F(2), F(3), F(4), E(10), & \subref{fig:exampleMain1}
\\ F(5), F(6), J(3),                 & \subref{fig:exampleMain2}
\\ F(4), E(10), F(5), F(6), J(3),    & \subref{fig:exampleMain3}
\\ F(4), E(10), F(5), F(6), J(3),
\\ F(4), E(10), F(5), F(6), J(3),
\\ F(4), E(10), F(5), F(6), J(3),
\\ F(7), E(0), F(8), F(9)]           & \subref{fig:exampleMain4}
\end{IEEEeqnarraybox*}%
\caption{Skyline fragments for \cref{fig:exampleMain}.
%\mkl{wrapfigures are not floats and must be placed explicitly.
%Whenever text is added or removed above, we have to adjust the placement.
%Ugly, but I don't see another way.
}
\label{fig:skylineFragmentsExample}
\end{wrapfigure}%
switched on.
Lines 2 and 3 do not change the power draw, which yields two forward
segments from line 1 to 2 and from line 2 to 3.
LED1 is switched on in line 4, which extends the skyline with a forward segment from
3 to 4, followed by a rising edge to power draw 10.

\Cref{fig:exampleMain2} shows the skyline after one loop iteration, when the loop condition in line 3 has been executed a second time.
Line 5 has caused a forward segment from 4 to 5.
Execution of line 3 has caused a forward segment from the last
statement of the loop
in line 5 to the closing brace of the loop in line 6, followed by a backwards jump to line 3, which is not visible in the diagram.

\Cref{fig:exampleMain3} shows the skyline after two iterations.
The second iteration starts at line 3 with power draw 10, and gives of three forward segments
3 to 4, 4 to 5, and 5 to 6.  None of them change the power draw, as the LED is already on.
These segments overlap with the segments of the previous iteration, and are drawn on top of each other.
All subsequent iterations also generate identical segments.

After five loop iterations, the program exits the loop, with the skyline shown in \cref{fig:exampleMain4}.
Switching off the LED generates a falling edge to power draw 0 in line 7.
The return statement finally generates two forward segments, one for itself from 7 to 8 and one for exiting the function from 8 to 9.

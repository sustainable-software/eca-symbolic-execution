\section{Methodology}
\label{sec:methodology}

Given a program in the SECA language (\cref{sec:ecaLanguage}), our system performs symbolic execution to examine all possible execution paths.
For each path, the symbolic execution engine tracks the power draw of all components that the program controls, resulting in a graph that relates program points to energy consumption (\cref{sec:symbolicExecution}).
%Program points are individual statements and parts of expressions, which are handled as a whole in a programming language (discussed in detail below).
We call such graphs \emph{skylines}.
The result of symbolic execution is a set of skylines for every function, considering all calls to a function, across all execution paths.
Our system then condenses these skylines into a summary of the energy behaviour of the program by \emph{merging} common segments, to emphasize where skylines differ (\cref{sec:merging}).
The merged skylines are rendered as \emph{skyline diagrams}, with line numbers on the vertical axis and power draw on the horizontal axis.
The paper ends with an analysis of a real-world example (\cref{sec:robot}), a discussion of related work (\cref{sec:relatedWork}) and ideas for future work (\cref{sec:futureWork}).

\paragraph{Control software}
Our domain is control software, whose main purpose is to control hardware components like sensors or motors.
It runs on embedded systems using low powered microprocessors, which have a negligable energy use compared to the software-controlled hardware.
Control software has two key characteristics.
First, it has low algorithmic complexity.
We aim to analyse programs that, for example, regulate a central heating installation, not those that calculate square roots.
Second, it contains statements that interact with hardware components.
These component calls are the focus of our system, as we seek to find how their invocation influences the energy behaviour of the program.
If programs have parts with high algorithmic complexity, which would overextend the capabilities of symbolic execution, such parts could be hidden in library calls and left out of the analysis.

SECA represents the behaviour of hardware components in a model similar to the one in \cite{Gastel2017}; essentially a labelled transition system where every state has a power draw, and state changes can only be initiated by the code.

\paragraph{Energy consumption}
Resources other than power draw can also be modelled, as long as they can be summed up.
The analysis does not care; it sees resource consumption as a unitless number.
We assume that components have rectangular power profiles, which means there is no ramp-up when switching them on.
%If this is not the case, the power draw of a state should represent an average number. [what kind of analysis is your result? What is the impact on the validity? This sentence raises lots of questions.]

\paragraph{Symbolic execution}
Symbolic execution \cite{King1976} is a program semantics that traces all possible program execution paths.
Whenever a program asks for input, for example from a sensor or terminal, a symbolic input variable $\id{any}_i$ is created.
When conditionals are encountered, execution splits into two paths: one for evaluating the condition to true, one to false.
Each path is coupled with the constraint on the symbolic inputs that must hold for this path to be followed.

To illustrate the idea, consider the program: \texttt{x = TEMP.readInt(); \textbf{if} (x $<$5) \{ y=7; \} \textbf{else} \{ y=2*x+1; \}}.
Symbolic execution results in two paths, one through the then- and one through the else-branch.
The first one terminates with global state $[x \mapsto \id{any}_0, y \mapsto 7]$ and path constraint $\id{any}_0 < 5$.
The second one terminates with $[x \mapsto \id{any}_0, y \mapsto 2\id{any}_0+1]$ and path constraint $\neg(\id{any}_0 < 5)$.

Path constraints can be given to an SMT (satisfiability modulo theories) solver, to prune infeasible paths% with conflicting path constraints
, and calculate example values for the $\id{any}$s.

Symbolic execution does not terminate if there is a path that loops indefi\-nitely.
To bypass this problem, our system exits loops after a pre-defined number of iterations, and generates a warning.
In such situations there could be paths whose energy usage is not reported, and hence the analysis is unsound.
However, due to the nature of symbolic execution, all possible energy behaviours of a loop will often be discovered in less iterations than what is needed for the behaviours to occur in concrete execution.
We expect situations with missed energy behaviours to be uncommon in typical programs.

\begingroup
%\setlength{\belowcaptionskip}{-16pt}
%\captionsetup{farskip=0pt}%
\begin{figure}[t]
\center%
\begin{minipage}{1.0\linewidth}%
\center%
\subfloat[]{%
  \begin{minipage}{0.33\textwidth}%
  \vspace{-0.7em}
  \lstinputlisting[language=SECA,numbers=left,xleftmargin=2em,basicstyle=\small]{programs/split-and-merge.eca}%
  \end{minipage}%
  \label{fig:splitAndMergeCode}%
}%
\subfloat[]{%
  \begin{minipage}{0.33\textwidth}%
  \def\svgwidth{1.0\textwidth}%
  \input{split-and-merge-main-no-merge.pdf_tex}%
  \end{minipage}%
  \label{fig:splitAndMergeNoMergee}%
}%
\subfloat[]{%
  \begin{minipage}{0.33\textwidth}%
  \def\svgwidth{1.0\textwidth}%
  \input{split-and-merge-main-yes-merge.pdf_tex}%
  \end{minipage}%
  \label{fig:splitAndMergeYesMerge}%
}
\caption{\protect\subref{fig:splitAndMergeCode} A program with two execution paths
\protect\subref{fig:splitAndMergeNoMergee} Its skylines
\protect\subref{fig:splitAndMergeYesMerge} Its merged skylines}%
\end{minipage}%
\end{figure}

\endgroup

\paragraph{Program points}
In previous work \cite{KlinikJP2017} we analysed resource consumption over time.
This has several advantages, but does not clearly show which parts of the program contribute to which parts of a skyline.
Here, we give up the time aspect and instead relate resource use directly to lines in the source code.
This requires certain coding conventions; for example, there may be only one non-trivial expression or statement per line, and closing braces must be on their own line.
The results are diagrams with a natural control flow from top to bottom with occasional jumps, which clearly relate parts of the program to their energy consumption.
Consider for example the program in \cref{fig:splitAndMergeCode}.
Symbolic execution results in the two skylines in \cref{fig:splitAndMergeNoMergee}, which show the hotspots in lines 5 and 8.

\paragraph{Merging}
Symbolic execution results in a set of skylines, one for every execution path.
These skylines often have identical parts, only differing after or up to a certain point.
Sometimes a skyline is equal to a second one for a few lines and then becomes equal to a third.
This effect is common in loops, where a piece of code is executed repeatedly.
%
The program in \cref{fig:splitAndMergeCode} has two execution paths that only differ during the execution of the conditional (lines 4--10).
\Cref{fig:splitAndMergeYesMerge} shows its skylines after merging.
Until line 4 they are drawn as a single skyline.  At line 4 is a \emph{split point}, after which they are drawn separately.
At line 11 they come together again, and continue so until the end of the function.

Our system aims to give programmers an idea of the energy behaviour of their programs, so that they gain insight where the hotspots lie.
We argue that for this goal it is not required that skyline diagrams convey all information about all runs.
Instead, we condense information such that unexpected spikes can be clearly identified.
This comes at the cost of information loss about the exact number of runs, and losing the ability to fully trace individual runs.


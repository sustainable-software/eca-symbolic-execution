\section{Introduction}
\label{sec:introduction}

% context
Software that controls hardware is found in many places, such as washing machines, smartphones, or self-driving cars.
The software running in such devices is in charge of orchestrating the hardware components, like sensors, motors, displays, or radios.
Formal analysis of such devices is hard, because hardware and software have to be analysed together.
In order to optimize energy consumption of such devices, especially when they are battery-powered, it is useful for programmers to have a prediction of energy-behaviour of all the components when running their program.
Simulation or actual measurement of running devices can give some insight, but only for one specific scenario and hardware configuration.

% goal
We develop a static analysis based on symbolic execution that can visualize the energy behaviour of all possible executions of a program at once.
This allows programmers to quickly assess the energy impact of a change, already during development.
Our method is parametrized with hardware models, so that programmers can swap components and explore different hardware configurations.

% scope
In the domain of embedded systems and control software, the energy use of the processor is sometimes negligible.
We therefore limit our scope to the energy use of the hardware components controlled by the software.
If desired, programmers can bypass this restriction by modelling the processor as a hardware component and switching between its power states explicitly with corresponding component calls.
Modelling processors as a hardware components is possible, because they often have approximately constant energy consumption. For example the popular ATmega328P, used on the Arduino UNO, has an amperage of 0.2 mA in Active Mode, 0.75 $\mu$A in Power-save Mode, and 0.1 $\mu$A in Power-down Mode \cite{ATmegaDataSheet}.

\begingroup
%\setlength{\textfloatsep}{0.0pt}
%\captionsetup{farskip=0pt}%
\begin{figure}[tb]%
\centering
\subfloat[]{%
  \begin{minipage}{0.39\textwidth}%
  \lstinputlisting[language=SECA,numbers=left,xleftmargin=2.0em,lastline=15,basicstyle=\small]{programs/bug.eca}%
  \label{lst:bug}%
  \end{minipage}%
}%
\subfloat[]{%
  \begin{minipage}{0.31\textwidth}%
  \includegraphics[width=1\textwidth]{bug-main.pdf}%
  \label{fig:bug}%
  \end{minipage}%
}%
\caption{\protect\subref{lst:bug} Sensor input controls a lamp.
\protect\subref{fig:bug} All possible runs of the program.
  One run does not end at power draw zero, which indicates a bug.}
\end{figure}
\endgroup

% approach
We illustrate our approach with an example.
The program in \cref{lst:bug} reads a sensor value, and switches on either LED1 or LED2.
It has a bug in line 9, where \seca{<} is used instead of \seca{<=}.
There are three possible executions, one of which does not end with a power draw of zero, as there exists a sensor value where LED1 is not switched off.
The skyline diagram in \cref{fig:bug} shows a merged view of the three executions.
The horizontal axis shows power draw, and the vertical axis line numbers.
Skylines that would be drawn on top of each other are shifted by a small offset.
In this view, programmers can see that some component still consumes energy at the end of the function, and can start investigating the issue.

% contribution
This paper brings together two distinct lines of earlier work: the energy consumption analysis by van Gastel et al. \cite{Gastel2017} and the skyline diagrams by Klinik et al. \cite{KlinikJP2017}.
Our contribution is threefold.
First, we introduce a symbolic execution engine that tracks hardware state and works with the programming language SECA (Symbolic Energy Consumption Analysis).
Second, we define visualization rules for the results of the symbolic execution as diagrams of power draw over points in the source code.
Third, we define an algorithm to reduce the number of plotted graphs, hiding redundant information, to make the diagrams more concise.
Our proof-of-concept implementation is available online \cite{SecaSourceCodeRepository}.

%Disclaimer
\paragraph{Remark}
Our goal is to explore the idea of drawing energy skylines over source lines; not (yet) to make an industry-ready tool.
To focus on this goal, our method considers a C-like language that lacks the complexity of C itself.
Likewise, the well-known problem of exponential state-space explosion, and reduction techniques that
may be used to manage this problem, is not included in our scope.
Thus, we do not currently consider programs with thousands of lines.

# We don't want techreport.tex, so these are given explicitly
TEX_SOURCES := paper/abstract.tex \
	paper/acknowledgements.tex \
	paper/discussion.tex \
	paper/eca-language.tex \
	paper/future-work.tex \
	paper/introduction.tex \
	paper/klinikGK2020-energy-skylines-analysis.tex \
	paper/languages.tex \
	paper/merging.tex \
	paper/methodology.tex \
	paper/related-work.tex \
	paper/robot.tex \
	paper/symbolic-execution.tex \
	paper/visualization.tex
# old figures
FIGURES := $(wildcard paper/*.pdf_tex)
# new figures don't have pdf_tex
FIGURE_PDFS := $(patsubst %.pdf_tex,%.pdf,${FIGURES}) \
	paper/bug-main.pdf \
	paper/nested-loops-main-no-merge.pdf \
	paper/nested-loops-main-yes-merge.pdf \
	paper/robot-literal-loop-loop.pdf \
	paper/robot-literal-MoveForward-MoveForward.pdf
LATEXMKRC := paper/latexmkrc
STYLE_FILES := paper/llncs.cls paper/splncs04.bst
BIBLIOGRAHPY_DIR := paper/bibliography
PROGRAMS_DIR := paper/programs
CONSENT_DIR := consent-to-publish
PAPER_PDF := paper/klinikGK2020-energy-skylines-analysis.pdf
README := paper/README.txt

PAPER_DIR := paper
TARGET_DIR := Paper4
TARGET_ZIP := Paper4.zip

.PHONY submissionZip:
	mkdir -p ${TARGET_DIR}
	cp ${TEX_SOURCES} ${TARGET_DIR}
	cp ${FIGURES} ${TARGET_DIR}
	cp ${FIGURE_PDFS} ${TARGET_DIR}
	cp ${STYLE_FILES} ${TARGET_DIR}
	cp ${LATEXMKRC} ${TARGET_DIR}
	cp ${README} ${TARGET_DIR}
	cp -r ${BIBLIOGRAHPY_DIR} ${TARGET_DIR}
	cp -r ${CONSENT_DIR} ${TARGET_DIR}
	cp -r ${PROGRAMS_DIR} ${TARGET_DIR}
	# important: make paper in PAPER_DIR, because we don't want build artefacts in TARGET_DIR
	cd ${PAPER_DIR}; latexmk klinikGK2020-energy-skylines-analysis.tex
	cp ${PAPER_PDF} ${TARGET_DIR}
	zip -r ${TARGET_ZIP} ${TARGET_DIR}

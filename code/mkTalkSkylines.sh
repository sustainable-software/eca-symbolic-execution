#!/bin/bash

#stack run -- --width 204 --height 360 --no-title --latex --file-format PDF -s programs/talk/bug.eca --y-bounds '(0,16)' --y-labels [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15] --margin 0 --x-label-gap 5 --x-labels [0,7,10] --font-name "TeX Gyre Heros" --font-size 13.0 --x-axis-title "power draw (mW)" --y-axis-title "line number"

#for LIMIT in 0 1 2 3 4 5 6 7
#do
  #echo 5 | stack run -- -c programs/talk/concrete-semantics.eca  --file-format PDF --width 140 --height 160 --no-title --y-bounds '(1,7)' --x-labels '[0,7]' --font-name "TeX Gyre Heros" --no-merge --fragment-limit $LIMIT -o $LIMIT
#done

#stack run -- -s programs/talk/split-and-merge.eca --file-format PDF --width 140 --height 200 --no-title --x-labels '[0,5,10,15]' --y-labels '[0,2,4,6,8,10,12]' --y-bounds '(0,14)' --font-name "TeX Gyre Heros" --no-merge

#stack run -- -s programs/talk/split-and-merge.eca --file-format PDF --width 140 --height 200 --no-title --x-labels '[0,5,10,15]' --y-labels '[0,2,4,6,8,10,12]' --y-bounds '(0,14)' --font-name "TeX Gyre Heros" --output-suffix -merged

#stack run -- -s programs/talk/nested-loops.eca --file-format PDF --width 300 --height 400 --no-title --x-labels '[0,5,10]' --y-labels '[2,4,6,8,10,12,14]' --y-bounds '(1,15)' --font-name "TeX Gyre Heros" --no-merge --output-suffix -unmerged

#stack run -- -s programs/talk/nested-loops.eca --file-format PDF --width 300 --height 400 --no-title --x-labels '[0,5,10]' --y-labels '[2,4,6,8,10,12,14]' --y-bounds '(1,15)' --font-name "TeX Gyre Heros" --output-suffix -merged

stack run -- -s programs/talk/robot.eca --file-format PDF --width 300 --height 400 --font-name "TeX Gyre Heros" --x-labels '[0,750,1500]' --x-axis-title "power draw (mW)" --y-axis-title "line number"

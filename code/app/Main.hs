module Main where

import System.Environment
import System.Console.GetOpt
import System.FilePath
import Data.SBV
import qualified Data.Map as Map
import Data.Map ((!))
import Data.List
import Control.Monad.State
import Data.Char (isSpace)
import qualified Data.Aeson as Aeson

import Parser
import qualified Interpreter
import SymInterpreter
import ExampleComponents
import SymComponent
import Skyline
import SkylineMerge
import AST
import SymAST
import Charts
import Options
import InterpreterUtil

main :: IO ()
main = do
  opts <- getArgs >>= parseOptions
  case (mode opts) of
    Help ->
      putStrLn $ usageInfo header optDescr
    ExecuteConcrete inputFilename -> do

      when (filterComponents opts) $ do
        putStrLn "WARNING: component filter not implemented for concrete execution"

      inputSource <- readFile inputFilename
      let ast_ = parseEca inputSource inputFilename
      case ast_ of
        Left err -> putStrLn err
        Right ast@(prog,_) -> do
          (_,Interpreter.ST{Interpreter._skies=skies}) <-
            Interpreter.executeMain ast defaultComponents

          when (showFinalSkylineFragments opts) (doShowFinalSkylineFragments skies)

          case (exportJson opts) of
            Nothing -> doPlottingSymbolic prog skies opts inputFilename
            Just jsonFilename -> doExportJson jsonFilename prog skies opts inputFilename

    ExecuteSymbolic inputFilename -> do
      inputSource <- readFile inputFilename
      let ast_ = parseEca inputSource inputFilename
      case ast_ of
        Left err -> putStrLn err
        Right ast@(prog,_) -> do
          let results = symExecuteMain ast defaultSymComponents opts

          if (showReturnValues opts)
            then
              doShowReturnValues results
            else do
              let allSkies = Map.unionsWith (<>) (map _skies results)
              when (debug opts) (doDebug allSkies)
              case (exportJson opts) of
                Nothing -> doPlottingSymbolic prog allSkies opts inputFilename
                Just jsonFilename -> doExportJson jsonFilename prog allSkies opts inputFilename

          when (showFinalSkylineFragments opts)
            (sequence_
              [ doShowFinalSkylineFragments skies
              | (SST {SymComponent._skies=skies}) <- results
              ])

    Foobar inputFilename -> foobar inputFilename opts

doShowReturnValues :: [SymInterpreterState] -> IO ()
doShowReturnValues results = do
  putStrLn $ (show $ length results) <> " results"
  sequence_
    [ do
      satResult <- mySat pathConstraint
      -- pair a skyline with its path constraint and a model for the
      -- path constraint, for plotting
      putStrLn $
        prettyprint (pstate!"#return")
        <> ": " <> prettyPrintModel satResult
        <> "   " <> prettyprint pathConstraint
        <> "   " <> show (Map.toList $ fmap prettyprint gstate)
    | SST{_pathConstraint=pathConstraint,_pstate=pstate,_gstate=gstate} <- results
    ]


doShowFinalSkylineFragments :: FunctionSkylines -> IO ()
doShowFinalSkylineFragments skies =
  sequence_ $ do
    (funName, skylines) <- Map.toList skies -- for every function with all its temp skylines
    return $ do
      putStrLn funName
      sequence_ $ do
        skyline <- skylines -- for every temp skyline
        frag <- reverse skyline
        return (putStrLn $ showTempFragment frag)

showTempFragment :: TempFragment -> String
showTempFragment (TStartPoint pt) = "S" ++ show pt
showTempFragment (TEdge (l,p)) = "F("++show l++")\nE(" ++ show p ++ ")"
showTempFragment (TFill l) = "F(" ++ show l ++ ")"
showTempFragment (TJump l) = "J(" ++ show l ++ ")"


doDebug :: FunctionSkylines -> IO ()
doDebug skies =
  sequence_ $ do
    (_, funTempSkies) <- Map.toList skies -- for every function with all its temp skylines
    let
      funSkies = map (tempToSkyline . reverse) funTempSkies
      fragDB = mergeSkylines funSkies
    return $ do
      mapM_ print funSkies
      mapM_ print fragDB

mkOutputFilename :: Options -> String -> FilePath -> FilePath
mkOutputFilename opts funName inputFilename = outputDir opts </> outputFile
  where
  inputBaseName = dropExtensions $ takeFileName inputFilename
  outputBaseName = inputBaseName <> "-" <> funName <> (maybe "" id $ outputSuffix opts)
  outputFile = addExtension outputBaseName (snd $ fileFormat opts)


doPlottingSymbolic :: Program -> FunctionSkylines -> Options -> FilePath -> IO ()
doPlottingSymbolic ast skies opts inputFilename = do -- IO monad
  sequence_ $ do -- List monad
    (funName, funTempSkies) <- Map.toList skies -- for every function with all its temp skylines
    return $ do -- IO Monad
      let
        funSkies = map (tempToSkyline . reverse) funTempSkies
        outputFileName = mkOutputFilename opts funName inputFilename
        fragDB = mergeSkylines funSkies
      if merge opts
        then plotFragments opts (ast!funName) fragDB outputFileName
        else plotSkylineSymbolic opts (ast!funName) funSkies outputFileName

doPrintSkylines :: [(Skyline, String)] -> IO ()
doPrintSkylines skylines = do
  putStr "Number of skylines: "
  print $ length skylines
  sequence_ [mapM_ print skyline | (skyline,_) <- skylines]

prettyPrintModel :: SatResult -> String
prettyPrintModel satResult =
  let dict = getModelDictionary satResult in
  concat $ intersperse ", " $ [var <> " = " <> (removeTypeCV $ show cv) | (var,cv) <- Map.toList dict]

-- Remove type annotation from printed CV.
-- The SBV library always prints type annotations for values. In our case we only have Int32.
-- This function turns "0 :: Int32" into "0"
removeTypeCV :: String -> String
removeTypeCV s = takeWhile (not . isSpace) s

doDebugSkylines :: [(Skyline,String)] -> IO ()
doDebugSkylines skylines = do
  let
    fragDB = prepare [skyline | (skyline,_) <- skylines]
    fragGroups = groupFragments fragDB
    fragDBMerged = execState (mergeAllGroups fragGroups) fragDB
  putStrLn $ "fragDB size: " <> (show $ Map.size fragDB)
  putStrLn $ "merged size: " <> (show $ Map.size fragDBMerged)

foobar :: FilePath -> Options -> IO ()
foobar _ opts = do
  let
    skyline = [TStartPoint (1,0), TFill 3, TEdge (7,10), TFill 9, TJump 4, TEdge (5,3), TFill 10]
    funDef = FunctionDefinition (error "type") (error "pos1") (error "name") (error "args") (error "pos2") (Block []) (error "pos3")
    funSky = tempToSkyline skyline
    outputFileName = mkOutputFilename opts "main" "out/skylineExample.eca"
    fragDB = mergeSkylines [funSky]
  plotFragments opts funDef fragDB outputFileName


doExportJson :: FilePath -> Program -> FunctionSkylines -> Options -> FilePath -> IO ()
doExportJson jsonFilename prog skies opts inputFilename =
  let
    funSkies = fmap (map (tempToSkyline . reverse)) skies
    fragDB = fmap mergeSkylines funSkies
  in
    Aeson.encodeFile jsonFilename fragDB

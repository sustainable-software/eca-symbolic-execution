#!/bin/bash

echo "svg to pdf conversion with inkscape no longer works with the Charts Cairo backend. Directly generate as PDF, and set the corresponding FONT name."

#INFILE="$1"
#OUTFILE="${1%.svg}.pdf"

#inkscape --export-latex --export-area-drawing "$1" --export-type=pdf

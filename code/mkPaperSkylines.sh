#!/bin/bash

###############
# Line Follower
###############

FONT="TeX Gyre Termes"

generate-robot()
{
  stack run -- --max-iterations 2 --width 200 --height 270 --no-title --latex --file-format PDF --font-name "${FONT}" -s ../STTT-paper/programs/line-follower/robot-literal.eca --margin 0 --x-labels '[0,1500]' --x-axis-title "power draw (mW)" --y-axis-title "line number" --y-label-prefix " " "$@"
}

#generate-robot --y-bounds '(6,25)' --y-labels '[6,8,10,12,14,16,18,20,22,24]' -o "-loop"
#generate-robot --y-bounds '(24,30)' --y-labels '[25,26,27,28,29]' -o -MoveForward

###########
# Others
###########

#stack run -- --width 204 --height 360 --no-title --latex --file-format PDF --font-name "${FONT}" -s ../STTT-paper/programs/bug.eca --y-bounds '(0,16)' --y-labels [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15] --margin 0 --x-label-gap 5 --x-labels [0,7,10] --font-size 13.0 --x-axis-title "power draw (mW)" --y-axis-title "line number"

#stack run -- --width 200 --height 300 --no-title --latex --file-format SVG -s programs/paper/split-and-merge.eca --y-bounds '(0,14)' --y-labels [1,2,3,4,5,6,7,8,9,10,11,12,13] --x-labels [0,5,12,15] -o -yes-merge
#./svg2tex.sh out/split-and-merge-main-yes-merge.svg

#stack run -- --width 200 --height 300 --no-title --latex --file-format SVG -s programs/paper/split-and-merge.eca --y-bounds '(0,14)' --y-labels [1,2,3,4,5,6,7,8,9,10,11,12,13] --x-labels [0,5,12,15] --no-merge -o -no-merge
#./svg2tex.sh out/split-and-merge-main-no-merge.svg

generate-nested-loops()
{
  stack run -- --width 180 --height 315 --no-title --latex --file-format PDF --font-name "${FONT}" --font-size 13.0 -s ../STTT-paper/programs/nested-loops.eca --y-label-gap 15 --y-bounds '(0,15)' --y-labels [1,2,3,4,5,6,7,8,9,10,11,12,13,14] --margin 0 --x-labels [0,7,10] "$@"
}

#generate-nested-loops --no-merge -o -no-merge
#generate-nested-loops -o -yes-merge

#stack run -- --width 200 --height 300 --no-title --latex --file-format SVG -s programs/paper/loop-alternating.eca --y-bounds '(0,18)' --y-labels [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17] --x-labels [0,7,10,17] --x-label-gap 5 --no-merge -o -no-merge
#./svg2tex.sh out/loop-alternating-main-no-merge.svg

#stack run -- --width 200 --height 300 --no-title --latex --file-format SVG -s programs/paper/loop-alternating.eca --y-bounds '(0,18)' --y-labels [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17] --x-labels [0,7,10,17] --x-label-gap 5 -o -yes-merge
#./svg2tex.sh out/loop-alternating-main-yes-merge.svg

generate-example()
{
  stack run -- --width 120 --height 192 --no-title --latex --file-format PDF --font-name "${FONT}" -s ../STTT-paper/programs/example.eca --y-labels [1,2,3,4,5,6,7,8,9] --x-labels [0,10] --margin 0 --no-merge -o $1 --y-label-gap 15 --fragment-limit $2
}

#generate-example -1 4
#generate-example -2 6
#generate-example -3 8
#generate-example -4 1000

#stack run -- --width 200 --height 300 --no-title --latex --file-format SVG -s programs/paper/function.eca --y-label-gap 20 --y-bounds '(0,9)' --y-labels [1,2,3,4,5,6,7,8] --x-labels [0,10]
#./svg2tex.sh out/function-main.svg
#stack run -- --width 200 --height 300 --no-title --latex --file-format SVG -s programs/paper/function.eca --y-label-gap 20 --y-bounds '(9.5,17.5)' --y-labels [10,11,12,13,14,15,16,17] --x-labels [0,10]
#./svg2tex.sh out/function-dispatch.svg

generate-simple-function-call()
{
  stack run -- --width 120 --height 192 --no-title --latex --file-format PDF --font-name "${FONT}" -s ../STTT-paper/programs/simple-function-call.eca --margin 0 --no-merge "$@"
}

#generate-simple-function-call -o -main --x-labels [0,7,10,17]
#generate-simple-function-call -o -helper --x-labels [10,17]

stack run -- --width 120 --height 192 --no-title --latex --file-format PDF --font-name "${FONT}" -s ../STTT-paper/programs/tiny-example.eca --margin 0 --no-merge --x-labels [0,5,10] --x-axis-title "power draw" --y-axis-title "line number" --y-labels [0,1,2,3,4,5,6] --y-label-prefix " "

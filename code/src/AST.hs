module AST where

import Text.Megaparsec (SourcePos)
import Data.Tree
import Data.Map (Map,(!))
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Graph as Graph
import Control.Monad.State
import Data.List ((\\), nub)

data FunctionDefinition
  = FunctionDefinition
      AstType
      SourcePos -- position of function name
      String
      [FormalParameter]
      SourcePos -- position of opening brace
      Stmt -- Will always be a Block
      SourcePos -- position of closing brace
  deriving (Show, Eq)

data GlobalVarDefinition
  = GlobalVarDefinition AstType SourcePos String Expr
  deriving (Show, Eq)

data Definition
  = FunDef FunctionDefinition
  | VarDef GlobalVarDefinition
  deriving (Show, Eq)

data AstType
  = TypeInt
  | TypeBool
  | TypeVoid
  deriving (Show, Eq)

data FormalParameter
  = FormalParameter AstType String
  deriving (Show, Eq)

data Expr
  = BoolConst Bool
  | IntConst Int
  | Var String
  | BinaryOp BinOp Expr Expr
  | UnaryOp UnOp Expr
  | FuncCall String [Expr] SourcePos
  -- In the paper the SourcePos is the closing parenthesis. I'm too lazy to
  -- change it here, and it doesn't really make a difference.
  | CompCall SourcePos String String
  deriving (Show, Eq)

data UnOp
  = Neg
  | Not
  deriving (Show, Eq)

data BinOp
  = And
  | Or
  | LessEq
  | Less
  | Eq
  | Add
  | Subtract
  | Multiply
  | Modulo
  deriving (Show, Eq)

data Stmt
  = Assign SourcePos String Expr
  | IfThenElse SourcePos Expr Stmt Stmt
  | Block [Stmt]
  -- The interpreters use the Int to count loop iterations. From this they can
  -- infer whether the loop has been run for the first time.
  | While SourcePos Expr Stmt SourcePos Integer
  | Return SourcePos Expr
  | ExprStmt Expr
  deriving (Show, Eq)

-- Program is a list of function definitions
type Program = Map String FunctionDefinition

instance Num Expr where
  (+) l r = BinaryOp Add l r
  (*) l r = BinaryOp Multiply l r
  (-) l r = BinaryOp Subtract l r
  negate e = UnaryOp Neg e
  fromInteger i = IntConst $ fromInteger i
  abs _ = error "instance Num Expr: abs not implemented"
  signum _ = error "instance Num Expr: signum not implemented"


type WhileTree = Forest (SourcePos,SourcePos,Int)

findWhileLoops :: FunctionDefinition -> (WhileTree,Int)
findWhileLoops (FunctionDefinition _ _ _ _ _ funBody _)
  = findWhileLoops_ funBody 0
  where

  findWhileLoops_ (Assign _ _ _) depth = ([],depth)
  findWhileLoops_ (IfThenElse _ _ thenBranch elseBranch) depth =
    let
    (thens,maxDepthThen) = findWhileLoops_ thenBranch depth
    (elses,maxDepthElse) = findWhileLoops_ elseBranch depth
    in
    (thens <> elses, max maxDepthThen maxDepthElse)
  findWhileLoops_ (Block stmts) depth =
    let (trees,depths) = unzip $ map (\stmt -> findWhileLoops_ stmt depth) stmts
    in (concat trees,maximum (depth:depths))
  findWhileLoops_ (While startPos _ whileBody endPos _) depth =
    let
    (body_,bodyDepth) = findWhileLoops_ whileBody (depth+1)
    in
    ([Node (startPos,endPos,depth) body_],bodyDepth)
  findWhileLoops_ (Return _ _) depth = ([],depth)
  findWhileLoops_ (ExprStmt _) depth = ([], depth)


-- Given a function definition, determine all components that are used in this function
findUsedComponents :: FunctionDefinition -> Set String
findUsedComponents (FunctionDefinition _ _ _ _ _ body _) = findUsedComponents_ body
  where
  findUsedComponents_ :: Stmt -> Set String
  findUsedComponents_ (Assign _ _ rhs) = findUsedComponents__ rhs
  findUsedComponents_ (IfThenElse _ cond thenBranch elseBranch) =
       findUsedComponents__ cond
    <> findUsedComponents_ thenBranch
    <> findUsedComponents_ elseBranch
  findUsedComponents_ (Block stmts) = mconcat (map findUsedComponents_ stmts)
  findUsedComponents_ (While _ cond whileBody _ _) =
    findUsedComponents__ cond <> findUsedComponents_ whileBody
  findUsedComponents_ (Return _ e) = findUsedComponents__ e
  findUsedComponents_ (ExprStmt e) = findUsedComponents__ e

  findUsedComponents__ :: Expr -> Set String
  findUsedComponents__ (BoolConst _) = mempty
  findUsedComponents__ (IntConst _) = mempty
  findUsedComponents__ (Var _) = mempty
  findUsedComponents__ (BinaryOp _ lhs rhs) = findUsedComponents__ lhs <> findUsedComponents__ rhs
  findUsedComponents__ (UnaryOp _ arg) = findUsedComponents__ arg
  findUsedComponents__ (FuncCall _ args _) = mconcat $ map findUsedComponents__ args
  findUsedComponents__ (CompCall _ compName _) = Set.singleton compName

findCalledFunctions :: FunctionDefinition -> Set String
findCalledFunctions (FunctionDefinition _ _ _ _ _ body _) = findCalledFunctions_ body
  where
  findCalledFunctions_ :: Stmt -> Set String
  findCalledFunctions_ (Assign _ _ rhs) = findCalledFunctions__ rhs
  findCalledFunctions_ (IfThenElse _ cond thenBranch elseBranch) =
       findCalledFunctions_ thenBranch
    <> findCalledFunctions_ elseBranch
    <> findCalledFunctions__ cond
  findCalledFunctions_ (Block stmts) = mconcat (map findCalledFunctions_ stmts)
  findCalledFunctions_ (While _ cond whileBody _ _) =
    findCalledFunctions_ whileBody <> findCalledFunctions__ cond
  findCalledFunctions_ (Return _ e) = findCalledFunctions__ e
  findCalledFunctions_ (ExprStmt e) = findCalledFunctions__ e

  findCalledFunctions__ :: Expr -> Set String
  findCalledFunctions__ (BoolConst _) = mempty
  findCalledFunctions__ (IntConst _) = mempty
  findCalledFunctions__ (Var _) = mempty
  findCalledFunctions__ (BinaryOp _ lhs rhs) = findCalledFunctions__ lhs <> findCalledFunctions__ rhs
  findCalledFunctions__ (UnaryOp _ arg) = findCalledFunctions__ arg
  findCalledFunctions__ (FuncCall name args _) =
    Set.singleton name <> (mconcat $ map findCalledFunctions__ args)
  findCalledFunctions__ (CompCall _ _ _) = mempty


-- The call graph in a format suitable for Data.Graph
callGraph :: Program -> [(FunctionDefinition, String, [String])]
callGraph prog =
  [ (funDef, funName, Set.toList $ findCalledFunctions funDef)
  | (funName, funDef) <- Map.toList prog
  ]


-- Given a program, calculate the relevant components for each function.
-- A component is *relevant* for a function f if either
--  - f directly uses it
--  - it is relevant for a function called by f
determineUsedComponents :: Program -> Map String [String]
determineUsedComponents prog = fmap (Set.toList) result
  where
  result = execState (mapM_ processOneSCC sccs) mempty
  graph = callGraph prog

  -- Strongly connected components, topologically sorted.
  -- This means we can just collect used components in order of the list
  sccs :: [Graph.SCC (FunctionDefinition, String, [String])]
  sccs = Graph.stronglyConnCompR graph

  processOneSCC
    :: Graph.SCC (FunctionDefinition, String, [String])
    -> State (Map String (Set String)) ()
  processOneSCC (Graph.AcyclicSCC (funDef, funName, calledFunctions)) = do
    usedComponents <- get
    put (Map.insert funName (allComponents usedComponents) usedComponents)
    -- because sccs is topologically sorted, all calledFunctions have already been processed
    where
    ownComponents :: Set String
    ownComponents = findUsedComponents funDef
    componentsOfCalledFunctions :: (Map String (Set String)) -> Set String
    componentsOfCalledFunctions usedComponents = mconcat [usedComponents!f | f <- calledFunctions]
    allComponents :: (Map String (Set String)) -> Set String
    allComponents usedComponents = ownComponents <> componentsOfCalledFunctions usedComponents
  processOneSCC (Graph.CyclicSCC scc) = do
    usedComponents <- get
    let
      allComps = allComponents usedComponents
      usedComponentsOfSCC = Map.fromList [(funName,allComps) | (_,funName,_) <- scc]
      resultMap = Map.union usedComponentsOfSCC usedComponents
    put resultMap
    where
    (funDefs, funNames, calledFunctions) = unzip3 scc
    ownComponents :: Set String
    ownComponents = mconcat $ map findUsedComponents funDefs
    componentsOfCalledFunctions usedComponents =
      -- components of all called functions that are not in the scc
      mconcat [usedComponents!f | f <- (nub $ mconcat calledFunctions) \\ funNames]
    allComponents usedComponents = ownComponents <> componentsOfCalledFunctions usedComponents

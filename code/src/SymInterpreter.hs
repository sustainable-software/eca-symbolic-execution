{-# LANGUAGE MultiParamTypeClasses, NamedFieldPuns #-}
module SymInterpreter where

import Text.Megaparsec
import qualified Data.Map as Map
import Data.Map ((!))
import System.IO.Unsafe

import SymComponent
import AST
import SymAST
import Skyline
import Util
import InterpreterUtil
import Options

mkSF
  :: ProgramCounter
  -> SymPState
  -> String
  -> SourcePos
  -> TempSkyline
  -> SymStackFrame
mkSF callerPC callerPState callerFunName sourcePos callerSky = SF
  { _callerPC = callerPC
  , _callerPState = callerPState
  , _callerFunName = callerFunName
  , _callSite = sourcePos
  , _callerSky = callerSky
  }

mkSST :: Stmt -> SymPState -> SCState -> Program -> Options -> SymInterpreterState
mkSST stmt pstate cstate prog opts = SST
  { _pc = [stmt]
  , _pstate = pstate
  , _funName = "main"
  , _sky = mempty
  , _prog = prog
  , _stack = []
  , _gstate = mempty
  , _cstate = cstate
  , _skies = mempty
  , _pathConstraint = SymBoolConst True
  , _nextAny = 0
  , _opts = opts
  , _funComponents = determineUsedComponents prog
  , _maxIterations = maxIterations opts
  }

symExecuteMain :: (Program,[GlobalVarDefinition]) -> SCState -> Options -> [SymInterpreterState]
symExecuteMain (prog,globalVars) cstate opts = do
  let
    -- find the main function
    FunctionDefinition _ _ _ _ openBracePos body _ = prog ! "main"

    -- preliminary interpreter state, needed to bootstrap
    st0 = (mkSST body mempty cstate prog opts)

    powDraw = curSymPowDraw cstate (getComponentFilter "main" st0)
    startLine = unPos $ sourceLine openBracePos

    -- make global PState
    gstate = symInitGlobalPState globalVars mempty

    st = st0
      { _stack = [mkSF  [] mempty "#eInterpMain" (pos 0 0) mempty]
      , _gstate=gstate
      , _sky = [TStartPoint (startLine,powDraw)]
      }
  symExecute st



-- Determine if and which components to filter when calculating the current power draw
getComponentFilter :: String -> SymInterpreterState -> Maybe [String]
getComponentFilter funName (SST{_opts=Options{filterComponents},_funComponents=funComponents})
  | filterComponents = Just (funComponents!funName)
  | otherwise = Nothing



symInitGlobalPState :: [GlobalVarDefinition] -> SymPState -> SymPState
symInitGlobalPState [] pstate = pstate
symInitGlobalPState (GlobalVarDefinition _ _ varName expr:rest) pstate =
  symInitGlobalPState rest (Map.insert varName val pstate)
  where
  val = symEval (exprToSymExpr expr) pstate


symExecute
  :: SymInterpreterState
  -> [SymInterpreterState]
symExecute st@(SST{ _pc=[],_stack=[] }) = [st]
-- PC is empty, but stack is not. This signals the end of a function without
-- explicit return. We insert a dummy return statement.
symExecute st@(SST{ _pc=[],_stack=(_:_) }) =
  let
    FunctionDefinition _ _ _ _ _ _ endPos = _prog st ! _funName st
    continuations = symStep (Return endPos (IntConst 0)) st
  in concat $ map symExecute continuations
symExecute st@(SST{ _pc=(stmt:rest) }) =
  let continuations = symStep stmt (st { _pc=rest })
  in concat $ map symExecute continuations

symStep :: Stmt -> SymInterpreterState -> [SymInterpreterState]

symStep (IfThenElse (SourcePos{sourceLine}) cond thenBranch elseBranch) st0 =
  let
    newSky = TFill (unPos sourceLine) : (_sky st0)
  in
  symEvaluate (exprToSymExpr cond) (st0 {_sky=newSky}) `myBind_`
  \condition st@(SST{_pathConstraint=pathConstraint,_pc=pc}) ->
  let
    thenConstraint = partialEval (SymBinaryOp And pathConstraint condition)
    elseConstraint = partialEval (SymBinaryOp And pathConstraint (SymUnaryOp Not condition))
    thenSatisfiable = unsafePerformIO $ isSatisfiable thenConstraint
    elseSatisfiable = unsafePerformIO $ isSatisfiable elseConstraint
  in
    [ st { _pc=branch:pc, _pathConstraint=branchConstraint }
    -- Only generate next states for satisfiable branches
    | (branch, True, branchConstraint) <-
        [(thenBranch, thenSatisfiable, thenConstraint)
        ,(elseBranch, elseSatisfiable, elseConstraint)
        ]
    ]

symStep (Return (SourcePos{sourceLine=returnLine}) retExpr) st0@(SST{_sky=sky0}) =
  symEvaluate (exprToSymExpr retExpr) st0{_sky=TFill (unPos returnLine) : sky0}  `myBind_`
    \retVal
    st@(SST
      { _stack=(SF
          {_callerPC=callerPC
          ,_callerPState=callerPState
          ,_callerFunName=callerFunName
          ,_callSite=callSite
          ,_callerSky=callerSky
          }:frames)
      , _funName=funName
      , _sky=sky1
      , _skies=skies
      , _prog=prog
      , _cstate=cstate
      }) ->


  let
    (FunctionDefinition _ _ _ _ _ _ (SourcePos{sourceLine=endLine})) = (prog!funName)
    newSky = TFill (unPos endLine) : sky1
    powDraw = curSymPowDraw cstate (getComponentFilter callerFunName st)
    -- When SymFunCallExprs return, they might cause CompCalls on the same
    -- line, or CompCalls that don't change the power draw. These cases are
    -- handeled in tempToSkyline.
    newCallerSky = TEdge (unPos $ sourceLine callSite, powDraw) : callerSky

  -- pop the topmost item from the call stack
  -- assign the return value to the return register in the caller's PState
  -- restore the call stack
  in
    [st
    { _pstate = Map.insert "#return" retVal callerPState
    , _pc = callerPC
    , _funName = callerFunName
    , _sky = newCallerSky
    , _stack = frames
    , _skies = Map.alter (addSky newSky) funName skies
    }]
  where
    addSky sky Nothing = Just [sky]
    addSky sky (Just otherSkies) = Just (sky:otherSkies)

symStep (Block stmts) st@(SST{_pc=pc}) = [st { _pc = stmts <> pc }]

symStep (Assign sourcePos var expr) st0@(SST{_sky=sky}) =
  let
    startSky = TFill (unPos $ sourceLine sourcePos) : sky
  in
  symEvaluate (exprToSymExpr expr) st0{_sky=startSky} `myBind_` \val st ->
  [assignVar var val st]

-- Drawing skylines for while loops follows these rules:
-- If the loop is encountered for the first time:
--  - We come from outside the loop
--  - Extend the skyline to the beginning of the loop
--  - If the loop loops
--    - Then do nothing
--    - Else extend to the end
-- If the loop is encountered subsequently
--  - We come from inside the loop
--  - Extend the skyline to the end of the loop
--  - If the loop loops
--    - Then jump to the beginning of the loop
--    - Else do nothing
symStep (While startPos cond body endPos numIterations) st0@(SST{_sky=sky0,_maxIterations=maxIterations}) =
  let
    firstTime = numIterations == 0
    startSky = if firstTime
      then TFill (unPos $ sourceLine startPos) : sky0
      else TJump (unPos $ sourceLine startPos) : TFill (unPos $ sourceLine endPos) : sky0
  in
  symEvaluate (exprToSymExpr cond) st0{_sky=startSky} `myBind_`
  \condition st1@(SST{_pc=pc,_pathConstraint=pathConstraint}) ->
  let
    loopContinuation = While startPos cond body endPos (numIterations+1)
    loopConstraint = partialEval (SymBinaryOp And pathConstraint condition)
    skipConstraint = partialEval (SymBinaryOp And pathConstraint (SymUnaryOp Not condition))
    maxIterationsReached = numIterations >= maxIterations
    loopSatisfiable = (unsafePerformIO $ isSatisfiable loopConstraint) && (not maxIterationsReached)
    skipSatisfiable = (unsafePerformIO $ isSatisfiable skipConstraint) || maxIterationsReached
  in
    [ st1 { _pc = newPC, _pathConstraint = branchConstraint }
    -- Only generate next states for satisfiable branches
    | (newPC, True, branchConstraint) <-
        [(body:loopContinuation:pc, loopSatisfiable, loopConstraint)
        ,(                      pc, skipSatisfiable, skipConstraint)
        ]
    ]

symStep (ExprStmt e) st = map snd $ symEvaluate (exprToSymExpr e) st


-- I'm actually working in some kind of monad stack with IO and List.
-- I'm not sure if this can be handelled by monad transformers. TODO: find out,
-- refactor.
myBind
  :: [(a, SymInterpreterState)]
  -> (a -> SymInterpreterState -> [(b, SymInterpreterState)])
  -> [(b, SymInterpreterState)]
myBind x f =
  concat
    [ f a s
    | (a, s) <- x
    ]

-- Non-standard variant of bind that has no result value.
myBind_
  :: [(a, SymInterpreterState)]
  -> (a -> SymInterpreterState -> [SymInterpreterState])
  -> [SymInterpreterState]
myBind_ x f = do
  concat
    [ f a s
    | (a, s) <- x
    ]

-- TODO: clean up this mess with different versions of myBind. The problem is
-- that in my "monad", sometimes I don't return a value, and then it is
-- [SymInterpreterState] instead of [((), SymInterpreterState)]
myBind__
  :: [SymInterpreterState]
  -> (SymInterpreterState -> [(b, SymInterpreterState)])
  -> [(b, SymInterpreterState)]
myBind__ x f = do
  concat
    [ f s
    | s <- x
    ]

mySequence
  :: [SymInterpreterState -> [(a, SymInterpreterState)]]
  -> SymInterpreterState
  -> [([a], SymInterpreterState)]
mySequence [] st = [([], st)]
mySequence (action:actions) st0 =
  (action st0) `myBind` \a st1 ->
  mySequence actions st1 `myBind` \as st2 ->
  [(a:as, st2)]


symCallFunction
  :: String
  -> SourcePos
  -> [SymExpr]
  -> SymInterpreterState
  -> [SymInterpreterState]
symCallFunction
  funName
  callSite
  argExprs
  st0
  =
  mySequence (map symEvaluate argExprs) st0 `myBind_`
    \argValues
    st1@(SST
      { _pstate=pstate
      , _cstate=cstate
      , _prog=prog
      , _stack=stack
      , _pc=pc
      , _sky=sky
      , _funName=callerFunName
      }) ->
  let
    errorMsg = error $ "Call to undefined function " ++ funName
    (FunctionDefinition _ _ _ formalArgs openBracePos funBody _) = Map.findWithDefault errorMsg funName prog
    -- create a new PState from the actual arguments
    newPState = Map.fromList
      [ (argName,argVal)
      | ((FormalParameter _ argName), argVal) <- zip formalArgs argValues
      ]
    powDraw = curSymPowDraw cstate (getComponentFilter funName st1)
    startLine = unPos $ sourceLine openBracePos
  in
    -- execute the function body in the new PState, while saving the current pc
    -- and pstate on the stack
    [st1
      { _pstate = newPState
      , _pc = [funBody]
      , _funName = funName
      , _sky = [TStartPoint (startLine,powDraw)]
      , _stack = (mkSF pc pstate callerFunName callSite sky):stack
      }]


symCallComponent
  :: SourcePos
  -> String
  -> String
  -> SymInterpreterState
  -> [(SymExpr, SymInterpreterState)]
symCallComponent
  sourcePos
  compName
  compFunction
  st0@(SST{_cstate=cstates,_sky=sky,_funName=funName,_pathConstraint=pathConstraint}) =
  -- case needed because let wants polymorphism, which is not possible here
  case Map.lookup compName cstates of
    Just (SymComponent cState cDraw cTrans) ->
      let
        (_, retVal, retPathConstraint, newCState, st1) = cTrans compFunction cState st0
        newCStates = Map.insert compName (SymComponent newCState cDraw cTrans) cstates
        powDraw = curSymPowDraw newCStates (getComponentFilter funName st1)
        newSky = TEdge (unPos $ sourceLine sourcePos, powDraw) : sky
        newPathConstraint = partialEval $ SymBinaryOp And pathConstraint retPathConstraint
      in
        [(retVal, st1{_cstate=newCStates,_sky=newSky,_pathConstraint=newPathConstraint})]
    Nothing ->
      error ("component call (" <> sourcePosPretty sourcePos <> "): unknown component " <> compName)

-- Stateful evaluation of symbolic binary operators
-- Nests IO and lists in a way that makes your brain hurt
sequenceAndCombine
  :: SymExpr
  -> SymExpr
  -> SymInterpreterState
  -> (SymExpr -> SymExpr -> SymExpr)
  -> [(SymExpr, SymInterpreterState)]
sequenceAndCombine l r st0 combiner =
    [ (partialEval $ combiner lhs rhs, st2)
    | (lhs, st1) <- symEvaluate l st0
    , (rhs, st2) <- symEvaluate r st1
    ]

sequenceAndCombine_
  :: SymExpr
  -> SymExpr
  -> SymInterpreterState
  -> (SymExpr -> SymExpr -> SymExpr)
  -> [(SymExpr, SymInterpreterState)]
sequenceAndCombine_ l r st0 combiner =
  (symEvaluate l st0) `myBind` \lhs st1 ->
  (symEvaluate r st1) `myBind` \rhs st2 ->
  [(partialEval $ combiner lhs rhs, st2)]

symEvaluate :: SymExpr -> SymInterpreterState -> [(SymExpr, SymInterpreterState)]
symEvaluate b@(SymBoolConst _) st = [(b, st)]
symEvaluate i@(SymIntConst _) st = [(i, st)]
symEvaluate (SymVar v) st = [(lookupVar v st, st)]
symEvaluate (SymBinaryOp Add l r) st0 =
  sequenceAndCombine l r st0 (SymBinaryOp Add)
symEvaluate (SymBinaryOp Subtract l r) st0 =
  sequenceAndCombine l r st0 (SymBinaryOp Subtract)
symEvaluate (SymBinaryOp Multiply l r) st0 =
  sequenceAndCombine l r st0 (SymBinaryOp Multiply)
symEvaluate (SymBinaryOp Modulo l r) st0 =
  sequenceAndCombine l r st0 (SymBinaryOp Modulo)
symEvaluate (SymBinaryOp LessEq l r) st0 =
  sequenceAndCombine l r st0 (SymBinaryOp LessEq)
symEvaluate (SymBinaryOp Less l r) st0 =
  sequenceAndCombine l r st0 (SymBinaryOp Less)
symEvaluate (SymBinaryOp Eq l r) st0 =
  sequenceAndCombine l r st0 (SymBinaryOp Eq)
symEvaluate (SymBinaryOp And l r) st0 =
  sequenceAndCombine l r st0 (SymBinaryOp And)
symEvaluate (SymBinaryOp Or l r) st0 =
  sequenceAndCombine l r st0 (SymBinaryOp Or)
symEvaluate (SymUnaryOp Neg e) st0 =
  symEvaluate e st0 `myBind` \result st1 ->
  [(- result, st1)]
symEvaluate (SymUnaryOp Not e) st0 =
  symEvaluate e st0 `myBind` \result st1 ->
  [(partialEval $ SymUnaryOp Not $ result, st1)]
symEvaluate (SymFuncCall funName argExprs callSite) st0 =
  -- create stack frame for function call
  -- Beware of mindfuck: we have to empty _pc so that the call to symExecute
  -- stops when the function returns. We also empty _stack, so that symExecute can identify
  -- functions without return statements
  symCallFunction funName callSite argExprs (st0 {_pc=[],_stack=[]}) `myBind__` \st1 ->

  -- interpret function call
  symExecute st1 `myBind__` \st2 ->

  -- return value
    let retVal = lookupVar "#return" st2
    -- restore the original _pc so that execution continues as before
    in [(retVal, st2 {_pc=_pc st0, _stack=_stack st0} )]
symEvaluate (SymCompCall sourcePos compName compFunction) st0 =
  symCallComponent sourcePos compName compFunction st0
symEvaluate (SymAny _) _ = error "symEvaluate SymAny should never happen, because it is not part of the syntax."

module ChartUtil where

import Graphics.Rendering.Chart.Easy
import Data.Colour.SRGB

import Types

-- needs type signature because of overloading of fromIntegral
nats :: [Int]
nats = [0..]

-- Cyclic list of colors, for the different paths
colorCycle :: [SkylineColor]
colorCycle = cycle trubetskoy


fromHex :: String -> SkylineColor
fromHex = opaque . sRGB24read


-- source: https://github.com/Gnuplotting/gnuplot-palettes
set1 :: [SkylineColor]
set1 = map fromHex
  [ "#E41A1C"
  , "#377EB8"
  , "#4DAF4A"
  , "#984EA3"
  , "#FF7F00"
  , "#FFFF33"
  , "#A65628"
  , "#F781BF"
  ]


-- source: https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/
trubetskoy :: [SkylineColor]
trubetskoy = map fromHex
  [ "#e6194b" -- Red
  , "#3cb44b" -- Green
  , "#4363d8" -- Blue
  , "#f58231" -- Orange
  , "#911eb4" -- Purple
  , "#46f0f0" -- Cyan
  , "#f032e6" -- Magenta
  , "#ffe119" -- Yellow
  , "#bcf60c" -- Lime
  , "#fabebe" -- Pink
  , "#008080" -- Teal
  , "#e6beff" -- Lavender
  , "#9a6324" -- Brown
  , "#fffac8" -- Beige
  , "#800000" -- Maroon
  , "#aaffc3" -- Mint
  , "#808000" -- Olive
  , "#ffd8b1" -- Apricot
  , "#000075" -- Navy
  , "#808080" -- Gray
  -- , "#ffffff" -- White
  , "#000000" -- Black
  ]

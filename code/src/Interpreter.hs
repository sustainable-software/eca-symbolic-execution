{-# LANGUAGE NamedFieldPuns, MultiParamTypeClasses #-}
module Interpreter where

import qualified Data.Map as Map
import Data.Map ((!))
import Text.Megaparsec

import AST
import Skyline
import Component
import Util
import InterpreterUtil

data StackFrame = SF
  { _callerPC :: ProgramCounter
  , _callerPState :: PState
  , _callerFunName :: String
  , _callSite :: SourcePos
  , _callerSky :: TempSkyline
  }
  deriving (Eq)

instance Show StackFrame where
  show SF{_callerFunName} = _callerFunName

mkSF
  :: ProgramCounter
  -> PState
  -> String
  -> SourcePos
  -> TempSkyline
  -> StackFrame
mkSF callerPC callerPState callerFunName sourcePos callerSky = SF
  { _callerPC = callerPC
  , _callerPState = callerPState
  , _callerFunName = callerFunName
  , _callSite = sourcePos
  , _callerSky = callerSky
  }

data InterpreterState = ST
  { _pc :: ProgramCounter
  , _pstate :: PState -- local variables
  , _funName :: String -- name of currently executing function
  , _sky :: TempSkyline -- current skyline
  , _prog :: Program -- function definitions
  , _stack :: [StackFrame] -- call stack
  , _gstate :: PState -- global variables
  , _cstate :: CState -- component state; always global
  -- for every function: all skylines that occured during execution
  , _skies :: FunctionSkylines
  }

instance Environment InterpreterState Int where
  getPState (ST {_pstate=pstate}) = pstate
  getGState (ST {_gstate=gstate}) = gstate
  setPState st pstate = st { _pstate=pstate }
  setGState st gstate = st { _gstate=gstate }

mkST :: Stmt -> PState -> CState -> Program -> InterpreterState
mkST stmt pstate cstate prog = ST
  { _pc = [stmt]
  , _pstate = pstate
  , _funName = "main"
  , _sky = mempty
  , _prog = prog
  , _stack = []
  , _gstate = mempty
  , _cstate = cstate
  , _skies = mempty
  }

executeMain :: (Program,[GlobalVarDefinition]) -> CState -> IO (Int,InterpreterState)
executeMain (prog,globalVars) cstate = do
  let
    -- find the main function
    FunctionDefinition _ _ _ _ openBracePos body _ = prog ! "main"

    -- preliminary interpreter state, needed to bootstrap
    -- Okay, that's not true. It will be needed to bootstrap once we implement
    -- component filters for concrete execution
    st0 = (mkST body mempty cstate prog)

    powDraw = curPowDraw cstate Nothing
    startLine = unPos $ sourceLine openBracePos

    -- make global PState
    gstate = initGlobalPState globalVars mempty

    -- initial interpreter state
    st = st0
      { _stack = [mkSF [] mempty "#executeMain" (pos 0 0) mempty]
      , _gstate=gstate
      , _sky = [TStartPoint (startLine,powDraw)]
      }

  -- gogo!
  resultEst@(ST {_pstate=pstate}) <- execute st
  return $ (pstate ! "#return", resultEst)


initGlobalPState :: [GlobalVarDefinition] -> PState -> PState
initGlobalPState [] pstate = pstate
initGlobalPState (GlobalVarDefinition _ _ varName expr:rest) pstate =
  initGlobalPState rest (Map.insert varName (pureEval expr pstate) pstate)


execute :: InterpreterState -> IO InterpreterState
execute st@(ST{ _pc=[],_stack=[] }) = return st
-- This condition signals that we reached the end of a function but there was no return statement.
-- We assume that it must be a void function, and insert an artificial return statement, located at
-- the position of the closing brace of the function
execute st@(ST{ _pc=[],_stack=(_:_) }) = do
  let FunctionDefinition _ _ _ _ _ _ endPos = _prog st ! _funName st
  continuation <- step (Return endPos (IntConst 0)) st
  execute continuation
execute st@(ST{ _pc=(stmt:rest) }) = do
  continuation <- step stmt (st { _pc=rest } )
  execute continuation

step :: Stmt -> InterpreterState -> IO InterpreterState

step (Block stmts) st@(ST{_pc=pc}) = return (st { _pc = stmts <> pc })

step (Assign sourcePos var expr) st@(ST{_sky=sky}) = do
  (val,st1) <- evaluate expr (st {_sky=startSky})
  return $ assignVar var val st1
  where
  startSky = TFill (unPos $ sourceLine sourcePos) : sky

step (IfThenElse (SourcePos{sourceLine}) cond thenBranch elseBranch) st0 = do
  let
    newSky = TFill (unPos sourceLine) : (_sky st0)
  (condVal,st1) <- evaluate cond (st0{_sky=newSky})
  if semToBool condVal
    then return (st1 { _pc = thenBranch:_pc st1})
    else return (st1 { _pc = elseBranch:_pc st1})

step (While startPos cond body endPos numIterations) st0@(ST{_sky=sky0}) = do
  let
    firstTime = numIterations == 0
    startSky = if firstTime
      -- just fill until the condition
      then TFill (unPos $ sourceLine startPos) : sky0
      -- unconditional jump from the end of the loop to the condition
      else TJump (unPos $ sourceLine startPos) : TFill (unPos $ sourceLine endPos) : sky0
  (condVal,st1@(ST{_pc=pc})) <- evaluate cond st0{_sky=startSky}
  let
    loopContinuation = While startPos cond body endPos (numIterations+1)
  if semToBool condVal
    then return st1 { _pc = body:loopContinuation:pc }
    else return st1

step (Return (SourcePos{sourceLine=returnLine}) expr) st0@(ST{_sky=sky0}) = do
  -- indentation needed due to offside rule
  (   retVal
    , st1@(ST
        { _stack=(SF
            {_callerPC=callerPC
            ,_callerPState=callerPState
            ,_callerFunName=callerFunName
            ,_callSite=callSite
            ,_callerSky=callerSky
            }:frames)
        , _funName=funName
        , _sky=sky1
        , _skies=skies
        , _prog=prog
        , _cstate=cstate
        })
    ) <- evaluate expr st0{_sky=TFill(unPos returnLine):sky0}

  let
    (FunctionDefinition _ _ _ _ _ _ endPos) = (prog!funName)
    newSky = TFill (unPos $ sourceLine endPos) : sky1
    powDraw = curPowDraw cstate Nothing
    newCallerSky = TEdge (unPos $ sourceLine callSite, powDraw) : callerSky

  -- pop the topmost item from the call stack
  -- assign the return value to the return register in the caller's PState
  -- restore the call stack
  return $ st1
    { _pstate = Map.insert "#return" retVal callerPState
    , _pc = callerPC
    , _funName = callerFunName
    , _sky = newCallerSky
    , _stack = frames
    , _skies = Map.alter (addSky newSky) funName skies
    }
  where
    -- Extend the current skyline to the closing brace of the function definition.
    -- The idea is that `return` goes to the end of the function
    addSky sky Nothing = Just [sky]
    addSky sky (Just otherSkies) = Just (sky:otherSkies)

step (ExprStmt e) st = snd <$> evaluate e st

callFunction :: String -> SourcePos -> [Expr] -> InterpreterState -> IO InterpreterState
callFunction
  funName
  callSite
  argExprs
  st0 = do
  (argValues, st1@(ST
    { _pstate=pstate
    , _cstate=cstate
    , _prog=prog
    , _stack=stack
    , _pc=pc
    , _sky=sky
    , _funName=callerFunName })) <- mySequence (map evaluate argExprs) st0
  let
    errorMsg = error $ "Call to undefined function " ++ funName
    (FunctionDefinition _ _ _ formalArgs openBracePos funBody _) = Map.findWithDefault errorMsg funName prog
    -- create a new PState from the actual arguments
    -- TODO: parameters can contain function calls. Use evaluate instead of pureEval.
    newPState = Map.fromList
      [ (argName,argVal)
      | ((FormalParameter _ argName), argVal) <- zip formalArgs argValues
      ]
    powDraw = curPowDraw cstate Nothing
    startLine = unPos $ sourceLine openBracePos
    -- execute the function body in the new PState, while saving the current pc
    -- and pstate on the stack
  return st1
    { _pstate = newPState
    , _pc = [funBody]
    , _funName = funName
    , _sky = [TStartPoint (startLine,powDraw)]
    , _stack = mkSF pc pstate callerFunName callSite sky:stack
    }

-- TODO: there should be some monad transformer stack to do that
mySequence
  :: [InterpreterState -> IO (a, InterpreterState)]
  -> InterpreterState
  -> IO ([a], InterpreterState)
mySequence [] st = return ([], st)
mySequence (f:fs) st0 = do
  (a, st1) <- f st0
  (as, st2) <- mySequence fs st1
  return (a:as, st2)

callComponent :: Pos -> String -> String -> InterpreterState -> IO (Int, InterpreterState)
callComponent
  sourceLine compName compFunction
  st@(ST{_cstate=cstate,_sky=sky})
  =
  -- case needed because let wants polymorphism, which is not possible here
  case cstate ! compName of
    (Component cState cDraw cTrans) -> do
      (_, compRetVal, newCState) <- cTrans compFunction cState
      let
        newCStates = Map.insert compName (Component newCState cDraw cTrans) cstate
        powDraw = curPowDraw newCStates Nothing
        newSky = TEdge (unPos sourceLine, powDraw) : sky
        result = (compRetVal, st{_cstate=newCStates,_sky=newSky})
      return result

evaluate :: Expr -> InterpreterState -> IO (Int, InterpreterState)
evaluate (BoolConst b) st = return (boolToSem b, st)
evaluate (IntConst i) st = return (i, st)
evaluate (Var v) st = return (lookupVar v st, st)
evaluate (BinaryOp Add l r) st0 = do
  (lhs,st1) <- (evaluate l st0)
  (rhs,st2) <- (evaluate r st1)
  return (lhs + rhs, st2)
evaluate (BinaryOp Subtract l r) st0 = do
  (lhs,st1) <- (evaluate l st0)
  (rhs,st2) <- (evaluate r st1)
  return (lhs - rhs, st2)
evaluate (BinaryOp Multiply l r) st0 = do
  (lhs,st1) <- (evaluate l st0)
  (rhs,st2) <- (evaluate r st1)
  return (lhs * rhs, st2)
evaluate (BinaryOp Modulo l r) st0 = do
  (lhs,st1) <- (evaluate l st0)
  (rhs,st2) <- (evaluate r st1)
  return (lhs `mod` rhs, st2)
evaluate (BinaryOp LessEq l r) st0 = do
  (lhs,st1) <- (evaluate l st0)
  (rhs,st2) <- (evaluate r st1)
  return (boolToSem $ lhs <= rhs, st2)
evaluate (BinaryOp Less l r) st0 = do
  (lhs,st1) <- (evaluate l st0)
  (rhs,st2) <- (evaluate r st1)
  return (boolToSem $ lhs < rhs, st2)
evaluate (BinaryOp Eq l r) st0 = do
  (lhs,st1) <- (evaluate l st0)
  (rhs,st2) <- (evaluate r st1)
  return (boolToSem $ lhs == rhs, st2)
evaluate (BinaryOp And l r) st0 = do
  (lhs,st1) <- (evaluate l st0)
  (rhs,st2) <- (evaluate r st1)
  return (boolToSem $ (semToBool $ lhs) && (semToBool $ rhs), st2)
evaluate (BinaryOp Or l r) st0 = do
  (lhs,st1) <- (evaluate l st0)
  (rhs,st2) <- (evaluate r st1)
  return (boolToSem $ (semToBool $ lhs) || (semToBool $ rhs), st2)
evaluate (UnaryOp Neg e) st0 = do
  (result,st1) <- evaluate e st0
  return (- result, st1)
evaluate (UnaryOp Not e) st0 = do
  (result,st1) <- evaluate e st0
  return (boolToSem $ not $ semToBool $ result, st1)
evaluate (FuncCall funName argExprs callSite) st0 = do
  st1 <- callFunction funName callSite argExprs (st0{_pc=[],_stack=[]})
  st2 <- execute st1
  let retVal = lookupVar "#return" st2
  return (retVal, st2{ _pc=_pc st0, _stack=_stack st0 })
evaluate (CompCall (SourcePos {sourceLine}) compName compFunction) st0 =
  callComponent sourceLine compName compFunction st0

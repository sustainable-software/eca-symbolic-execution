{-# LANGUAGE ExistentialQuantification, RecordWildCards #-}
module Component where

import Data.Map (Map,(!))
import qualified Data.Map as Map

import Types

data Component = forall s . Component
  { compState :: s
  , compPowerDraw :: s -> PowerDraw
  , compTransition :: String -> s -> IO (EnergyConsumption, Int, s)
  }

type CState = (Map String Component)

curPowDraw :: CState -> Maybe [String] -> Int
curPowDraw cstate Nothing = sum [cDraw_ cState_ | (Component cState_ cDraw_ _) <- Map.elems cstate]
curPowDraw cstate (Just desiredComponentNames) =
  sum [cDraw_ cState_ | (Component cState_ cDraw_ _) <- desiredCompontents]
  where
  desiredCompontents = fmap (cstate!) desiredComponentNames

module ExampleComponents where

import qualified Data.Map as Map

import System.IO
import Component
import SymComponent
import AST
import SymAST

data LedState = SwitchedOn | SwitchedOff | PowerSave
  deriving (Show, Eq)

-- A led is a simple component with two states: on and off. It is parameterized by a powerDraw. When
-- in state SwitchedOn, it uses powerDraw, otherwise 0.
led :: Int -> Component
led powerDraw = Component
  { compState = SwitchedOff
  , compPowerDraw = \s -> case s of
      SwitchedOff -> 0
      SwitchedOn -> powerDraw
      _ -> error "led: unsupported state"
  , compTransition = \transition -> \s -> case (s, transition) of
      (SwitchedOff, "switchOn")  -> return (0, 0, SwitchedOn)
      (SwitchedOff, "switchOff") -> return (0, 0, SwitchedOff)
      (SwitchedOn,  "switchOn")  -> return (0, 0, SwitchedOn)
      (SwitchedOn,  "switchOff") -> return (0, 0, SwitchedOff)
      _ -> error "led: unsupported transition"
  }


-- A LED with power save mode
ledSave :: Int -> Int -> Component
ledSave powerDraw powerSave = Component
  { compState = SwitchedOff
  , compPowerDraw = \s -> case s of
      SwitchedOff -> 0
      SwitchedOn -> powerDraw
      PowerSave -> powerSave
  , compTransition = \transition -> \s -> case (s, transition) of
      (_, "switchOn")  -> return (0, 0, SwitchedOn)
      (_, "switchOff") -> return (0, 0, SwitchedOff)
      (_, "powerSave") -> return (0, 0, PowerSave)
      _ -> error "led: unsupported transition"
  }


-- A sensor returns a value from the transition function.
-- This one is a sensor that alternatingly returns one of the given values
sensor :: Int -> Int -> Component
sensor valA valB = Component
  { compState = (0 :: Int)
  , compPowerDraw = \_ -> 0
  , compTransition = \transition -> \s -> case (s, transition) of
      (0, _) -> return (0, valA, 1)
      (1, _) -> return (0, valB, 0)
      _ -> error "sensor: unsupported transition"
  }


counter :: Component
counter = Component
  { compState = (0 :: Int)
  , compPowerDraw = \s -> s
  , compTransition = \transition -> \s -> case (s, transition) of
      (n, "inc") -> return (0, n, n+1)
      (n, "get") -> return (0, n, n)
      _ -> error "counter: unsupported transition"
  }










-----------------------------------------------------------
-- For the line follower robot
-----------------------------------------------------------


data MotorState = MotorForward | MotorBackward | MotorStop

-- A motor can rotate forward and backward, and stop. It is parameterized by the power draw.
motor :: Int -> Component
motor powerDraw = Component
  { compState = MotorStop
  , compPowerDraw = \s -> case s of
      MotorForward -> powerDraw
      MotorBackward -> powerDraw
      MotorStop -> 0
  , compTransition = \transition -> \s -> case (s, transition) of
      (_, "Stop") -> return (0, 0, MotorStop)
      (_, "Forward") -> return (0, 0, MotorForward)
      (_, "Backward") -> return (0, 0, MotorBackward)
      (_, compCall) -> error ("motor: unsupported component call " ++ compCall)
  }








data WeirdState
  = Off
  | HighA | MidA | LowA
  | HighB | MidB | LowB
  deriving (Show, Eq)

wtf :: Component
wtf = Component
  { compState = Off
  , compPowerDraw = \s -> case s of
      Off -> 0
      HighA -> 10
      HighB -> 10
      MidA -> 5
      MidB -> 5
      LowA -> 1
      LowB -> 1
  , compTransition = \transition -> \s -> case (s, transition) of
      (Off, "descend") -> return (0, 0, HighA)
      (Off, "ascend") -> return (0, 0, LowB)

      (HighA, "next") -> return (0, 0, MidA)
      (MidA, "next") -> return (0, 0, LowA)

      (LowB, "next") -> return (0, 0, MidB)
      (MidB, "next") -> return (0, 0, HighB)

      _ -> return (0, 0, Off)
  }


terminal :: Component
terminal = Component
  { compState = ()
  , compPowerDraw = \_ -> 0
  , compTransition = \transition -> \_ -> case transition of
      "readInt" -> do
        putStr "? "
        hFlush stdout
        val <- (read <$> getLine :: IO Int)
        return (0,val,())
      _ -> error "terminal: unsupported transition"
  }


clock :: Component
clock = Component
  { compState = ()
  , compPowerDraw = \_ -> 0
  , compTransition = \transition -> \_ -> case transition of
      "getTime" -> do
        putStr "? "
        hFlush stdout
        val <- (read <$> getLine :: IO Int)
        return (0,val,())
      _ -> error "clock: unsupported transition"
  }

symTerminal :: SymComponent
symTerminal = SymComponent
  { symCompState = ()
  , symCompPowerDraw = \_ -> 0
  , symCompTransition = \transition -> \s -> \st0 -> case transition of
      "readInt" ->
        let
          (symAny, st1) = getNextAny st0
        in
          (0, symAny, SymBoolConst True, s, st1)
      _ -> error "terminal: unsupported transition"
  }

symClock :: SymComponent
symClock = SymComponent
  { symCompState = ()
  , symCompPowerDraw = \_ -> 0
  , symCompTransition = \transition -> \s -> \st0 -> case transition of
      "getTime" ->
        let
          (symAny, st1) = getNextAny st0
        in
          (0, symAny, SymBoolConst True, s, st1)
      _ -> error "clock: unsupported transition"
  }



defaultComponents :: CState
defaultComponents =
  Map.fromList
    [("LED1", led 10)
    ,("LED2", led 7)
    ,("LED3", led 5)
    ,("LED4", ledSave 10 3)
    ,("WTF", wtf)
    ,("SENS1", sensor 101 5)
    ,("TERM", terminal)
    ,("CLOCK", clock)
    ,("MotorLeft", motor 750)
    ,("MotorRight", motor 750)
    ,("SensorLeft", sensor 0 1)
    ,("SensorRight", sensor 0 1)
    ]


-- Symbolic sensor sense returns a symbolic value between low and high
symSensor :: Int -> Int -> SymComponent
symSensor low high = SymComponent
  { symCompState = ()
  , symCompPowerDraw = \_ -> 0
  , symCompTransition =
      \transition s st0 -> case transition of
        _ ->
          let
            (symAny, st1) = getNextAny st0
          in
            (0, symAny, symBetween (SymIntConst low) symAny (SymIntConst high), s, st1)
  }

-- Constructs a SymExpr that expresses that for given low e high, low <= e && e <= high
symBetween :: SymExpr -> SymExpr -> SymExpr -> SymExpr
symBetween low e high = SymBinaryOp And lowerBound upperBound
  where
  lowerBound = SymBinaryOp LessEq low e
  upperBound = SymBinaryOp LessEq e high

defaultSymComponents :: SCState
defaultSymComponents =
  Map.fromList
    [("LED", unsafeCompToSymComp $ led 10)
    ,("LED1", unsafeCompToSymComp $ led 10)
    ,("LED2", unsafeCompToSymComp $ led 7)
    ,("LED3", unsafeCompToSymComp $ led 5)
    ,("LED4", unsafeCompToSymComp $ ledSave 10 3)
    ,("WTF", unsafeCompToSymComp $ wtf)
    ,("SENS1", unsafeCompToSymComp $ sensor 101 5)

    ,("MotorLeft", unsafeCompToSymComp $ motor 750)
    ,("MotorRight", unsafeCompToSymComp $ motor 750)
    ,("SensorLeft", symSensor 0 1)
    ,("SensorRight", symSensor 0 1)
    ]
  <>
  Map.fromList
    [ ("SENS", symSensor 5 20)
    , ("TERM", symTerminal)
    , ("CLOCK", symClock)
    ]

module Util where

import Text.Megaparsec

dummyFileName :: String
dummyFileName = "dummyFileName"

pos :: Int -> Int -> SourcePos
pos line col = SourcePos
  { sourceName = dummyFileName
  , sourceLine = mkPos line
  , sourceColumn = mkPos col
  }

module Parser where

-- Inspired by
-- https://markkarpov.com/megaparsec/parsing-simple-imperative-language.html

import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import Data.Void
import Control.Monad.Combinators.Expr
import qualified Data.Map as Map

import AST

type Parser = Parsec Void String

parseEca :: String -> FilePath -> Either String (Program, [GlobalVarDefinition])
parseEca input fileName = case runParser pProgram fileName input of
  Right a -> Right a
  Left err -> Left $ errorBundlePretty err

-- space consumer
sc :: Parser ()
sc = L.space space1 lineCmnt blockCmnt
  where
    lineCmnt  = L.skipLineComment "//"
    blockCmnt = L.skipBlockComment "/*" "*/"


lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc


symbol :: String -> Parser String
symbol = L.symbol sc


parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")


commaSep :: Parser a -> Parser [a]
commaSep p = p `sepBy` symbol ","


integer :: Parser Int
integer = fromInteger <$> lexeme L.decimal


semicolon :: Parser String
semicolon = symbol ";"


-- reserved word
rword :: String -> Parser ()
rword w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)

reservedWords :: [String] -- list of reserved words
reservedWords = ["if","else","while","true","false","int","bool","void","return"]

identifier :: Parser String
identifier = (lexeme . try) (p >>= check)
  where
    p       = (:) <$> letterChar <*> many alphaNumChar
    check x = if x `elem` reservedWords
                then fail $ "keyword " ++ show x ++ " cannot be an identifier"
                else return x






operators :: [[Operator Parser Expr]]
operators =
  [ [ Prefix (UnaryOp Neg <$ symbol "-")
    , Prefix (UnaryOp Not <$ symbol "!")
    ]
  , [ InfixL (BinaryOp Multiply <$ symbol "*")
    -- , InfixL (BinaryOp Divide   <$ symbol "/")
    , InfixL (BinaryOp Modulo   <$ symbol "%")
    ]
  , [ InfixL (BinaryOp Add      <$ symbol "+")
    , InfixL (BinaryOp Subtract <$ symbol "-")
    ]
  , [ InfixN (BinaryOp LessEq <$ symbol "<=")
    , InfixN (BinaryOp Less   <$ symbol "<")
    , InfixN (BinaryOp Eq     <$ symbol "==")
    ]
  , [ InfixL (BinaryOp And <$ symbol "&&")
    ]
  , [ InfixL (BinaryOp Or <$ symbol "||")
    ]
  ]




term :: Parser Expr
term =
      parens pExpr
  <|> try (FuncCall
        <$> identifier
        <* symbol "("
        <*> commaSep pExpr
        <*> getSourcePos
        <* symbol ")")
  <|> try (CompCall
        <$> getSourcePos
        <*> identifier
        <* symbol "."
        <*> identifier
        <* symbol "(" <* symbol ")")
  <|> Var <$> identifier
  <|> IntConst <$> integer
  <|> (BoolConst True  <$ rword "true")
  <|> (BoolConst False <$ rword "false")


pExpr :: Parser Expr
pExpr = makeExprParser term operators


pProgram :: Parser (Program, [GlobalVarDefinition])
pProgram = do
  defs <- between sc eof (some pDefinition)
  let
    prog = Map.fromList [(name,def) | FunDef def@(FunctionDefinition _ _ name _ _ _ _) <- defs]
    vars = [def | VarDef def@(GlobalVarDefinition _ _ _ _) <- defs]
  return (prog,vars)

pStmt :: Parser Stmt
pStmt =
      -- assignments and expressions can both start with an identifier. We need to create a
      -- backtracking point with try
      try pAssignment
  <|> ExprStmt <$> pExpr <* symbol ";"
  <|> try (IfThenElse
        <$> getSourcePos
        <*  rword "if"
        <*> parens pExpr
        <*> pStmt
        <*  rword "else"
        <*> pStmt)
  <|> IfThenElse
        <$> getSourcePos
        <*  rword "if"
        <*> parens pExpr
        <*> pStmt
        <*> pure (Block [])
  <|> Block <$> between (symbol "{") (symbol "}") (many pStmt)
  <|> While
        <$> getSourcePos
        <* rword "while"
        <*> parens pExpr
        <* symbol "{"
        <*> (Block <$> (many pStmt))
        <*> getSourcePos
        <* symbol "}"
        <*> pure 0
  <|> Return <$> getSourcePos
        <* rword "return"
        <*> pExpr
        <* semicolon

pAssignment :: Parser Stmt
pAssignment =
      Assign <$> getSourcePos <*> identifier <* symbol "=" <*> pExpr <* semicolon
  -- what looks like a definition with initializer is just an assignment in disguise. We ignore the
  -- type
  <|> Assign <$ pType <*> getSourcePos <*> identifier <* symbol "=" <*> pExpr <* semicolon


pDefinition :: Parser Definition
pDefinition =
  try (FunDef <$> (FunctionDefinition
    <$> pReturnType
    <*> getSourcePos
    <*> identifier
    <*> parens (commaSep pParameter)
    <*> getSourcePos
    <* symbol "{"
    <*> (Block <$> many pStmt)
    <*> getSourcePos
    <* symbol "}"))
  <|> (VarDef <$> (GlobalVarDefinition
    <$> pType
    <*> getSourcePos
    <*> identifier
    <* symbol "="
    <*> pExpr
    <* semicolon))

pParameter :: Parser FormalParameter
pParameter = FormalParameter <$> pType <*> identifier


pType :: Parser AstType
pType =
      TypeInt <$ rword "int"
  <|> TypeBool <$ rword "bool"

pReturnType :: Parser AstType
pReturnType =
      pType
  <|> TypeVoid <$ rword "void"

module Options where

import Data.Default.Class
import System.Console.GetOpt
import Graphics.Rendering.Chart.Backend.Cairo

data Mode
  = ExecuteConcrete FilePath
  | ExecuteSymbolic FilePath
  | Help
  | Foobar String
  deriving (Show,Eq)

data Options = Options
  { mode :: Mode
  , outputDir :: FilePath
  , outputSuffix :: Maybe String
  , outputSizeX :: Int
  , outputSizeY :: Int
  , showLegend :: Bool
  , fileFormat :: (FileFormat, String)
  , showTitle :: Bool
  , formulasAsLatex :: Bool
  , filterComponents :: Bool
  , showReturnValues :: Bool
  , showFinalSkylineFragments :: Bool
  , debug :: Bool
  , xAxisLabels :: Maybe [Double]
  , yAxisLabels :: Maybe [Double]
  , yAxisLabelPrefix :: String
  , merge :: Bool
  , skylineFragmentLimit :: Maybe Int
  , xAxisBounds :: Maybe (Double, Double)
  , yAxisBounds :: Maybe (Double, Double)
  , xAxisLabelGap :: Int
  , yAxisLabelGap :: Int
  , axisFontName :: String
  , axisFontSize :: Double
  , maxIterations :: Integer
  , margin :: Double
  , xAxisTitle :: String
  , yAxisTitle :: String
  , exportJson :: Maybe FilePath
  }

instance Default Options where
  def = Options
    { mode=Help
    , outputDir="out"
    , outputSuffix=Nothing
    , outputSizeX = 600
    , outputSizeY = 800
    , showLegend=True
    , fileFormat=parseFileFormat "SVG"
    , showTitle=True
    , formulasAsLatex = False
    , filterComponents = False
    , showReturnValues = False
    , showFinalSkylineFragments = False
    , debug = False
    , xAxisLabels = Nothing
    , yAxisLabels = Nothing
    , yAxisLabelPrefix = ""
    , merge = True
    , skylineFragmentLimit = Nothing
    , xAxisBounds = Nothing
    , yAxisBounds = Nothing
    , xAxisLabelGap = 10 -- default in Charts
    , yAxisLabelGap = 10
    , axisFontName = "Latin Modern Roman"
    , axisFontSize = 11.0
    , maxIterations = 3
    , margin = 10
    , xAxisTitle = ""
    , yAxisTitle = ""
    , exportJson = Nothing
    }

optDescr :: [OptDescr (Options -> Options)]
optDescr =
  [ Option ['s'] ["symbolic"] (ReqArg (\f o -> o { mode = ExecuteSymbolic f}) "FILE")
      "symbolic execution"

  , Option ['c'] ["concrete"] (ReqArg (\f o -> o { mode = ExecuteConcrete f}) "FILE")
      "concrete execution"

  , Option ['o'] ["output-suffix"] (ReqArg (\f o -> appendOutputSuffix f o) "String")
      "modify output file name\nexample:\neca --output-suffix blah -s program.eca\nproduces programblah.svg\nMultiple occurances of this option are accumulative"
  , Option [] ["output-dir"] (ReqArg (\f o -> o { outputDir = f}) "DIR")
      "directory where to put generated files"

  , Option ['h'] ["help"] (NoArg (\o -> o { mode = Help }))
      "display this help text"

  , Option [] ["width"] (ReqArg (\w o -> o { outputSizeX = (read w) }) "NUMBER")
      "width of result image"

  , Option [] ["height"] (ReqArg (\h o -> o { outputSizeY = (read h) }) "NUMBER")
      "height of result image"

  , Option [] ["file-format"] (ReqArg (\ff o -> o { fileFormat = parseFileFormat ff }) "FILEFORMAT")
      "PNG, SVG, PS, or PDF (as described in the Charts-cairo backend library)"

  , Option [] ["no-title"] (NoArg (\o -> o { showTitle = False }))
      "don't include a title at the top"

  , Option [] ["no-legend"] (NoArg (\o -> o { showLegend = False }))
      "don't plot legend for skylines of symbolic execution"

  , Option [] ["no-merge"] (NoArg (\o -> o { merge = False }))
      "don't merge skylines for symbolic execution"

  , Option [] ["latex"] (NoArg (\o -> o { formulasAsLatex = True }))
      "print logical formulas as latex math"

  , Option [] ["show-return-values"] (NoArg (\o -> o { showReturnValues = True }))
      "print return values of main function"

  , Option [] ["show-skyline-fragments"] (NoArg (\o -> o { showFinalSkylineFragments = True }))
      "print skyline fragments"

  , Option [] ["debug"] (NoArg (\o -> o { debug = True }))
      "debug mode"

  , Option [] ["x-labels"] (ReqArg (\labels o -> o { xAxisLabels = Just (read labels) }) "[n, ...]")
      "List of positions where labels should be drawn on the x axis"

  , Option [] ["y-labels"] (ReqArg (\labels o -> o { yAxisLabels = Just (read labels) }) "[n, ...]")
      "List of positions where labels should be drawn on the y axis"

  , Option [] ["y-label-prefix"] (ReqArg (\prefix o -> o { yAxisLabelPrefix = prefix }) "String")
      "A string to prefix all y-axis labels. Only used in conjunction with --y-labels. Can be used to put some space between axis title and labels."

  , Option [] ["x-bounds"] (ReqArg (\bounds o -> o { xAxisBounds = Just (read bounds) }) "(Double,Double)")
      "Bounds of the x-axis."

  , Option [] ["y-bounds"] (ReqArg (\bounds o -> o { yAxisBounds = Just (read bounds) }) "(Double,Double)")
      "Bounds of the y-axis."

  , Option [] ["x-label-gap"] (ReqArg (\gap o -> o { xAxisLabelGap = read gap }) "Int")
      "Gap between labels and x-axis."

  , Option [] ["y-label-gap"] (ReqArg (\gap o -> o { yAxisLabelGap = read gap }) "Int")
      "Gap between labels and y-axis."

  , Option [] ["x-axis-title"] (ReqArg (\s o -> o { xAxisTitle = s }) "String")
      "Title of the x axis"

  , Option [] ["y-axis-title"] (ReqArg (\s o -> o { yAxisTitle = s }) "String")
      "Title of the y axis"

  , Option [] ["margin"] (ReqArg (\n o -> o { margin = read n }) "Double")
      "Margin at the border of diagrams"

  , Option [] ["font-name"] (ReqArg (\n o -> o { axisFontName = n }) "String")
      "font to use for ticks and axis labels"

  , Option [] ["font-size"] (ReqArg (\n o -> o { axisFontSize = read n }) "Int")
      "font size to use for ticks and axis labels"

  , Option [] ["max-iterations"] (ReqArg (\iter o -> o { maxIterations = read iter }) "Int")
      "Loop bound for symbolic execution."

  , Option [] ["filter-components"]
      (NoArg (\o -> o { filterComponents = True }))
      "compute power draw only for components used in a function"

  , Option [] ["fragment-limit"]
      (ReqArg (\l o -> o { skylineFragmentLimit = Just (read l)}) "NUMBER")
      "Limit skyline drawing to the first initial fragments.\nOnly for unmerged skylines.\nFor debugging and internal use."

  , Option [] ["export-json"]
      (ReqArg (\f o -> o { exportJson = Just f }) "FILE")
      "Instead of generating a chart, export merged skyline to a json file FILE"

  , Option [] ["foobar"] (ReqArg (\s o -> o { mode = Foobar s}) "String")
      "for internal use"
  ]

appendOutputSuffix :: String -> Options -> Options
appendOutputSuffix suffix o@Options{ outputSuffix = Nothing } = o { outputSuffix = Just suffix }
appendOutputSuffix suffix o@Options{ outputSuffix = Just oldSuffix } = o { outputSuffix = Just (oldSuffix <> suffix) }

parseFileFormat :: String -> (FileFormat, String)
parseFileFormat ff
  | ff == "PNG" = (PNG, "png")
  | ff == "SVG" = (SVG, "svg")
  | ff == "PS"  = (PS, "ps")
  | ff == "PDF" = (PDF, "pdf")
  | otherwise = error $ "unrecognized file format " <> ff

header :: String
header = "Usage: eca [OPTIONS]"

parseOptions :: [String] -> IO Options
parseOptions [] = ioError (userError (usageInfo header optDescr))
parseOptions args = case getOpt Permute optDescr args of
  (o,(_),[]) -> do
    return $ foldl (flip id) def o
  (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header optDescr))


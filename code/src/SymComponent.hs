{-# LANGUAGE ExistentialQuantification, RecordWildCards, MultiParamTypeClasses #-}
module SymComponent where

import Text.Megaparsec
import qualified Data.Map as Map
import Data.Map (Map,(!))
import System.IO.Unsafe

import Component
import Types
import AST
import SymAST
import InterpreterUtil
import Skyline
import Options

data SymComponent = forall s . SymComponent
  { symCompState :: s
  , symCompPowerDraw :: s -> PowerDraw
  , symCompTransition
      :: String
      -> s
      -> SymInterpreterState -- for generating SymAnys if needed
      -> ( EnergyConsumption
         , SymExpr -- result value
         , SymExpr -- constraint on result value
         , s -- result state
         , SymInterpreterState
         )
  }

type SCState = (Map String SymComponent)

data SymStackFrame = SF
  { _callerPC :: ProgramCounter
  , _callerPState :: SymPState
  , _callerFunName :: String
  , _callSite :: SourcePos
  , _callerSky :: TempSkyline
  }

data SymInterpreterState = SST
  { _pc :: ProgramCounter
  , _pstate :: SymPState -- local variables
  , _funName :: String -- name of currently executing function
  , _sky :: TempSkyline -- current skyline
  , _prog :: Program -- function definitions
  , _stack :: [SymStackFrame] -- call stack
  , _gstate :: SymPState -- global variables
  , _cstate :: SCState -- symbolic component state; always global
  -- for every function: all skylines that occured during execution
  , _skies :: FunctionSkylines
  , _pathConstraint :: PathConstraint
  , _nextAny :: Int
  , _opts :: Options
  , _funComponents :: Map String [String] -- used components by each function
  , _maxIterations :: Integer
  }

instance Environment SymInterpreterState SymExpr where
  getPState (SST {_pstate=pstate}) = pstate
  getGState (SST {_gstate=gstate}) = gstate
  setPState st pstate = st { _pstate=pstate }
  setGState st gstate = st { _gstate=gstate }

getNextAny :: SymInterpreterState -> (SymExpr, SymInterpreterState)
getNextAny st0@(SST{_nextAny=nextAny}) = (retVal,st1)
  where
  retVal = SymAny nextAny
  st1 = st0 {_nextAny = nextAny+1}

-- Every component is also a symbolic component that returns constant values
-- This function can only be used on components that don't actually perform IO!
unsafeCompToSymComp :: Component -> SymComponent
unsafeCompToSymComp (Component {..}) = SymComponent
  { symCompState = compState
  , symCompPowerDraw = compPowerDraw
  , symCompTransition = \func -> \s -> \st ->
      let
        (energyConsumption, retVal, s_) = unsafePerformIO $ compTransition func s
      in
        (energyConsumption, SymIntConst retVal, SymBoolConst True, s_, st)
  }

curSymPowDraw :: SCState -> Maybe [String] -> Int
curSymPowDraw cstate Nothing =
  sum [cDraw_ cState_ | (SymComponent cState_ cDraw_ _) <- Map.elems cstate]
curSymPowDraw cstate (Just desiredComponentNames) =
  sum [cDraw_ cState_ | (SymComponent cState_ cDraw_ _) <- desiredCompontents]
  where
  desiredCompontents = fmap (cstate!) desiredComponentNames

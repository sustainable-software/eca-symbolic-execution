{-# LANGUAGE RecordWildCards, DeriveGeneric, OverloadedStrings #-}
module FragmentMeta where

import Control.DeepSeq
import Data.Aeson

import Data.Map (Map)
import Data.List (nub)

import Types
import Skyline

-- Continuations refer to indexes in a FragmentDB
-- A continuation has a maybe direct continuation, and a list of indirect continuations
type Continuation = (Int, [(Int,(ProgramPoint,PowerDraw))])

-- A fragment together with some meta information
data FragmentMeta
  = FragmentMeta
  { fragment :: Fragment
  , continuation :: Continuation
  , color :: Maybe SkylineColor
  , offset :: Int
  }
  deriving (Show,Eq)

-- We have to write this by hand because AlphaColour does not have a Generics
-- instance?? Not sure.
instance ToJSON FragmentMeta where
  toEncoding (FragmentMeta {..}) = pairs
    (  "fragment" .= fragment
    <> "continuation" .= fmap nub continuation
    <> "color" .= color
    <> "offset" .= offset
    )
  toJSON (FragmentMeta {..}) = object
    [ "fragment" .= fragment
    , "continuation" .= fmap nub continuation
    , "color" .= color
    , "offest" .= offset
    ]

instance NFData FragmentMeta where
  -- For the merge algorithm, we only need to deepseq fragment and continuation
  rnf FragmentMeta{..} = fragment `deepseq` continuation `deepseq` ()

-- Every fragment gets a unique index
type FragmentDB = Map Int FragmentMeta

mkFragmentMeta :: Fragment -> Continuation -> FragmentMeta
mkFragmentMeta f c = FragmentMeta f c Nothing 0

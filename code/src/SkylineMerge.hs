{-# LANGUAGE NamedFieldPuns #-}
module SkylineMerge where

import Data.Map (Map,(!))
import qualified Data.Map as Map
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.List ((\\))
import Control.Monad.State
import Numeric.Interval
import Control.DeepSeq
import Data.List

import Skyline
import FragmentMeta
import ChartUtil
import Types

-- A Run is a list of fragments, all of which must be directly connected.
-- The interval is kept for convenience.
type Run = ([Int], Interval ProgramPoint, Interval PowerDraw)

prepare :: [Skyline] -> FragmentDB
prepare skylines = snd $ execState (mapM prepareSkyline skylines) (0,mempty)


-- generate a fresh index
freshIndex :: State (Int,FragmentDB) Int
freshIndex = do
  (idx,db) <- get
  put (idx+1,db)
  return idx


-- insert the given fragment into the database
insertFragment :: Int -> FragmentMeta -> State (Int,FragmentDB) ()
insertFragment k meta = do
  (idx,db) <- get
  put (idx, Map.insert k meta db)

isJump :: Fragment -> Bool
isJump (Jump _ _) = True
isJump _ = False

prepareSkyline :: [Fragment] -> State (Int, FragmentDB) ()
prepareSkyline skyline = prepSkyline (filter (not . isJump) skyline)

prepSkyline :: Skyline -> State (Int,FragmentDB) ()
prepSkyline [] = pure ()
prepSkyline [fj] = do
  k <- freshIndex
  let meta = mkFragmentMeta fj (-1, [])
  insertFragment k meta

prepSkyline (fj:rest@(fj1:_)) = do
  k <- freshIndex
  let
    jumps = (fst $ fragmentStart fj1) < (fst $ fragmentEnd fj)
    meta = if jumps
      then mkFragmentMeta fj (-1 , [(k+1, fragmentStart fj1)])
      else mkFragmentMeta fj (k+1, [(k+1, fragmentStart fj1)])
  insertFragment k meta
  prepSkyline rest


type FragmentGroups = Map Fragment [Int]

-- Put all fragments into bins, grouped by fragment equality
-- Returns indexes of equal fragments
groupFragments :: FragmentDB -> FragmentGroups
groupFragments fdb =
  let
    insertOps =
      [ \groups -> Map.insertWith (++) fragment [idx] groups
      | (idx,FragmentMeta{fragment}) <- Map.assocs fdb
      ]
  in
    foldl (flip id) mempty insertOps


-- Merge fragment i into fragment j.
-- Assumes that i and j exist in the database.
mergeFragment :: Int -> Int -> State FragmentDB ()
mergeFragment i j = do
  (FragmentMeta {             continuation=(_ , conti)}) <- gets (!i)
  (FragmentMeta {fragment=fj, continuation=(nj, contj)}) <- gets (!j)
  modify (Map.delete i)
  modify (Map.insert j (mkFragmentMeta fj (nj,contj <> conti)))
  modify (fmap deleteDirectContinuation)
  modify (fmap deleteIndirectContinuations)

  where
    deleteDirectContinuation f@(FragmentMeta {fragment=fk, continuation=(k,contk)})
      | k == i = mkFragmentMeta fk (-1, contk)
      | otherwise = f
    deleteIndirectContinuations (FragmentMeta {fragment=fk, continuation=(n, contk)}) =
      mkFragmentMeta fk (n, map deleteIndirect contk)
    deleteIndirect c@(k,pt)
      | k == i = (j,pt)
      | otherwise = c


-- For all fragment groups with two or more elements: merge all later elements
-- into the first one.
mergeFragments :: [Int] -> State FragmentDB ()
mergeFragments (i:j:rest) = do
  mergeFragment j i
  -- Iterating over FragmentDB multiple times creates a huge space leak.
  -- Forcing the map after each iteration makes the merge algorithm run in constant space.
  modify force
  mergeFragments (i:rest)
mergeFragments _ = return ()


mergeAllGroups :: FragmentGroups -> State FragmentDB ()
mergeAllGroups groups = mapM_ mergeFragments $ Map.elems groups


mergeSkylines :: [Skyline] -> FragmentDB
mergeSkylines skylines = fragDBOffsetted
  where
  fragDB = prepare skylines
  fragGroups = groupFragments fragDB
  -- sorting the fragment groups seems to give nicer results: longer runs with less merge points
  fragGroupsSorted = fmap sort fragGroups
  fragDBMerged = execState (mergeAllGroups fragGroupsSorted) fragDB
  runs = collectRuns fragDBMerged
  fragDBOffsetted = osDB $ execState (offsetRuns runs) (mkOffsetState fragDBMerged)


-- Keeps track of the visited fragments
type Visited = IntSet

collectRuns :: FragmentDB -> [Run]
collectRuns db = evalState (collectRuns_ (Map.keys db)) mempty
  where

  collectRuns_ :: [Int] -> State Visited [Run]
  collectRuns_ [] = return []
  collectRuns_ (fragId:rest) = do
    run <- collectRun fragId
    visiteds <- get
    -- skip fragments that have already been visited
    otherRuns <- collectRuns_ (filter (\i -> not $ IntSet.member i visiteds) rest)
    return (run : otherRuns)

  -- invariant: only collect unvisited fragments
  collectRun :: Int -> State Visited Run
  collectRun fragId = do
    let
      FragmentMeta{continuation=(n,_),fragment=fragment} = db ! fragId
      myIntervalX = (fst $ fragmentStart fragment) ... (fst $ fragmentEnd fragment)
      myIntervalY = startY ... endY
      startY = min (snd $ fragmentStart fragment) (snd $ fragmentEnd fragment)
      endY = max (snd $ fragmentStart fragment) (snd $ fragmentEnd fragment)
    modify (IntSet.insert fragId)
    if n /= -1
      then do
        (conts, contsIntervalX, contsIntervalY) <- collectRun n
        return (fragId : conts, hull contsIntervalX myIntervalX, hull contsIntervalY myIntervalY)
      else
        return ([fragId], myIntervalX, myIntervalY)


-- Returns the smallest natural number not contained in the given list
findOffset :: [Int] -> Int
findOffset nums = head $ nats \\ nums


data OffsetState
  = OffsetState
  -- The fragments to compute offsets for
  { osDB     :: FragmentDB
  -- The runs that have already been processed. Line data is cached for convenience
  , osRuns   :: [(Run, ([Line],[Line]))]
  -- cyclic list of colors that is advanced by one when a new color is needed
  , osColors :: [SkylineColor]
  }

mkOffsetState :: FragmentDB -> OffsetState
mkOffsetState db = OffsetState db [] colorCycle

nextColor :: State OffsetState SkylineColor
nextColor = do
  cs <- gets osColors
  modify (\s -> s { osColors=tail cs })
  return $ head cs

offsetRuns :: [Run] -> State OffsetState ()
offsetRuns rs = mapM_ offsetRun rs
  where
  offsetRun :: Run -> State OffsetState ()
  offsetRun thisRun@(fragments, thisIntervalX, thisIntervalY) = do
    doneRuns <- gets osRuns
    db <- gets osDB
    let
      thisLines = runToLines thisRun db
      collidingRuns =
        [ fragId
        | ((fragId:_, fragIntervalX, fragIntervalY),fragLines) <- doneRuns
        , thisIntervalX ==? fragIntervalX
        , thisIntervalY ==? fragIntervalY
        , collide thisLines fragLines
        ]
      collidingOffsets = [ offset (db ! fragId) | fragId <- collidingRuns ]
      availableOffset = findOffset collidingOffsets
    -- give offsets to all fragments in the run
    newColor <- nextColor
    sequence_ [ offsetFragment f availableOffset newColor | f <- fragments ]
    -- add run to the list of processed runs
    modify (\s -> s { osRuns = ((thisRun, thisLines) : (osRuns s))} )
  offsetFragment :: Int -> Int -> SkylineColor -> State OffsetState ()
  offsetFragment fragId newOffset newColor =
    modify (\s -> s { osDB =
      Map.adjust
        (\f -> f { offset=newOffset, color=Just newColor } )
        fragId
        (osDB s)
      })

runToLines :: Run -> FragmentDB -> ([Line],[Line])
runToLines (frags, _, _) db = skylineToLines_
  [ fragment (db!i)
  | i <- frags
  ]

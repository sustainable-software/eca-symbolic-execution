module Types where

-- import Graphics.Rendering.Chart.Easy
import Data.Aeson
import Data.Colour
import Data.Text

type PowerDraw = Int
type EnergyConsumption = Int

type SkylineColor = AlphaColour Double

-- remove alpha channel and just pretty print
instance (Eq a, Fractional a, Show a) => ToJSON (AlphaColour a) where
  toEncoding c = toEncoding (show $ c `over` black)
  toJSON c = String $ pack (show $ c `over` black)

type Offset = Double

{-# LANGUAGE MultiParamTypeClasses #-}
module InterpreterUtil where

import qualified Data.Map as Map
import Data.Map (Map)
import Control.Applicative
import Data.SBV.Dynamic
import Data.SBV (MonadSymbolic)
import Data.Set (Set)
import qualified Data.Set as Set

import AST
import SymAST
import Skyline

type PState = Map String Int
type SymPState = Map String SymExpr
type PathConstraint = SymExpr
type FunctionSkylines = Map String [TempSkyline]
type ProgramCounter = [Stmt]

-- EInterpreterState and SymInterpreterState only differ in the type of values
-- in environments, otherwise they have similar roles. The type class
-- implements record subtyping of some sort, because scoping rules are the same
-- in both. Function calls are also similar.

class Environment state valType where
  getPState :: state -> Map String valType
  getGState :: state -> Map String valType
  setPState :: state -> Map String valType -> state
  setGState :: state -> Map String valType -> state


-- Assign a value to a variable. Implements the following scoping rules. First
-- matching rule is used.
-- 1. If a local variable already exists, assign to it
-- 2. If a global variable exists, assign to it
-- 3. If none of the above, create a new local variable
lookupVar :: Environment s v => String -> s -> v
lookupVar var s =
  maybe (error $ "no such variable: " <> var) id mbValue
  where
    -- Alternative Maybe takes care of leftmost wins
    mbValue = Map.lookup var pstate <|> Map.lookup var gstate
    pstate = getPState s
    gstate = getGState s


-- Assign a value to a variable. Implements the following scoping rules. First
-- matching rule is used.
-- 1. If a local variable already exists, assign to it
-- 2. If a global variable exists, assign to it
-- 3. If none of the above, create a new local variable
assignVar :: Environment s v => String -> v -> s -> s
assignVar var value st
  | localExists  = assignToLocal
  | globalExists = assignToGlobal
  | otherwise    = assignToLocal
  where
  pstate = getPState st
  gstate = getGState st
  localExists = Map.member var pstate
  globalExists = Map.member var gstate
  assignToLocal = setPState st (Map.insert var value pstate)
  assignToGlobal = setGState st (Map.insert var value gstate)



pureEval :: Expr -> PState -> Int
pureEval (BoolConst b) _ = boolToSem b
pureEval (IntConst i) _ = i
pureEval (Var v) s = case Map.lookup v s of
  Just i -> i
  Nothing -> error $ "undefined variable " <> v
pureEval (BinaryOp Add l r) s = (pureEval l s) + (pureEval r s)
pureEval (BinaryOp Subtract l r) s = (pureEval l s) - (pureEval r s)
pureEval (BinaryOp Multiply l r) s = (pureEval l s) * (pureEval r s)
pureEval (BinaryOp Modulo l r) s = (pureEval l s) `mod` (pureEval r s)
pureEval (BinaryOp LessEq l r) s = boolToSem $ (pureEval l s) <= (pureEval r s)
pureEval (BinaryOp Less l r) s = boolToSem $ (pureEval l s) < (pureEval r s)
pureEval (BinaryOp Eq l r) s = boolToSem $ (pureEval l s) == (pureEval r s)
pureEval (BinaryOp And l r) s = boolToSem $ (semToBool $ pureEval l s) && (semToBool $ pureEval r s)
pureEval (BinaryOp Or l r) s = boolToSem $ (semToBool $ pureEval l s) || (semToBool $ pureEval r s)
pureEval (UnaryOp Neg e) s = (- pureEval e s)
pureEval (UnaryOp Not e) s = boolToSem $ not $ semToBool $ pureEval e s
pureEval (FuncCall _ _ _) _ = error "pureEval: FuncCall not supported in pure expressions"
pureEval (CompCall _ _ _) _ = error "pureEval: CompCall not supported in pure expressions"

-- conversions from booleans in the semantics (0 or 1) to integers
semToBool :: Int -> Bool
semToBool n = n /= 0

boolToSem :: Bool -> Int
boolToSem True = 1
boolToSem False = 0



exprToSymExpr :: Expr -> SymExpr
exprToSymExpr (BoolConst b) = SymBoolConst b
exprToSymExpr (IntConst i) = SymIntConst i
exprToSymExpr (Var x) = SymVar x
exprToSymExpr (BinaryOp op l r) = SymBinaryOp op (exprToSymExpr l) (exprToSymExpr r)
exprToSymExpr (UnaryOp op e) = SymUnaryOp op (exprToSymExpr e)
exprToSymExpr (FuncCall funName args sourcePos) =
  SymFuncCall funName (map exprToSymExpr args) sourcePos
exprToSymExpr (CompCall sourcePos compName compFunction) =
  SymCompCall sourcePos compName compFunction


-- TODO: can this be deleted?
symEval :: SymExpr -> SymPState -> SymExpr
symEval e@(SymBoolConst _) _ = e
symEval e@(SymIntConst _) _ = e
symEval (SymVar v) s = case Map.lookup v s of
  Just i -> i
  Nothing -> error $ "undefined variable " <> v
symEval e@(SymAny _) _ = e
symEval (SymBinaryOp Add l r)      s = partialEval $ SymBinaryOp Add      (symEval l s) (symEval r s)
symEval (SymBinaryOp Subtract l r) s = partialEval $ SymBinaryOp Subtract (symEval l s) (symEval r s)
symEval (SymBinaryOp Multiply l r) s = partialEval $ SymBinaryOp Multiply (symEval l s) (symEval r s)
symEval (SymBinaryOp LessEq   l r) s = partialEval $ SymBinaryOp LessEq   (symEval l s) (symEval r s)
symEval (SymBinaryOp Less     l r) s = partialEval $ SymBinaryOp Less     (symEval l s) (symEval r s)
symEval (SymBinaryOp And l r) s = partialEval $ SymBinaryOp And (symEval l s) (symEval r s)
symEval (SymBinaryOp Or _ _) _ = error "symEval Or not implemented"
symEval (SymUnaryOp Neg e) s = (- symEval e s)
symEval (SymUnaryOp Not e) s = partialEval $ SymUnaryOp Not $ symEval e s
symEval _ _ = error "symEval case not implemented"


-- Some simple constant folding
partialEval :: SymExpr -> SymExpr
partialEval (SymBinaryOp Add      (SymIntConst n) (SymIntConst m)) = SymIntConst (n + m)
partialEval (SymBinaryOp Subtract (SymIntConst n) (SymIntConst m)) = SymIntConst (n - m)
partialEval (SymBinaryOp Multiply (SymIntConst n) (SymIntConst m)) = SymIntConst (n * m)
partialEval (SymBinaryOp LessEq   (SymIntConst n) (SymIntConst m)) = SymBoolConst (n <= m)
partialEval (SymBinaryOp Less     (SymIntConst n) (SymIntConst m)) = SymBoolConst (n < m)
partialEval (SymBinaryOp Eq       (SymIntConst n) (SymIntConst m)) = SymBoolConst (n == m)
partialEval (SymBinaryOp And l r) =
  case (partialEval l, partialEval r) of
    (SymBoolConst True, rhs) -> rhs
    (lhs, SymBoolConst True) -> lhs
    (SymBoolConst False, _) -> SymBoolConst False
    (_, SymBoolConst False) -> SymBoolConst False
    (lhs, rhs) -> SymBinaryOp And lhs rhs
partialEval (SymUnaryOp Not (SymBoolConst True)) = SymBoolConst False
partialEval (SymUnaryOp Not (SymBoolConst False)) = SymBoolConst True
partialEval (SymUnaryOp Not (SymUnaryOp Not e)) = partialEval e
partialEval e = e

prettySymPState :: SymPState -> [String]
prettySymPState pstate = [ var <> "=" <> prettyprint expr | (var, expr) <- Map.assocs pstate ]

symToSMT :: SymExpr -> Map Int SVal -> SVal
symToSMT (SymBoolConst b) _ = svBool b
symToSMT (SymIntConst i) _ = svInteger (KBounded True 32) (toInteger i)
symToSMT (SymVar v) _ = error ("symToSMT: free variable " <> v)
symToSMT (SymAny i) env = case Map.lookup i env of
  Just s -> s
  Nothing -> error ("symToSMT: free symbolic variable " <> show i)
symToSMT (SymBinaryOp Add l r) env = svPlus (symToSMT l env) (symToSMT r env)
symToSMT (SymBinaryOp Subtract l r) env = svMinus (symToSMT l env) (symToSMT r env)
symToSMT (SymBinaryOp Multiply l r) env = svTimes (symToSMT l env) (symToSMT r env)
symToSMT (SymBinaryOp Modulo l r) env = svRem (symToSMT l env) (symToSMT r env)
symToSMT (SymBinaryOp LessEq l r) env = svLessEq (symToSMT l env) (symToSMT r env)
symToSMT (SymBinaryOp Less   l r) env = svLessThan (symToSMT l env) (symToSMT r env)
symToSMT (SymBinaryOp Eq     l r) env = svEqual (symToSMT l env) (symToSMT r env)
symToSMT (SymBinaryOp And l r) env = svAnd (symToSMT l env) (symToSMT r env)
symToSMT (SymBinaryOp Or l r) env = svOr (symToSMT l env) (symToSMT r env)
symToSMT (SymUnaryOp Neg e) env = svUNeg (symToSMT e env)
symToSMT (SymUnaryOp Not e) env = svNot (symToSMT e env)
symToSMT (SymFuncCall _ _ _) _ = error "symToSMT SymFuncCall should never happen"
symToSMT (SymCompCall _ _ _) _ = error "symToSMT SymCompCall should never happen"

anys :: SymExpr -> Set Int
anys (SymBoolConst _) = mempty
anys (SymIntConst _) = mempty
anys (SymVar _) = mempty
anys (SymBinaryOp _ l r) = (anys l) <> (anys r)
anys (SymUnaryOp _ e) = (anys e)
anys (SymAny i) = Set.singleton i
anys (SymFuncCall _ _ _) = error "anys SymFuncCall should never happen"
anys (SymCompCall _ _ _) = error "anys SymCompCall should never happen"

mkPredicate :: MonadSymbolic m => SymExpr -> m SVal
mkPredicate expr = do
  env <- sequence
    [ do -- IO monad? Or Symbolic monad?
        symVar <- sIntN 32 ("?" <> show i)
        return (i, symVar)
    | i <- Set.toList (anys expr)
    ]
  return $ symToSMT expr (Map.fromList env)

mySat :: SymExpr -> IO SatResult
mySat expr =
  let predicate = mkPredicate expr in
  satWith z3 predicate

isSatisfiable :: SymExpr -> IO Bool
isSatisfiable expr = do
  (SatResult res) <- mySat expr
  case res of
    Satisfiable _ _ -> return True
    _ -> return False

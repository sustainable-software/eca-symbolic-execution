{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
module Skyline where

import Data.Aeson
import GHC.Generics (Generic)
import Control.DeepSeq

import Types
import Numeric.Interval

-- For now, program points are line numbers
type ProgramPoint = Int

data TempFragment
  = TEdge (ProgramPoint,PowerDraw)
  | TJump ProgramPoint
  | TFill ProgramPoint
  -- Used as a start point. Should occur exactly one at the beginning of every skyline
  | TStartPoint (ProgramPoint,PowerDraw)
  deriving (Show,Eq,Generic)

-- The fragments of a temporary skyline do not have the redundant helper points
-- A TempSkyline is in reverse order!
type TempSkyline = [TempFragment]

data Fragment
  -- A component call changes the power draw
  = Vert (ProgramPoint,PowerDraw) PowerDraw
  -- A jump does not change the power draw
  | Jump (ProgramPoint,PowerDraw) ProgramPoint
  -- A horizontal segment where nothing happens. To fill a skyline to a given point.
  | Fill (ProgramPoint,PowerDraw) ProgramPoint
  deriving (Show,Eq,Ord,Generic,NFData)

-- for ECA, skylines are about power draw, which is a reusable resource
type Skyline = [Fragment]

-- see https://hackage.haskell.org/package/aeson-1.5.3.0/docs/Data-Aeson.html
instance ToJSON Fragment where
  toEncoding = genericToEncoding defaultOptions


fragmentXs :: Fragment -> [ProgramPoint]
fragmentXs (Vert (x1,_) _) = [x1]
fragmentXs (Jump (x1,_) x2) = [x1, x2]
fragmentXs (Fill (x1,_) x2) = [x1, x2]

fragmentYs :: Fragment -> [PowerDraw]
fragmentYs (Vert (_,y1) y2) = [y1, y2]
fragmentYs (Jump (_,y1) _) = [y1]
fragmentYs (Fill (_,y1) _) = [y1]


tempToSkyline :: TempSkyline -> Skyline
tempToSkyline [] = []
tempToSkyline tsky = tempToSkyline_ (0,0) tsky
  where
  tempToSkyline_ _ [] = []

  tempToSkyline_ _ (TStartPoint startPoint:rest) = tempToSkyline_ startPoint rest

  tempToSkyline_ curPt@(curX,curY) (TEdge pt@(ptX,ptY) : rest)
    -- If the component call neither changes the power level, nor advances the
    -- skyline, we skip it completely
    | curPt == pt = tempToSkyline_ curPt rest
    -- If the component call only changes the power level, but not advance the
    -- skyline, we only draw a vertical part
    | curX == ptX && curY /= ptY = Vert (ptX, curY) ptY : tempToSkyline_ pt rest
    -- If the component call does not change the power level, we omit the
    -- vertial line. This causes a statement marker to be drawn in the diagram.
    | curY == ptY = Fill curPt ptX                        : tempToSkyline_ pt rest
    | otherwise   = Fill curPt ptX : Vert (ptX, curY) ptY : tempToSkyline_ pt rest

  tempToSkyline_ curPt@(_,curY) (TJump x2 : rest) =
    Jump curPt x2 : tempToSkyline_ (x2,curY) rest

  tempToSkyline_ curPt@(curX,curY) (TFill toX : rest)
    -- omit fills of zero width
    | curX == toX = tempToSkyline_ curPt rest
    | otherwise = Fill curPt toX : tempToSkyline_ (toX,curY) rest


skylineToLines :: Skyline -> [[(Double,Double)]]
skylineToLines skyline = map fragmentToLines skyline

fragmentToLines :: Fragment -> [(Double,Double)]
fragmentToLines (Vert (x1,y1) y2) =
  [ (fromIntegral x1, fromIntegral y1)
  , (fromIntegral x1, fromIntegral y2)
  ]
fragmentToLines (Jump _ _) = []
fragmentToLines (Fill (x1,y1) x2) =
  [(fromIntegral x1, fromIntegral y1), (fromIntegral x2, fromIntegral y1)]

fragmentStart :: Fragment -> (ProgramPoint,PowerDraw)
fragmentStart (Vert p _) = p
fragmentStart (Jump p _) = p
fragmentStart (Fill p _) = p

fragmentEnd :: Fragment -> (ProgramPoint,PowerDraw)
fragmentEnd (Vert (x1,_) y2) = (x1,y2)
fragmentEnd (Jump (_,y1) x2) = (x2,y1)
fragmentEnd (Fill (_,y1) x2) = (x2,y1)


-- For collision detection
-- Horizontal lines always go from left to right
-- Vertical lines can go top-to-bottom or bottom-to-top
data Line
  = Horizontal (Interval Int) Int -- x1 x2 y
  | Vertical   (Interval Int) Int -- y1 y2 x
  deriving (Show,Eq)

fragmentToLines_ :: Fragment -> ([Line],[Line])
fragmentToLines_ (Jump _ _) = ([],[])
fragmentToLines_ (Fill (x1,y1) x2) = ([Horizontal (x1 ... x2) y1],[])
fragmentToLines_ (Vert (x1,y1) y2) =
  ( []
  , [Vertical (start ... end) x1]
  )
  where
  start = min y1 y2
  end = max y1 y2

skylineToLines_ :: Skyline -> ([Line],[Line])
skylineToLines_ [] = ([],[])
skylineToLines_ (frag:rest) =
  let
    (horiz,vert) = fragmentToLines_ frag
    (horizs, verts) = skylineToLines_ rest
  in
    (horiz <> horizs, vert <> verts)

-- Determine whether two runs collide.
-- The runs must be given split up in horizontal and vertical line segments.
-- If any of the horizontals or the verticals overlap, we have a collision.
-- Special case: if the begin or end point of a Horizontal lies on a Vertical,
-- or vice versa, we have a collision.
collide :: ([Line],[Line]) -> ([Line],[Line]) -> Bool
collide (horiz1,vert1) (horiz2,vert2) =
     horizontalCollision
  || verticalCollision
  || pointOnLine1
  || pointOnLine2
  where
  horizontalCollision = or
    [ interval1 ==? interval2
    | (Horizontal interval1 y1) <- horiz1
    , (Horizontal interval2 y2) <- horiz2
    , y1 == y2
    ]
  verticalCollision = or
    [ interval1 ==? interval2
    | (Vertical interval1 x1) <- vert1
    , (Vertical interval2 x2) <- vert2
    , x1 == x2
    ]

  -- A horizontal and a vertical line such that one of the end points of one
  -- line lies on the other line.
  pointOnLine1 = or
    [    sup intervalY == y2
      || inf intervalY == y2
      || sup intervalX == x1
      || inf intervalX == x1
    | (Vertical   intervalY x1) <- vert1
    , (Horizontal intervalX y2) <- horiz2

    -- Necessary but not sufficient: the two lines cross
    , singleton y2 ==? intervalY
    , singleton x1 ==? intervalX
    ]

  -- Vice versa
  pointOnLine2 = or
    [    sup intervalY == y2
      || inf intervalY == y2
      || sup intervalX == x1
      || inf intervalX == x1
    | (Vertical   intervalY x1) <- vert2
    , (Horizontal intervalX y2) <- horiz1

    -- Necessary but not sufficient: the two lines cross
    , singleton y2 ==? intervalY
    , singleton x1 ==? intervalX
    ]

module SymAST where

import AST
import Text.Megaparsec

data SymExpr
  = SymBoolConst Bool
  | SymIntConst Int
  | SymVar String
  | SymBinaryOp BinOp SymExpr SymExpr
  | SymUnaryOp UnOp SymExpr
  | SymAny Int
  | SymFuncCall String [SymExpr] SourcePos
  | SymCompCall SourcePos String String
  deriving (Show, Eq)

data PrettyPrintOption = Latex | Normal

prettyprintLatex :: SymExpr -> String
prettyprintLatex e = "$" <> prettyprint_ Latex e <> "$"

prettyprint :: SymExpr -> String
prettyprint e = prettyprint_ Normal e

prettyprint_ :: PrettyPrintOption -> SymExpr -> String
prettyprint_ _ (SymBoolConst b) = if b then "true" else "false"
prettyprint_ _ (SymIntConst i) = show i
prettyprint_ _ (SymVar v) = v
prettyprint_ _ (SymAny i) = "?" <> show i
prettyprint_ latex (SymBinaryOp op l r) = "(" <> leftS <> opS latex <> rightS <> ")"
  where
  leftS = prettyprint_ latex l
  opS Latex = ppOpLatex op
  opS _ = ppOp op
  rightS = prettyprint_ latex r
prettyprint_ latex (SymUnaryOp op e) = "(" <> opS latex <> prettyprint_ latex e <> ")"
  where
  opS Latex = ppUnLatex op
  opS _ = ppUn op
-- function calls should always be interpreted away before expressions end up being pretty printed
prettyprint_ _ (SymFuncCall _ _ _) = error "prettyprint_ SymFuncCall should never happen"
prettyprint_ _ (SymCompCall _ _ _) = error "prettyprint_ SymCompCall should never happen"

ppOp :: BinOp -> String
ppOp Add      = "+"
ppOp Multiply = "*"
ppOp Modulo   = "%"
ppOp Subtract = "-"
ppOp And      = " && "
ppOp Or       = " || "
ppOp LessEq   = "<="
ppOp Less     = "<"
ppOp Eq       = "=="

ppOpLatex :: BinOp -> String
ppOpLatex Add      = " + "
ppOpLatex Multiply = " * "
ppOpLatex Modulo   = " % "
ppOpLatex Subtract = " - "
ppOpLatex And      = " \\wedge "
ppOpLatex Or       = " \\vee "
ppOpLatex LessEq   = " \\leq "
ppOpLatex Less     = "<"
ppOpLatex Eq       = "="

ppUn :: UnOp -> String
ppUn Neg = "-"
ppUn Not = "!"

ppUnLatex :: UnOp -> String
ppUnLatex Neg = " - "
ppUnLatex Not = "\\neg "

instance Num SymExpr where
  (+) l r = SymBinaryOp Add l r
  (*) l r = SymBinaryOp Multiply l r
  (-) l r = SymBinaryOp Subtract l r
  negate e = SymUnaryOp Neg e
  fromInteger i = SymIntConst $ fromInteger i
  abs _ = error "instance Num SymExpr: abs not implemented"
  signum _ = error "instance Num SymExpr: signum not implemented"

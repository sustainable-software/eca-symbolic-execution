module Charts where

import Graphics.Rendering.Chart.Easy
import Graphics.Rendering.Chart.Backend.Cairo
import Text.Megaparsec.Pos

import Data.Functor
import System.FilePath
import Data.Tree
import qualified Data.Map as Map
import Data.Map ((!))
import Data.Maybe
import Data.List
import Text.Printf

import Types
import Skyline
import ChartUtil
import AST
import Options
import FragmentMeta


mergePointRadius :: Double
mergePointRadius = 3.0

mergePointShift :: Double
mergePointShift = 6.0

jumpPointRadius :: Double
jumpPointRadius = 4.0

jumpPointBorderWidth :: Double
jumpPointBorderWidth = 1.5

defaultOffsetFactor :: Double
defaultOffsetFactor = 3.0

fileOptions :: Options -> FileOptions
fileOptions opts
  = fo_size .~
      ( fromIntegral $ outputSizeX opts
      , fromIntegral $ outputSizeY opts
      )
  $ fo_format .~ fst (fileFormat opts)
  $ def



whileMarkers :: FunctionDefinition -> Double -> Plot Double Double
whileMarkers funDef whileMarkerHeight =
  let
  (whiles,maxDepth) = findWhileLoops funDef
  markerTree = fmap (fmap $ whileToMarker maxDepth whileMarkerHeight) whiles
  markers = concatMap flatten markerTree
  in
  toPlot
  $ plot_lines_values .~ fmap (fmap swap) markers
  $ plot_lines_style . line_color .~ opaque black
  $ plot_lines_style . line_dashes .~ [5, 5]
  $ def


whileMarkerVerticalSpacing :: Double
whileMarkerVerticalSpacing = 0.3

whileToMarker :: Int -> Double -> (SourcePos,SourcePos,Int) -> [(Double,Double)]
whileToMarker maxDepth whileMarkerHeight (startPos,endPos,curDepth) =
  [ (fromIntegral $ unPos $ sourceLine startPos, whileMarkerHeight)
  , (fromIntegral $ unPos $ sourceLine startPos, whileMarkerVerticalSpacing * (fromIntegral $ curDepth - maxDepth))
  , (fromIntegral $ unPos $ sourceLine endPos, whileMarkerVerticalSpacing * (fromIntegral $ curDepth - maxDepth))
  , (fromIntegral $ unPos $ sourceLine endPos, whileMarkerHeight)
  ]




-- Given a skyline, calculate spacing so that the lines don't fall on axis and
-- borders.
-- While markers and strut both use the maximum skyline height. It makes sense
-- to compute them in the same function.
strutAndWhileMarkers :: FunctionDefinition -> [Fragment] -> [Plot Double Double]
strutAndWhileMarkers ast skyline = [strutPlot,whileMarkerPlot]
  where
  strutPlot = toPlot $ PlotHidden
    { _plot_hidden_x_values = [minY, maxY]
    , _plot_hidden_y_values = [minX, maxX]
    }
  whileMarkerPlot = whileMarkers ast maxY
  xs = concatMap fragmentXs skyline
  ys = concatMap fragmentYs skyline
  minX = fromIntegral $ minimum xs - 1
  maxX = fromIntegral $ maximum xs + 1
  minY = fromIntegral $ minimum ys - 1
  maxY = fromIntegral $ maximum ys + 1




plotSkylineSymbolic :: Options -> FunctionDefinition -> [Skyline] -> FilePath -> IO ()
plotSkylineSymbolic opts ast skylines outputFileName =
  void $ renderableToFile (fileOptions opts) outputFileName skylineRenderable
  where

  skylineRenderable = toRenderable skylineLayout

  skylineLayout
    = (if showTitle opts
        then layout_title .~ dropExtension (takeFileName outputFileName)
        else id
      )
    $ layout_plots .~
        (strutAndWhileMarkers ast $ concat skylines)
        <>
        [ translatePlot thisOffset thisOffset $
            toPlot (skylineData skyline thisColor)
        | (skyline,thisColor,num) <- zip3 skylines colorCycle nats
        , let thisOffset = fromIntegral num * defaultOffsetFactor
        ]
    -- one legend entry per row
    $ layout_legend . _Just . legend_orientation .~ LORows 1

    $ layout_margin .~ margin opts

    -- override axis
    $ axisStyles opts
    $ def

  skylineData :: Skyline -> AlphaColour Double -> PlotLines Double Double
  skylineData skyline thisColor
    = plot_lines_values .~ skylineValues
    -- $ plot_lines_title .~ (if (showLegend opts) then pathCondition else "")
    $ plot_lines_style . line_color .~ thisColor
    $ def
    where
    skylineValues = fmap (fmap swap) skylineLines
    skylineLines = skylineToLines truncatedSkyline
    truncatedSkyline = case (skylineFragmentLimit opts) of
      Nothing -> skyline
      (Just n) -> take n skyline



swap :: (a,b) -> (b,a)
swap (x,y) = (y,x)


-- Use this to shift a plot in device coordinates
translatePlot :: Double -> Double -> Plot Double Double -> Plot Double Double
translatePlot offsetX offsetY p
  = plot_render .~ (withTranslation $ Point offsetX offsetY) . (p^.plot_render)
  $ p

plotFragments :: Options -> FunctionDefinition -> FragmentDB -> FilePath -> IO ()
plotFragments opts funDef fragDB outputFileName = do
  void $ renderableToFile (fileOptions opts) outputFileName fragmentsRenderable
  where

  fragmentsRenderable = toRenderable fragmentsLayout

  fragmentsLayout
    = (if showTitle opts
        then layout_title .~ dropExtension (takeFileName outputFileName)
        else id
      )
    $ layout_plots .~
        (strutAndWhileMarkers funDef $ map fragment $ Map.elems fragDB)
        <>
        -- Plot skylines
        -- negative y-offset because y-coordinates start at the top
        [ translatePlot thisOffset thisOffset $
            toPlot (skylineFragment frag)
        | frag <- Map.elems fragDB
        , let thisOffset = fromIntegral (offset frag) * defaultOffsetFactor
        ]
        <>
        -- Plot jump and merge points
        [ translatePlot (fromIntegral num * mergePointShift + thisOffset) thisOffset $
            toPlot (fragmentMergePoint pointToDraw_ isMergePoint paintColor)
        | frag <- Map.elems fragDB
        , let thisOffset = fromIntegral (offset frag) * defaultOffsetFactor
        , let (n,conts) = continuation frag
        -- Only indirect continuations get merge points
        , let indirectConts = filter (\(i,_) -> i /= n) (nub conts)
        , ((i,pt),num) <- zip indirectConts nats
        , let isMergePoint = fragmentEnd (fragment frag) == pt
        , let paintColor = color (fragDB!i)
        , let pointToDraw = if isMergePoint then pt else fragmentEnd (fragment frag)
        , let pointToDraw_ = bimap fromIntegral fromIntegral pointToDraw
        ]
        <>
        -- Statement markers, only drawn when a Fill is directly followed by another Fill
        [ translatePlot thisOffset thisOffset $
            toPlot (fragmentStatementMarker pointToDraw_ paintColor)
        -- The pattern match ensures that we only handle Fills
        | frag@FragmentMeta{fragment=Fill _ _} <- Map.elems fragDB
        , let thisOffset = fromIntegral (offset frag) * defaultOffsetFactor
        , let (n,allConts) = continuation frag
        -- We're only interested in direct continuations, and only if there are no other indirect continuations.
        -- Because if there are indirect continuations, there is already a merge point here
        , let conts = nub allConts
        , length conts == 1
        , let directConts = filter (\(i,_) -> i == n) conts
        , (i,_) <- directConts
        -- Make sure that the continuation is also a fill
        , FragmentMeta{fragment=Fill _ _} <- [fragDB!i]
        , let paintColor = color (fragDB!i)
        , let pointToDraw = fragmentEnd (fragment frag)
        , let pointToDraw_ = bimap fromIntegral fromIntegral pointToDraw
        ]
    -- one legend entry per row
    $ layout_legend . _Just . legend_orientation .~ LORows 1
    $ layout_margin .~ margin opts
    $ axisStyles opts
    $ def

  skylineFragment frag
    = plot_lines_values .~ [fmap swap $ fragmentToLines $ fragment frag]
    $ plot_lines_style . line_color .~ fromMaybe (opaque black) (color frag)
    $ def

  fragmentMergePoint pt isMergePoint paintColor
    = plot_points_values .~ [swap pt]
    $ plot_points_style .~ (if isMergePoint
        then mergePointStyle paintColor
        else jumpPointStyle paintColor)
    $ def

  fragmentStatementMarker pt paintColor
    = plot_points_values .~ [swap pt]
    $ plot_points_style .~ markerPointStyle paintColor
    $ def

axisStyles :: Options -> Layout Double Double -> Layout Double Double
axisStyles opts l =
    mkAxis opts layout_y_axis yAxisBounds
  $ layout_y_axis . laxis_override .~ axisGridAtLabels . predefinedLabels (yAxisLabels opts) (yAxisLabelPrefix opts)
  $ layout_y_axis . laxis_style . axis_label_gap .~ (fromIntegral $ yAxisLabelGap opts)
  $ layout_y_axis . laxis_reverse .~ True
  $ layout_y_axis . laxis_style . axis_label_style . font_name .~ axisFontName opts
  $ layout_y_axis . laxis_style . axis_label_style . font_size .~ axisFontSize opts
  $ layout_y_axis . laxis_title_style . font_name .~ axisFontName opts
  $ layout_y_axis . laxis_title_style . font_size .~ axisFontSize opts
  $ layout_y_axis . laxis_title .~ yAxisTitle opts
  $ mkAxis opts layout_x_axis xAxisBounds
  $ layout_x_axis . laxis_override .~ axisGridAtLabels . predefinedLabels (xAxisLabels opts) ""
  $ layout_x_axis . laxis_style . axis_label_gap .~ (fromIntegral $ xAxisLabelGap opts)
  $ layout_x_axis . laxis_style . axis_label_style . font_name .~ axisFontName opts
  $ layout_x_axis . laxis_style . axis_label_style . font_size .~ axisFontSize opts
  $ layout_x_axis . laxis_title_style . font_name .~ axisFontName opts
  $ layout_x_axis . laxis_title_style . font_size .~ axisFontSize opts
  $ layout_x_axis . laxis_title .~ xAxisTitle opts
  $ l

predefinedLabels :: Maybe [Double] -> String -> AxisData Double -> AxisData Double
predefinedLabels mbLabels padding ax =
  case mbLabels of
    Nothing -> id
    Just labels -> axis_labels .~ [map (\x -> (x, printf (padding <> "%.0f") x)) labels]
  $ axis_ticks .~ []
  $ ax

-- axis scaling is a bit too much magic, which is probably what you want in most cases.
-- Setting la_nLabels and la_nTicks to axisMax seems to properly restrict the
-- axis to that length, at least in the cases I'm interested in.
-- This type signature has been copied from the compile warning message.
mkAxis ::
  Options
  -> ((LayoutAxis Double -> Identity (LayoutAxis Double)) -> a2 -> Identity a2)
  -> (Options -> Maybe (Double, Double))
  -> a2
  -> a2
mkAxis opts selector bounds =
  case bounds opts of
      Nothing -> id
      (Just (axisMin,axisMax)) -> selector . laxis_generate .~
        scaledAxis axisParams (axisMin,axisMax)
          where
            axisParams =
                la_nLabels .~ floor axisMax
              $ la_nTicks .~ floor axisMax
              $ def

mergePointStyle :: Maybe SkylineColor -> PointStyle
mergePointStyle paintColor
  = point_radius .~ mergePointRadius
  $ point_color .~ fromMaybe (opaque red) paintColor
  $ point_shape .~ PointShapeCircle
  $ def

jumpPointStyle :: Maybe SkylineColor -> PointStyle
jumpPointStyle paintColor
  = point_radius .~ jumpPointRadius
  $ point_color .~ transparent
  $ point_border_width .~ jumpPointBorderWidth
  $ point_border_color .~ fromMaybe (opaque red) paintColor
  $ def

markerPointStyle :: Maybe SkylineColor -> PointStyle
markerPointStyle paintColor
  = point_radius .~ mergePointRadius
  $ point_border_color .~ fromMaybe (opaque red) paintColor
  $ point_border_width .~ jumpPointBorderWidth
  $ point_color .~ fromMaybe (opaque red) paintColor
  $ point_shape .~ PointShapePlus
  $ def

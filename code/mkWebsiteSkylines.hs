-- run this script like this
--     $ stack runghc mkWebsiteSkylines.hs
import System.Process
import System.Environment
import Data.List

setZ3Path = do
  path <- getEnv "PATH"
  setEnv "PATH" $ path <> ":/home/mkl/radboud/src/symbolic-execution/z3/build/"

noMerge = ["--no-merge", "--output-suffix", "-no-merge"]
yesFilter = ["--filter-components", "--output-suffix", "-filtered"]
highRes = ["--width", "900", "--height", "1200", "--output-suffix", "-highres"]
xLabels lastLineNr = ["--x-labels", show [1,2..lastLineNr]]

-- filename of input program and extra command line arguments
programList =
  [ ("programs/website/common-initial-segment.eca", [])
  , ("programs/website/common-initial-segment.eca", highRes <> xLabels 12)
  , ("programs/website/common-initial-segment.eca", noMerge)
  , ("programs/website/common-initial-segment.eca", noMerge <> highRes <> xLabels 12)

  , ("programs/website/common-final-segment.eca", [])
  , ("programs/website/common-final-segment.eca", highRes <> xLabels 25)
  , ("programs/website/common-final-segment.eca", noMerge)
  , ("programs/website/common-final-segment.eca", noMerge <> highRes <> xLabels 25)

  , ("programs/paper/bug.eca", [])
  , ("programs/paper/bug.eca", highRes <> xLabels 15)
  , ("programs/paper/bug.eca", noMerge)
  , ("programs/paper/bug.eca", noMerge <> highRes <> xLabels 15)

  , ("programs/paper/loop-alternating.eca", [])
  , ("programs/paper/loop-alternating.eca", highRes <> xLabels 20)
  , ("programs/paper/loop-alternating.eca", noMerge)
  , ("programs/paper/loop-alternating.eca", noMerge <> highRes <> xLabels 20)

  , ("programs/paper/split-and-merge.eca", [])
  , ("programs/paper/split-and-merge.eca", highRes <> xLabels 15)
  , ("programs/paper/split-and-merge.eca", noMerge)
  , ("programs/paper/split-and-merge.eca", noMerge <> highRes <> xLabels 15)

  , ("programs/website/loop-changes.eca", [])
  , ("programs/website/loop-changes.eca", highRes <> xLabels 25)
  , ("programs/website/loop-changes.eca", noMerge)
  , ("programs/website/loop-changes.eca", noMerge <> highRes <> xLabels 25)

  , ("programs/functions/filter-example.eca", [])
  , ("programs/functions/filter-example.eca", highRes <> xLabels 20)
  , ("programs/functions/filter-example.eca", noMerge)
  , ("programs/functions/filter-example.eca", noMerge <> highRes <> xLabels 20)
  , ("programs/functions/filter-example.eca", yesFilter)
  , ("programs/functions/filter-example.eca", yesFilter <> highRes <> xLabels 20)

  , ("programs/functions/filter-example2.eca", [])
  , ("programs/functions/filter-example2.eca", highRes <> xLabels 35)
  , ("programs/functions/filter-example2.eca", noMerge)
  , ("programs/functions/filter-example2.eca", noMerge <> highRes <> xLabels 35)
  , ("programs/functions/filter-example2.eca", yesFilter)
  , ("programs/functions/filter-example2.eca", yesFilter <> highRes <> xLabels 35)

  , ("programs/functions/recursive.eca", [])
  , ("programs/functions/recursive.eca", highRes <> xLabels 70)
  , ("programs/functions/recursive.eca", noMerge)
  , ("programs/functions/recursive.eca", noMerge <> highRes <> xLabels 70)

  , ("programs/infeasible-omitted.eca", [])
  , ("programs/infeasible-omitted.eca", highRes <> xLabels 35)
  , ("programs/infeasible-omitted.eca", noMerge)
  , ("programs/infeasible-omitted.eca", noMerge <> highRes <> xLabels 35)

  , ("programs/loop-condition-with-side-effect.eca", [])
  , ("programs/loop-condition-with-side-effect.eca", highRes <> xLabels 27)
  , ("programs/loop-condition-with-side-effect.eca", noMerge)
  , ("programs/loop-condition-with-side-effect.eca", noMerge <> highRes <> xLabels 27)

  , ("programs/many-fragments.eca", [])
  , ("programs/many-fragments.eca", highRes <> ["--x-labels", show [1,3..80]])
  , ("programs/many-fragments.eca", noMerge)
  , ("programs/many-fragments.eca", noMerge <> highRes <> ["--x-labels", show [1,3..80]])

  , ("programs/nested-loops.eca", [])
  , ("programs/nested-loops.eca", highRes <> xLabels 25)
  , ("programs/nested-loops.eca", noMerge)
  , ("programs/nested-loops.eca", noMerge <> highRes <> xLabels 25)

  , ("programs/two-loops.eca", [])
  , ("programs/two-loops.eca", highRes <> xLabels 25)
  , ("programs/two-loops.eca", noMerge)
  , ("programs/two-loops.eca", noMerge <> highRes <> xLabels 25)

  , ("programs/website/robot-optimized.eca", [])
  , ("programs/website/robot-optimized.eca", highRes)
  ]

programs :: [(String, [String])]
programs =
  filter
    -- (const True)
    (\(prog,_) -> "robot-optimized" `isInfixOf` prog)
    programList

mkArgs (prog, extraArgs) =
  [ "exec", "eca", "--", "-s", prog
  , "--width", "300"
  , "--height", "400"
  , "--output-dir", "website"
  ]
  <> extraArgs

runEca :: (String, [String]) -> IO ()
runEca prog@(fileName,_) = do
  putStrLn fileName
  callProcess "stack" $ mkArgs prog

main = do
  setZ3Path
  mapM_ runEca programs

{-# LANGUAGE QuasiQuotes #-}
module InterpreterSpec where

import Test.Hspec

import qualified Data.Map as Map
import Text.Heredoc
import Text.Megaparsec

import Parser
import Interpreter
import TestComponents
import InterpreterUtil

import SpecUtil

testInterpret :: String -> PState -> IO PState
testInterpret stmt pstate =
  _pstate <$> execute (mkST (testParse (between sc eof pStmt) stmt) pstate defaultComponents mempty)

testInterpretWithResult :: String -> [(String, Int)] -> Expectation
testInterpretWithResult prog expectedResultState =
  testInterpret prog mempty `shouldReturn` Map.fromList expectedResultState

testInterpretMain :: String -> IO Int
testInterpretMain program = do
  let prog = (testParse (between sc eof pProgram) program)
  fst <$> executeMain prog defaultComponents

testInterpretMainWithGlobal :: String -> Int -> [(String,Int)] -> Expectation
testInterpretMainWithGlobal program expectedResult expectedResultState = do
  let prog = (testParse (between sc eof pProgram) program)
  (result, ST{_gstate=gstate,_stack=stack}) <- executeMain prog defaultComponents
  (result, gstate, stack) `shouldBe` (expectedResult, Map.fromList expectedResultState, [])

spec :: Spec
spec = do
  describe "execute" $ do
    it "skips a skip" $ do
      testInterpret "{}" mempty `shouldReturn` mempty
      testInterpret "{}" (Map.fromList [("x",42)]) `shouldReturn` (Map.fromList [("x",42)])
    it "interprets an assignment in the empty environment" $ do
      testInterpret "y = 100;" mempty `shouldReturn` (Map.fromList [("y", 100)])
    it "interprets an assignment in a non-empty environment" $ do
      testInterpret "y = 13;" (Map.fromList [("y", 0)]) `shouldReturn` (Map.fromList [("y", 13)])
    it "interprets a program with some assignments" $ do
      let prog = [here|
        {
          x = 5;
          y = x + 1;
          x = y + x;
          a = 0 % 2;
          b = 1 % 2;
          c = 500 % 1000;
          d = 1500 % 1000;
        }
        |]
      testInterpret prog mempty `shouldReturn` (Map.fromList
        [("x",11)
        ,("y",6)
        ,("a",0)
        ,("b",1)
        ,("c",500)
        ,("d",500)
        ])
    it "interprets a conditional with True condition" $ do
      let prog = [here|
        {
          foo = true;
          if( foo ) bar = 42; else bar = -100;
        }
        |]
      testInterpretWithResult prog [("foo",boolToSem True),("bar",42)]
    it "interprets a conditional with False condition" $ do
      let prog = [here|{
          foo = false;
          if( foo ) bar = 42; else bar = -100;
        }|]
      testInterpretWithResult prog [("foo",boolToSem False),("bar",-100)]
    it "interprets a conditional with compound statements in the then branch" $ do
      let prog = [here|{
            x = 5;
            if( x <= 10 )
            {
              x = x * 2;
              y = x + 1;
            }
            else y = -100;
            }
          |]
      testInterpretWithResult prog [("x",10),("y",11)]

    it "interprets a while loop" $ do
      let
        prog = [here|{
          x = 0;
          while( x <= 10 )
          {
            x = x + 2;
          }
        }|]
      testInterpretWithResult prog [("x",12)]

    it "counts loop iterations" $ do
      let
        prog = [here|{
          x = 0;
          iterations = 0;
          while( x <= 10 )
          {
            x = x + 2;
            iterations = iterations + 1;
          }
        }|]
      testInterpretWithResult prog [("x",12), ("iterations",6)]

    it "skips while when condition is false" $ do
      let
        prog = [here|{
          x = 0;
          iterations = 0;
          while( ! (x <= 9) )
          {
            x = x + 2;
            iterations = iterations + 1;
          }
        }|]
      testInterpretWithResult prog [("x",0), ("iterations",0)]

    it "interprets a whole program with just the main function" $ do
      let
        prog = [here|
          int main()
          {
            return 42;
          }
        |]
      testInterpretMain prog `shouldReturn` 42

    it "assigns and reads a global variable" $ do
      let
        prog = [here|
          int main()
          {
            globalX = 100;
            return globalX;
          }
          int globalX = 0;
        |]
      testInterpretMain prog `shouldReturn` 100

    it "assigns and reads a global variable" $ do
      let
        prog = [here|
          int main()
          {
            globalX = 100;
            return globalX;
          }
          int globalX = 0;
        |]
      testInterpretMain prog `shouldReturn` 100

    it "interprets a whole program where main calls another function" $ do
      let
        prog = [here|
          int gX = 0;
          int main()
          {
            foobar();
            return gX;
          }

          int foobar()
          {
            gX = 42;
            return 0;
          }
        |]
      testInterpretMain prog `shouldReturn` 42

    it "local variables shadow global variables" $ do
      let
        prog = [here|
          int shadow = 42;
          int main()
          {
            foobar(100);
            // shadow should not be changed
            return shadow;
          }

          int foobar(int shadow)
          {
            shadow = 100;
            return 0;
          }
        |]
      testInterpretMain prog `shouldReturn` 42

    it "global variable is initialized with value of another global variable" $ do
      let
        prog = [here|
          int x = 42;
          int y = x + 1;
          int main()
          {
            return y;
          }
        |]
      testInterpretMain prog `shouldReturn` 43

    it "recursive function" $ do
      let
        prog = [here|
          int result = 0;

          int gauss(int n)
          {
            if( n <= 0 )
            {
            }
            else
            {
              result = result + n;
              gauss(n-1);
            }
            return 0;
          }
          int main()
          {
            result = 0;
            gauss(10);
            return result;
          }
        |]
      testInterpretMain prog `shouldReturn` 55

    it "function call in expression" $ do
      let
        prog = [here|
          int gauss(int n)
          {
            if( n <= 0 )
            {
              return 0;
            }
            else
            {
              return n + gauss(n-1);
            }
          }
          int main()
          {
            return gauss(10);
          }
        |]
      testInterpretMain prog `shouldReturn` 55

    it "function call in expression changes global variable" $ do
      let
        prog = [here|
          int global = 0;
          int foo(int n)
          {
            global = global + n;
            return n;
          }
          int main()
          {
            return (foo(10) * foo(3)) + global;
          }
        |]
      testInterpretMain prog `shouldReturn` (10 * 3 + 13)

    it "mutual recursion" $ do
      let
        prog = [here|
          int even(int n)
          {
            if( n == 0 )
            {
              return true;
            }
            else
            {
              return odd(n-1);
            }
          }
          int odd(int n)
          {
            if( n == 0 )
            {
              return false;
            }
            else
            {
              return even(n-1);
            }
          }
          int main()
          {
            return even(211);
          }
        |]
      testInterpretMain prog `shouldReturn` (boolToSem False)

    it "component calls with return values" $ do
      let
        prog = [here|
          int main()
          {
            x = SENS1.sense();
            y = SENS1.sense();
            return x + y;
          }
        |]
      testInterpretMain prog `shouldReturn` (101 + 42)

    -- this tests a bug in evaluate, where execution of the function call
    -- foo() would continue until the end of main.
    it "function call expression" $ do
      testInterpretMainWithGlobal [here|
        int g = 0;
        int foo()
        {
          return 100;
        }
        int main ()
        {
          if( foo() == 100 )
          {
            g = 1;
          }
          else
          {
            g = 2;
          }

          return 42;
        }|]
        42 [("g", 1)]

    it "void function" $ do
      testInterpretMainWithGlobal [here|
        int g = 0;
        void foo()
        {
          g = 20;
        }
        int main()
        {
          foo();
          return 101;
        }|]
        101 [("g", 20)]

    it "nested void functions" $ do
      testInterpretMainWithGlobal [here|
        int g = 0;
        void foo()
        {
          g = g + 20;
          bar();
        }
        void bar()
        {
          g = g + 10;
        }
        int main()
        {
          foo();
          return 101;
        }|]
        101 [("g", 30)]

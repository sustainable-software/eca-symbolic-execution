{-# LANGUAGE QuasiQuotes #-}
module SymInterpreterSpec where

import Text.Heredoc
import Test.Hspec
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Set (Set)
import Data.Default.Class

import Parser
import SymInterpreter
import SymAST
import InterpreterUtil
import TestComponents
import SymComponent
import Options

import SpecUtil

testSymInterpret_ :: String -> [(SymExpr, SymExpr, SymPState, [SymStackFrame])]
testSymInterpret_ inputSource =
  [ (pathConstraint, lookupVar "#return" st, gstate, stack)
  | st@(SST{_pathConstraint=pathConstraint,_gstate=gstate,_stack=stack}) <- symExecuteMain
      (testParse pProgram inputSource)
      defaultSymComponents
      def{maxIterations=12}
  ]

-- Assume that no two paths can have the same path constraint. Then we can use
-- path constraints as keys for a map of constraint -> program state
mkResultTable :: [(SymExpr, SymExpr, a, [SymStackFrame])] -> Map String String
mkResultTable paths =
  Map.fromList
    [ (prettyprint pathConstraint, prettyprint returnValue)
    | (pathConstraint, returnValue, _, _) <- paths
    ]

mkResultSet
  :: [(SymExpr, SymExpr, SymPState, [SymStackFrame])]
  -> Set (String, String, (Set (String, String)), Int)
mkResultSet paths =
  Set.fromList
    [ (prettyprint pathConstraint, prettyprint returnValue, Set.fromList globals, length stack)
    | (pathConstraint, returnValue, globalPState, stack) <- paths
    , let globals = [ (var, prettyprint expr) | (var, expr) <- Map.toList globalPState]
    ]

-- result SymPStates are pretty printed for convenience
testSymInterpret
  :: String
  -> [(String, String)] -- (path constraint, return value)
  -> Expectation
testSymInterpret program expectedResult =
  (mkResultTable $ testSymInterpret_ program)
    `shouldBe` Map.fromList expectedResult

testSymInterpretWithGlobal
  :: String
  -> [(String, String, [(String, String)])] -- (path constraint, return value, global variables)
  -> Expectation
testSymInterpretWithGlobal program expectedResult =
  (mkResultSet $ testSymInterpret_ program)
    `shouldBe`
      Set.fromList
        [ (pathConstraint, retVal, Set.fromList globals, 0)
        | (pathConstraint, retVal,              globals) <- expectedResult
        ]

spec :: Spec
spec = do
  describe "symbolicexecutionexplicit" $ do
    it "main returns a value" $ do
      testSymInterpret [here|
        int main()
        {
          return 5;
        }
        |] [("true", "5")]
    it "executes an assignment" $ do
      testSymInterpret [here|
        int main()
        {
          x = 4;
          return x;
        }
        |] [("true", "4")]

    it "executes a sequence of assignments" $ do
      testSymInterpret
        [here|
        int main()
        {
          x = 5;
          y = x + 1;
          z = y * 2;
          x = z - 1;
          return x;
        }
        |]
        [("true", "11")]
    it "simplifies LessEq" $ do
      testSymInterpret
        [here| int main() { return 5000 + 2 <= 1000; } |]
        [("true", "false")]
    it "executes a read statement" $ do
      testSymInterpret
        [here| int main() { foo = TERM.readInt(); return foo; } |]
        [("true", "?0")]
    it "executes two read statement" $ do
      testSymInterpret
        [here| int main() { foo = TERM.readInt(); bar = TERM.readInt(); return foo + bar; } |]
        [("true", "(?0+?1)")]
    it "executes read and arithmetic" $ do
      testSymInterpret
        [here| int main() {
          foo = TERM.readInt();
          x = 1 + foo * 2;
          return x;
        }
        |]
        [("true", "(1+(?0*2))")]
    it "yields two states when executing a conditional" $ do
      testSymInterpret
        [here| int main() {
          x = TERM.readInt();
          if( x <= 10 )
          { y = 5; }
          else
          { y = -100; }
          return y;
        }
        |]
        [ ("(?0<=10)",       "5")
        , ("(!(?0<=10))", "(-100)")
        ]
    it "yields four states for two consecutive conditionals" $ do
      testSymInterpret
        [here|
        int main() {
          x = TERM.readInt();
          if( x <= 10 )
            { y = 5; }
          else
            { y = -100; }

          z = TERM.readInt();

          if( z <= 9 )
            { y = y + x; }
          else
            { y = y + z; }

          return y;
        }
        |]
        [ (   "((?0<=10) && (?1<=9))" , "(5+?0)")
        , (   "((?0<=10) && (!(?1<=9)))", "(5+?1)")
        , ("((!(?0<=10)) && (?1<=9))" , "((-100)+?0)")
        , ("((!(?0<=10)) && (!(?1<=9)))", "((-100)+?1)")
        ]


    it "correctly handles a global variable" $ do
      testSymInterpretWithGlobal [here|
        int g = 0;
        int main()
        {
          g = 100;
          return g;
        }
        |]
        [ ("true", "100", [("g", "100")])
        ]

    it "handles a simple function call statement" $ do
      testSymInterpretWithGlobal [here|
        int g = 0;
        int main()
        {
          foo();
          return g;
        }
        int foo()
        {
          g = 42;
          return 0;
        }
        |]
        [ ("true", "42", [("g", "42")])
        ]

    it "handles a read in a function call statement with multiple outcomes" $ do
      testSymInterpretWithGlobal [here|
        int g = 0;
        int main()
        {
          foo();
          return g;
        }
        int foo()
        {
          x = TERM.readInt();
          if( x <= 10 )
          {
            g = 5;
          }
          else
          {
            g = 42;
          }
          return 0;
        }
        |]
        [ (  "(?0<=10)",   "5", [("g",  "5")])
        , ("(!(?0<=10))", "42", [("g", "42")])
        ]

    it "handles read to global variable" $ do
      testSymInterpretWithGlobal [here|
        int g = 0;
        int main()
        {
          foo();
          return g;
        }
        int foo()
        {
          g = TERM.readInt();
          return 0;
        }
        |]
        [ ("true", "?0", [("g", "?0")])
        ]

    it "handles recursive functions" $ do
      testSymInterpretWithGlobal [here|
        int result = 0;
        int main()
        {
          result = 0;
          gauss(10);
          return result;
        }
        int gauss(int n)
        {
          if( n == 0 )
          {
            return 0;
          }
          else
          {
            result = result + n;
            gauss(n-1);
            return 0;
          }
        }
        |]
        [ ("true", "55", [("result", "55")])
        ]

    it "handles a function call expression" $ do
      testSymInterpret [here|
        int main()
        {
          return foo(100);
        }
        int foo(int n)
        {
          return n+1;
        }
        |]
        [ ("true", "101")
        ]

    it "mutual recursion" $ do
      testSymInterpret [here|
          int even(int n)
          {
            if( n == 0 )
            {
              return true;
            }
            else
            {
              return odd(n-1);
            }
          }
          int odd(int n)
          {
            if( n == 0 )
            {
              return false;
            }
            else
            {
              return even(n-1);
            }
          }
          int main()
          {
            return even(21);
          }
        |]
        [ ("true", "false")
        ]

    it "function call in conditional" $ do
      testSymInterpret [here|
          int isZero(int n)
          {
            if( n == 0 )
            {
              return true;
            }
            else
            {
              return false;
            }
          }
          int main()
          {
            if( isZero(2) )
            {
              return 0;
            }
            else
            {
              return 1;
            }
          }
        |]
        [ ("true", "1")
        ]

    it "function call in function parameter" $ do
      testSymInterpret [here|
          int isZero(int n)
          {
            if( n == 0 )
            {
              return true;
            }
            else
            {
              return false;
            }
          }
          int myFunction(int x, int y, int z)
          {
            return x && y && z;
          }
          int main()
          {
            return myFunction(isZero(0), !isZero(1), !isZero(200));
          }
        |]
        [ ("true", "true")
        ]

    it "function call with symbolic argument" $ do
      testSymInterpret [here|
          int isZero(int n)
          {
            if( n == 0 )
            {
              return true;
            }
            else
            {
              return false;
            }
          }
          int main()
          {
            x = TERM.readInt();
            return isZero(x);
          }
        |]
        [ (   "(?0==0)",  "true")
        , ("(!(?0==0))", "false")
        ]

    it "conditional with function call with symbolic argument" $ do
      testSymInterpret [here|
          int isZero(int n)
          {
            if( n == 0 )
            {
              return true;
            }
            else
            {
              return false;
            }
          }
          int main()
          {
            x = TERM.readInt();
            if( isZero(x) )
            {
              return 100;
            }
            else
            {
              return 101;
            }
          }
        |]
        [ (   "(?0==0)", "100")
        , ("(!(?0==0))", "101")
        ]


    it "executes a while loop" $ do
      testSymInterpret [here|
        int main()
        {
          x = 0;
          while( x <= 10 )
          {
            x = x + 2;
          }
          return x;
        }|]
        [("true", "12")]

    it "counts loop iterations" $ do
      testSymInterpret [here|
        int main()
        {
          x = 0;
          iterations = 0;
          while( x <= 10 )
          {
            x = x + 2;
            iterations = iterations + 1;
          }
          return iterations;
        }|]
        [("true", "6")]

    it "skips while when condition is false" $ do
      testSymInterpret [here|
        int main ()
        {
          x = 0;
          iterations = 0;
          while( ! (x <= 9) )
          {
            x = x + 2;
            iterations = iterations + 1;
          }
          return iterations;
        }|]
        [("true", "0")]

    it "skips while when condition is false, with read" $ do
      testSymInterpret [here|
        int main ()
        {
          x = TERM.readInt();
          iterations = 0;
          while( (x <= 9) && (10 <= x) )
          {
            x = x + 2;
            iterations = iterations + 1;
          }
          return iterations;
        }|]
        [("(!((?0<=9) && (10<=?0)))", "0")]

    it "handles a component call" $ do
      testSymInterpret [here|
        int main ()
        {
          LED1.switchOn();
          return 0;
        }|]
        [("true", "0")]

    it "function call with variable" $ do
      testSymInterpret [here|
        int foo(int n)
        {
          return n + 10;
        }
        int main ()
        {
          x = 50;
          return foo(x);
        }|]
        [("true", "60")]

    -- this tests a bug in eSymEvaluate, where execution of the function call
    -- foo() would continue until the end of main.
    it "function call expression" $ do
      testSymInterpretWithGlobal [here|
        int g = 0;
        int foo()
        {
          return 100;
        }
        int main ()
        {
          if( foo() == 100 )
          {
            g = 1;
          }
          else
          {
            g = 2;
          }

          return 42;
        }|]
        [("true", "42", [("g", "1")])]

    it "component call with return value" $ do
      testSymInterpret [here|
        int main ()
        {
          return SENS2.sense();
        }|]
        [("((10<=?0) && (?0<=20))", "?0")]

    it "void function" $ do
      testSymInterpretWithGlobal [here|
        int g = 0;
        void foo()
        {
          g = 20;
        }
        int main()
        {
          foo();
          return 101;
        }|]
        [("true", "101", [("g", "20")])]

    it "nested void functions" $ do
      testSymInterpretWithGlobal [here|
        int g = 0;
        void foo()
        {
          g = g + 20;
          bar();
        }
        void bar()
        {
          g = g + 10;
        }
        int main()
        {
          foo();
          return 101;
        }|]
        [("true", "101", [("g", "30")])]

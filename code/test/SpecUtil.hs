module SpecUtil where

import Text.Megaparsec
import Data.List

import Parser
import Util

testParse :: (Parser a) -> String -> a
testParse p i = case runParser p dummyFileName i of
  Right a -> a
  Left err -> error $ errorBundlePretty err

testParse' :: (Parser a) -> String -> Either String a
testParse' p i = case runParser p dummyFileName i of
  Right a -> Right a
  Left err -> Left $ errorBundlePretty err

isError :: String -> Either String a -> Bool
isError expectedErrorPrefix (Left err) =
  expectedErrorPrefix `isPrefixOf` err
isError _ _ = False

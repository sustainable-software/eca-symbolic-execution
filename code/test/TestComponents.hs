module TestComponents where

import qualified Data.Map as Map

import Component
import SymComponent

import qualified ExampleComponents as EC

defaultComponents :: CState
defaultComponents =
  Map.fromList
    [("SENS1", EC.sensor 101 42)
    ]

defaultSymComponents :: SCState
defaultSymComponents =
  Map.fromList
    [("LED1", unsafeCompToSymComp $ EC.led 10)
    ,("SENS1", unsafeCompToSymComp $ EC.sensor 101 5)
    ,("SENS2", EC.symSensor 10 20)
    ]
  <>
  Map.fromList
    [ ("TERM", EC.symTerminal)
    ]

module SkylineMergeSpec where

import Test.Hspec

import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.List as List
import Numeric.Interval

import FragmentMeta
import SkylineMerge
import Skyline

-- 3-fragment run 0,4,6
-- 2-fragment run 1,3
-- 2-fragment run 2,5
testFragments :: [(Int,FragmentMeta)]
testFragments =
  [ (0, mkFragmentMeta (Fill (1,0) 2) ( 4,[]))
  , (1, mkFragmentMeta (Fill (3,0) 5) ( 3,[]))
  , (2, mkFragmentMeta (Fill (4,0) 6) ( 5,[]))
  , (3, mkFragmentMeta (Fill (5,0) 7) (-1,[]))
  , (4, mkFragmentMeta (Fill (2,0) 4) ( 6,[]))
  , (5, mkFragmentMeta (Fill (6,0) 7) (-1,[]))
  , (6, mkFragmentMeta (Fill (4,0) 6) (-1,[]))
  ]

testFragmentDB :: FragmentDB
testFragmentDB = Map.fromList testFragments

spec :: Spec
spec = do
  describe "collectRuns" $ do
    it "collects some simple runs" $ do
      let
        myRuns = collectRuns testFragmentDB
        isRun r = List.elem r myRuns
      ([0,4,6], 1...6, 0...0) `shouldSatisfy` isRun
      ([1,3]  , 3...7, 0...0) `shouldSatisfy` isRun
      ([2,5]  , 4...7, 0...0) `shouldSatisfy` isRun

    it "collects fragments only once" $ do
      let
        myRuns = collectRuns testFragmentDB
        isRun r = List.elem r myRuns
      ([4,6], 2...6, 0...0) `shouldSatisfy` (not . isRun)
      ([6]  , 4...6, 0...0) `shouldSatisfy` (not . isRun)

    it "collects all indexes: every index in the fragmentDB should occur in at least one run" $ do
      let
        myRuns = collectRuns testFragmentDB
        indexes = concatMap (\(a,_,_) -> a) myRuns
      Set.fromList indexes `shouldBe` Set.fromList (Map.keys testFragmentDB)

  describe "findOffset" $ do
    it "finds the smallest natural number not in a list" $ do
      findOffset [0,1,3,7] `shouldBe` 2
      findOffset [3,7,0,1] `shouldBe` 2
      findOffset [1,2,3,4,5,6,7] `shouldBe` 0
      findOffset [0,0,1,1,4,4,2,2,3,3] `shouldBe` 5
      findOffset [] `shouldBe` 0
      findOffset [-1] `shouldBe` 0

  let
    skyCollides :: (Bool -> Bool) -> Skyline -> Skyline -> Expectation
    skyCollides modifier sky1 sky2 = do
      lines1 `shouldSatisfy` modifier . collide lines2
      lines2 `shouldSatisfy` modifier . collide lines1
      where
      lines1 = skylineToLines_ sky1
      lines2 = skylineToLines_ sky2

  describe "collide" $ do
    it "figures out that identical Fills collide" $
      let
        skyline = [Fill (5,3) 10]
      in
        skyCollides id skyline skyline
    it "identical Verts collide" $
      let
        skyline = [Vert (0,0) 5]
      in
        skyCollides id skyline skyline
    it "identical jumps do not collide" $
      let
        skyline = [Jump (100,101) 7]
      in
        skyCollides not skyline skyline

    it "two fills above each other don't collide" $
      let
        sky1 = [Fill (0,0) 10]
        sky2 = [Fill (0,1) 10]
      in
        skyCollides not sky1 sky2

    it "two fills next to each other don't collide" $
      let
        sky1 = [Fill (0,0) 10]
        sky2 = [Fill (0,11) 100]
      in
        skyCollides not sky2 sky1

    it "two overlapping fills do collide" $
      let
        sky1 = [Fill (0,0) 10]
        sky2 = [Fill (5,0) 20]
      in
        skyCollides id sky1 sky2

    it "two touching fills do collide" $
      let
        sky1 = [Fill (0,0) 10]
        sky2 = [Fill (10,0) 20]
      in
        skyCollides id sky1 sky2

    it "one fill inside another do collide" $
      let
        sky1 = [Fill (0,0) 10]
        sky2 = [Fill (1,0) 9]
      in
        skyCollides id sky1 sky2


    it "two component calls don't collide" $
      let
        sky1 = [Fill (0,0) 10, Vert (10,0) 10]
        sky2 = [Fill (0,5)  5, Vert ( 5,5) 10]
      in
        skyCollides not sky1 sky2

    it "two component calls where both lines collide" $
      let
        sky1 = [Fill (0,0) 10, Vert (10,0) 10]
        sky2 = [Fill (5,0) 10, Vert (10,0)  5]
      in
        skyCollides id sky1 sky2

    it "two component calls where the vertical lines collide" $
      let
        sky1 = [Fill (0,0) 10, Vert (10,0) 10]
        sky2 = [Fill (5,3) 10, Vert (10,3)  6]
      in
        skyCollides id sky1 sky2

    it "two component calls that cross but don't collide" $
      let
        sky1 = [Fill (0,0) 10, Vert (10,0) 10]
        sky2 = [Fill (5,5) 15, Vert (15,5) 15]
      in
        skyCollides not sky1 sky2

    it "Fill and Vert that cross but don't collide" $
      let
        sky1 = [Fill (5,5) 20]
        sky2 = [Vert (10,0) 10]
      in
        skyCollides not sky1 sky2


    it "detects collision when start point of Vert touches end point of Fill" $
      let
        sky1 = [Fill (0,0) 5]
        sky2 = [Vert (5,0) 10]
      in
        skyCollides id sky1 sky2

    it "detects collision when start point of Vert lies on Fill" $
      let
        sky1 = [Fill (0,0) 10]
        sky2 = [Vert (5,0)  5]
      in
        skyCollides id sky1 sky2

    it "detects collision when end point of Vert lies on Fill" $
      let
        sky1 = [Fill (0, 0) 10]
        sky2 = [Vert (5,-5)  0]
      in
        skyCollides id sky1 sky2

    it "no collision when Vert and Fill cross" $
      let
        sky1 = [Fill (0,5) 10]
        sky2 = [Vert (5,0) 10]
      in
        skyCollides not sky1 sky2

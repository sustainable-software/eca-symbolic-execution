module ParserSpec where

import Test.Hspec

import Text.Megaparsec

import Parser
import AST

import SpecUtil
import Util

spec :: Spec
spec = do
  describe "parser" $ do
    it "parses an integer expression" $ do
      testParse pExpr "100" `shouldBe` 100
      testParse pExpr "100  " `shouldBe` 100

    it "parses an identifier" $ do
      testParse pExpr "foo" `shouldBe` Var "foo"

    it "parses some expressions" $ do
      testParse pExpr "1 + 2" `shouldBe` 1 + 2
      testParse pExpr "-42" `shouldBe` (-42)
      testParse pExpr "-42 * x" `shouldBe` (-42) * (Var "x")
    -- Unary operators of the same precedence can only occur once
    -- http://hackage.haskell.org/package/parser-combinators-1.0.1/docs/Control-Monad-Combinators-Expr.html
    -- it "parses multiple unary operators" $ do
        -- pendingWith "need to investigate makeExprParser"
      -- testParse pExpr "--42" `shouldBe`
        -- (UnaryOp Neg (UnaryOp Neg $ IntConst 42))

    it "parses precedence correctly" $ do
      testParse pExpr "1 * 2 + 3" `shouldBe`
          (1 * 2) + 3
      testParse pExpr "1 + 2 * 3" `shouldBe`
          1 + (2 * 3)

    it "parses mixed negation and subtract" $ do
      testParse pExpr "1 - - 2" `shouldBe`
        BinaryOp Subtract (IntConst 1) (UnaryOp Neg $ IntConst 2)
      testParse pExpr "1 - -2" `shouldBe`
        BinaryOp Subtract (IntConst 1) (UnaryOp Neg $ IntConst 2)
      testParse pExpr "1--2" `shouldBe`
        BinaryOp Subtract (IntConst 1) (UnaryOp Neg $ IntConst 2)

    it "parses parenthesized expressions" $ do
      testParse pExpr "1 * (2 + 3)" `shouldBe`
        BinaryOp Multiply
          (IntConst 1)
          (BinaryOp Add (IntConst 2) (IntConst 3))

    it "parses boolean expressions" $ do
      testParse pExpr "true" `shouldBe` BoolConst True
      testParse pExpr "false" `shouldBe` BoolConst False
      testParse pExpr "true && false" `shouldBe`
        BinaryOp And (BoolConst True) (BoolConst False)
      testParse pExpr "! true && ! false" `shouldBe`
        BinaryOp And
          (UnaryOp Not $ BoolConst True)
          (UnaryOp Not $ BoolConst False)
      testParse pExpr "! (true && ! false)" `shouldBe`
        UnaryOp Not (BinaryOp And
          (BoolConst True)
          (UnaryOp Not $ BoolConst False))
      testParse pExpr "true || true && false" `shouldBe`
        BinaryOp Or
          (BoolConst True)
          (BinaryOp And (BoolConst True) (BoolConst False))

    it "parses relational operators" $ do
      testParse pExpr "5 <= 7" `shouldBe`
        BinaryOp LessEq (IntConst 5) (IntConst 7)
      testParse pExpr "5 + 1 <= 7 * 3" `shouldBe`
        BinaryOp LessEq
          (BinaryOp Add (IntConst 5) (IntConst 1))
          (BinaryOp Multiply (IntConst 7) (IntConst 3))

    it "parses function call expr" $ do
      testParse pExpr "foobar(1,2,true,quux)" `shouldBe`
        FuncCall "foobar" [IntConst 1, IntConst 2, BoolConst True, Var "quux"] (pos 1 21)


    it "parses an assignment statement" $ do
      testParse pStmt "{x=1;}" `shouldBe`
        (Block
          [Assign (pos 1 2) "x" (IntConst 1)]
        )
    it "parses multiple statements" $ do
      testParse pStmt "{x=1;  x=5+true;}" `shouldBe`
        (Block
          [Assign (pos 1 2) "x" 1
          ,Assign (pos 1 8) "x" (5 + (BoolConst True))
          ]
        )
      testParse pStmt "{     x; x=1;      y; x=5+true;}" `shouldBe`
        (Block
          [ExprStmt (Var "x")
          ,Assign (pos 1 10) "x" 1
          ,ExprStmt (Var "y")
          ,Assign (pos 1 23) "x" (5 + (BoolConst True))
          ]
        )

    it "gives an error message for trailing garbage" $ do
      testParse' (between sc eof pStmt) "x=1;;" `shouldSatisfy` isError "dummyFileName:1:5:"


    it "parses conditionals" $ do
      testParse pStmt "{if( true )      x; else      y;}" `shouldBe`
        (Block
          [IfThenElse (pos 1 2) (BoolConst True)
            (ExprStmt $ Var "x")
            (ExprStmt $ Var "y")]
        )
      testParse pStmt "{if( x <= 2 ) x=5; else x=100;}" `shouldBe`
        (Block [IfThenElse (pos 1 2)
          (BinaryOp LessEq (Var "x") 2)
          (Assign (pos 1 15) "x" 5)
          (Assign (pos 1 25) "x" 100)
        ]
        )

    it "parses conditionals with multiple statements" $ do
      testParse pStmt "{if( true ) {      x;      y; } else {     x;      y;}}"
        `shouldBe`
          (Block [IfThenElse (pos 1 2)
            (BoolConst True)
            (Block [ExprStmt $ Var "x", ExprStmt $ Var "y"])
            (Block [ExprStmt $ Var "x", ExprStmt $ Var "y"])
          ]
          )

    it "parses a component call" $ do
      testParse pStmt "{L3.switchOn();}" `shouldBe`
        (Block [ExprStmt $ CompCall (pos 1 2) "L3" "switchOn"])

    it "parses a while statement" $ do
      testParse pStmt "{while( x <= 10 ) { x = x + 1; }}" `shouldBe`
        (Block [While
          (pos 1 2)
          (BinaryOp LessEq (Var "x") 10)
          (Block [(Assign (pos 1 21) "x" ((Var "x") + 1))])
          (pos 1 32)
          0
        ]
        )

    it "parses a function definition with empty body" $ do
      testParse pDefinition "int foobar(int x, int y) { }" `shouldBe`
        (FunDef $ FunctionDefinition
          TypeInt
          (pos 1 5)
          "foobar"
          [ FormalParameter TypeInt "x"
          , FormalParameter TypeInt "y"
          ]
          (pos 1 26)
          (Block [])
          (pos 1 28))
    it "parses a function definition with non-empty body and return statement" $ do
      testParse pDefinition
        "int myFunction(int x, int y) { z = x + y; return x; }   " `shouldBe`
        (FunDef $ FunctionDefinition
          TypeInt
          (pos 1 5)
          "myFunction"
          [ FormalParameter TypeInt "x"
          , FormalParameter TypeInt "y"
          ]
          (pos 1 30)
          (Block
            [ Assign (pos 1 32) "z" (BinaryOp Add (Var "x") (Var "y"))
            , Return (pos 1 43) (Var "x")
            ])
          (pos 1 53))

    it "parses a function call statement" $ do
      testParse pStmt "{foobar(1,true,quux);}" `shouldBe`
        (Block [ExprStmt $ FuncCall "foobar" [IntConst 1, BoolConst True, Var "quux"] (pos 1 20)
        ])

    it "parses a void function" $ do
      testParse pDefinition
        "void foo() { z = x; }" `shouldBe`
        (FunDef $ FunctionDefinition
          TypeVoid
          (pos 1 6)
          "foo"
          []
          (pos 1 12)
          (Block
            [ Assign (pos 1 14) "z" (Var "x")
            ])
          (pos 1 21))

    it "parses an if with missing else" $ do
      testParse pStmt
        "if (x) y = 10;" `shouldBe`
        (IfThenElse
          (pos 1 1)
          (Var "x")
          (Assign (pos 1 8) "y" (IntConst 10))
          (Block []))

    it "parses a nested if with missing else" $ do
      testParse pStmt
        "if (x) y = 10; else if (y) y = 20;" `shouldBe`
        (IfThenElse
          (pos 1 1)
          (Var "x")
          (Assign (pos 1 8) "y" (IntConst 10))
          (IfThenElse
            (pos 1 21)
            (Var "y")
            (Assign (pos 1 28) "y" (IntConst 20))
            (Block [])
          )
        )

    it "parses a nested if with dangling else" $ do
      testParse pStmt
        "if (x) if (y) y = 20; else x = 10;" `shouldBe`
        (IfThenElse
          (pos 1 1)
          (Var "x")
          (IfThenElse
            (pos 1 8)
            (Var "y")
            (Assign (pos 1 15) "y" (IntConst 20))
            (Assign (pos 1 28) "x" (IntConst 10))
          )
          (Block [])
        )

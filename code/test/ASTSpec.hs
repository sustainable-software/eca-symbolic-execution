{-# LANGUAGE QuasiQuotes #-}
module ASTSpec where

import Text.Heredoc
import Test.Hspec
import Data.Tree
import Data.Map ((!))
import qualified Data.Set as Set
import qualified Data.Map as Map

import AST
import Util
import SpecUtil
import Parser

testFindWhileLoops :: String -> String -> (WhileTree,Int) -> Expectation
testFindWhileLoops program funName expectedResult =
  findWhileLoops funDef `shouldBe` expectedResult
  where
  (prog,_) = testParse pProgram program
  funDef = prog!funName

testFindUsedComponents :: String -> String -> [String] -> Expectation
testFindUsedComponents program funName expectedResult =
  findUsedComponents funDef `shouldBe` (Set.fromList expectedResult)
  where
  (prog,_) = testParse pProgram program
  funDef = prog!funName

spec :: Spec
spec = do
  describe "findWhileLoops" $ do
    it "finds no while loop in a program that doesn't contain any" $ do
      testFindWhileLoops [here|
        int main()
        {
          x = TERM.readInt();
          if( x <= 10 )
            { LED1.switchOn(); }
          else
            { LED2.switchOff(); }
          return 0;
        }
        |]
        "main"
        ([ ],0)

    it "finds one while loop on top level" $ do
      testFindWhileLoops [str|
        |int main()
        |{
        |  x = TERM.readInt();
        |  if ( x <= 10 )
        |  { LED1.switchOn(); }
        |  else { LED2.switchOff(); }
        |  while( x <= 10 )
        |  {
        |  }
        |}
        |]
        "main"
        ([Node (pos 8 3, pos 10 3, 0) []],1)

    it "finds two while loops on top level" $ do
      testFindWhileLoops
        [str|int main()
        |{
        |  x = TERM.readInt();
        |  while( true )
        |  {
        |  }
        |  if( x <= 10 )
        |  { LED1.switchOn(); }
        |  else { LED2.switchOff(); }
        |  while( x <= 10 )
        |  {
        |  }
        |}
        |]
        "main"
        ([ Node (pos  4 3, pos  6 3, 0) []
         , Node (pos 10 3, pos 12 3, 0) []
         ], 1)

    it "a while loop inside a conditional is still top level" $ do
      testFindWhileLoops
        [str|int main()
        |{
        |  x = TERM.readInt();
        |  if( x <= 10 )
        |  {
        |     while( true )
        |     {
        |     }
        |  }
        |  else { LED2.switchOff(); }
        |}
        |]
        "main"
        ([ Node (pos 6 6, pos 8 6, 0) []
         ], 1)

    it "finds two nested while loops" $ do
      testFindWhileLoops
        [str|int main()
        |{
        |  x = TERM.readInt();
        |  if( x <= 10 )
        |  {
        |     while( true )
        |     {
        |       while( true )
        |       {
        |       }
        |     }
        |  }
        |  else { LED2.switchOff(); }
        |}
        |]
        "main"
        ([ Node (pos 6 6, pos 11 6, 0)
           [ Node (pos 8 8, pos 10 8, 1) []
           ]
         ], 2)

    it "finds two loops nested inside one" $ do
      testFindWhileLoops
        [str|int main()
        |{
        |  x = TERM.readInt();
        |  if( x <= 10 )
        |  {
        |     while( true )
        |     {
        |       if( x <= 100 )
        |       {
        |         while( true )
        |         {
        |         }
        |       } else {
        |         while( true )
        |         {
        |         }
        |       }
        |     }
        |  }
        |  else { LED2.switchOff(); }
        |}
        |]
        "main"
        ([ Node (pos 6 6, pos 18 6, 0)
           [ Node (pos 10 10, pos 12 10, 1) []
           , Node (pos 14 10, pos 16 10, 1) []
           ]
         ], 2)

    it "finds while loops in functions other than main" $ do
      let
        inputSource =
          [str|int main()
          |{
          |  x = TERM.readInt();
          |  if( x <= 10 )
          |  {
          |     while( true )
          |     {
          |       if( x <= 100 )
          |       {
          |         while( true )
          |         {
          |         }
          |       } else {
          |         while( true )
          |         {
          |         }
          |       }
          |     }
          |  }
          |  else { LED2.switchOff(); }
          |}
          |
          |int foobar(int x, int y)
          |{
          |   while( true )
          |   {
          |   }
          |}
          |]
      testFindWhileLoops inputSource "main"
        ([ Node (pos 6 6, pos 18 6, 0)
           [ Node (pos 10 10, pos 12 10, 1) []
           , Node (pos 14 10, pos 16 10, 1) []
           ]
         ], 2)
      testFindWhileLoops inputSource "foobar"
        ([ Node (pos 25 4, pos 27 4, 0) []
         ], 1)

  describe "findUsedComponents" $ do
    it "finds some components in main" $ do
      testFindUsedComponents
        [here|
        int main()
        {
          LED1.switchOn();
          LED2.switchOff();
          WTF.ascend();
        }

        int foo()
        {
          LED3.switchOff();
        }
        |]
        "main"
        ["WTF", "LED2", "LED1"]
    it "finds a component in foo" $ do
      testFindUsedComponents
        [here|
        int main()
        {
          LED1.switchOn();
          LED2.switchOff();
          WTF.ascend();
        }

        int foo()
        {
          LED3.switchOff();
        }
        |]
        "foo"
        ["LED3"]
    it "finds no components in main" $ do
      testFindUsedComponents
        [here|
        int main()
        {
        }

        int foo()
        {
          LED3.switchOff();
          LED1.switchOn();
          LED2.switchOff();
          WTF.ascend();
        }
        |]
        "main"
        []
    it "finds components in expressions" $ do
      testFindUsedComponents
        [here|
        int main()
        {
        }

        int id(int x)
        {
          return x;
        }

        int foo()
        {
          return LED3.switchOff() + id(LED1.switchOn()) + LED2.switchOff() + WTF.ascend();
        }
        |]
        "foo"
        ["LED3", "LED1", "LED2", "WTF"]
  describe "callGraph" $ do
    let
      evenOddSourceCode = [here|
        int even(int n)
        {
          if( n == 0 )
          {
            LED1.switchOn();
            return true;
          }
          else
          {
            LED1.switchOff();
            return odd(n-1);
          }
        }
        int odd(int n)
        {
          if( n == 0 )
          {
            LED2.switchOn();
            return false;
          }
          else
          {
            LED2.switchOff();
            return even(foo(n-1));
          }
        }

        bool between(int lower, int x, int upper)
        {
          return lower <= x && x <= upper;
        }

        int main()
        {
          x = TERM.readInt();
          if( between(1,x,20) )
          {
            return even(x);
          }
          else
          {
            LED3.switchOn();
          }
        }

        int foo() { return 0; }
        |]
      (prog,_) = testParse pProgram evenOddSourceCode
    it "determines the call graph for the even-odd program" $ do
      let
        cg = callGraph prog
      -- remove FunctionDefinitions, for easier testing
      Set.fromList [(funName,calledFunctions)|(_,funName,calledFunctions) <- cg] `shouldBe` Set.fromList
        [ ("main",    ["between", "even"])
        , ("between", [])
        , ("even",    ["odd"])
        , ("odd",     ["even", "foo"])
        , ("foo",     [])
        ]
    it "determines used components for the even-odd program" $ do
      determineUsedComponents prog `shouldBe` Map.fromList
        [ ("main",    ["LED1", "LED2", "LED3", "TERM"])
        , ("between", [])
        , ("even",    ["LED1", "LED2"])
        , ("odd",     ["LED1", "LED2"])
        , ("foo",     [])
        ]


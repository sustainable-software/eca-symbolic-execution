#!/bin/bash

# abort when there are compile errors
#stack build || exit 1
stack --profile build || exit 1

# run program

# stack-limiting technique
#stack --profile exec eca -- +RTS -K96K -xc -RTS --foobar programs/many-fragments.eca
#exit 0

# heap profiling
#cp eca.prof eca-old.prof
stack --profile exec eca -- +RTS -hc -RTS --foobar programs/many-fragments.eca
#stack exec eca -- +RTS -hT -RTS -s programs/many-fragments.eca --merge

# save old profile
cp eca.ps eca-old.ps

# convert profile to postscript
stack exec hp2ps -- -c eca.hp


\section{Application domain and limitations}
\label{sec:applications}

%\mkl{Note for Bernard, regarding extension to different resource types. SECA can handle any kind of \emph{reusable} resource. These are resources that will not be used up, but only temporarily occupied. When the hardware component enters a low-demanding state, the resource will become available again. Examples of such resources are people, chairs, tools, storage space (both physical and computer memory), but also all kinds of rates or flows: power is basically energy flow, cool water flow, hydraulic pressure. This is because they all behave according to the arithmetic operations we use in the semantics: they can be added and subtracted to give the current state of resource "occupation"}

%Like it was the case for the conference paper, the last part of the conclusion of the present submission mention potential application to other resources but without any details. It is perfectly fine that the extended version does not contain a complete brand new methodology regarding (cross checking of) those quantities. However, it would be great to have a longer discussion on potential and on limits. 

SECA can be used to predict the power draw of devices controlled by software.
We envision that our method can be used by developers of control software, who can get additional insights in the power draw behaviour of the software they are developing. They can use this information to reduce the energy consumption of their programs. 
This includes control systems in factories, cars, aeroplanes, or smart-home applications.
Examples of hardware components range from heaters to engines, motors and urban lighting.
In this manuscript we take resource consumption as electrical power draw, but since the analysis sees it as a number without units, it can also be taken as gas, water, or any other resources whose consumption increases monotonically.
It can also be used to calculate the emissions or production rates of components controlled by software. For a combustion engine controlled by software, the finite state hardware model can be labelled with the $CO_2$ emission rate. Our method can then visualize the $CO_2$ emission rate of a system, which can then be optimized by the programmer. Our method is not suitable for single-use resources, such as rounds of ammunition, as we do not track the current stock of these resources. These resources do not replenish themselves, as we assume is the case with electricity.

We do not analyse the power draw of the processor running the software.
Processors are hard to statically analyse due to advanced optimizations affecting timing and energy, such as register-by-pass and caching.
Compared to the power draw of the controlled peripherals, the power draw of processors is in many applications negligible.
For example, in a system where a programmable thermostat controls the central heating of a house, the big energy spender is the heating system, not the thermostat's processor.
Let's assume that the thermostat uses an 8-bit Atmel ATmega328p, which is often used in control applications.
When running at 1 Mhz it consumes in its most active state 0.2 mA at 1.8 V, according to its data sheet.
This translates to a power draw of 0,00036 W, which is 4665.6 J per hour, or 32110656 J (roughly 32 MJ) per year.
If using power save modes, which are according to the data sheet about 25 times more energy efficient, this number can be reduced significantly.
A typical modern house in the Netherlands uses about 17000 MJ for heating per year.
Compared to that, both the power draw and energy consumption of the processor controlling the heating is not significant.

The validity of applying our method in practice depends on several factors besides the symbolic execution being implemented correctly.
The quality of the results depends directly on the quality of the component models.
We impose severe restrictions on component models, foremost that the power draw is assumed to be constant in every state of the component, and that all state changes have a corresponding statement in our high level language.
This is in practice not true for most devices; for example, the power draw in a state can change over time, or there are implicit state changes where devices can enter a power save mode automatically, or carry out other actions based on a timer.
Such fluctuations in power draw could easily be incorporated in our approach by giving lower and upper bounds for power consumption in every logical state, and these bounds can be rendered to the software engineer.

Such bounds can be derived from hardware specifications or measurements, but in general providing realistic bounds is hard.
When component models are based on specifications from hardware vendors, errors in the specifications are transferred to the component models.
Hardware production errors, and eventually the degradation of hardware, can induce erratic power consumption behaviour that is neither reflected in the specification nor the component model.
Revisions of hardware, possibly undocumented, can result in deviating power behaviour between units of different production batches.

Deriving bounds from a measurement setup is hard, because precise power measurements are difficult.
Small differences in power consumption are hard to measure correctly, and external conditions like temperature can influence the results greatly.
Then there is the problem of extrapolating measurement results of a limited number of test devices.
The likelihood of the test devices coming from the same production batch is high, and might not be representative for users on the other side of the world.
Devices might operate in a different climate (temperature and humidity), or use different power grids (110V vs 230V), which result in different efficiency of the device.
An additional problem is that regular wear can result in increased power draw over the lifetime of a device, which the extrapolation should include.
Inferring the number of power states of hardware components from measurements is a tough problem in itself.
It is hard to correlate power measurements with power state, and to know if a piece of hardware can actually be modelled with the model restrictions in place.

Even with all these threats to validity, the strength of our approach is the ability to abstract from hardware.
This is useful for example during development, when the final decision which hardware components to use has not been made, or when an abstract hardware model should be considered.
We observe that many decisions are based on relative properties between systems.
Abstract hardware models can be used to focus on the relative differences between component methods and component states.
A class of applications where our approach can be useful is the production of many variations of the same device.
Variations occur based on local requirements, or on regional differences in the electric grid, or on different requirements set by integrators or consumers, or any combination thereof.
SECA allows for quickly designing and analysing those variations, and having a clear view on how changes will impact all those variations.

The control software that is used in our envisioned application domain is restricted in the number of language features that it is allowed to use. Often, it is required that such embedded systems adhere to a coding standard such as the MISRA guidelines. Many other coding standards are derived from these MISRA guidelines. For example, the NASA Jet Propulsion Laboratory C Coding Standards for safety critical systems are derived from the MISRA 2004 guidelines for C. The objective of these coding styles is to increase the reliability and readability of the code. The coding style also creates opportunities to analyse code adhering to these coding styles using automatic tooling, because scalability and completeness issues with analysis techniques used in these tools are often linked with code not adhering to these coding standards.

This is also the case for the symbolic execution method we are proposing: aliasing as can occur in C programs is hard to support with symbolic execution. However, the coding standard often used for control software forbid aliasing. The control software additionally does not have dynamic memory allocation, nor use pointer arithmetic. The coding standard only allows one form of unbounded looping: \verb|while(true) { ... }|. This form of loop occurs e.g.\ in control software that has a simple control loop, after some setup is performed.

This loop is supported in our method; however, using this loop may yield an unsound analysis, as our analysis exits after a certain number of loops. A clear warning is given in that case.
In such situations there may be paths whose power draw is not reported, giving an unsound analysis.
However, due to the nature of symbolic execution, all possible power behaviours of a loop are often discovered in fewer iterations than what is needed for the behaviours to occur in concrete execution.
We expect situations with missed power behaviours to be uncommon in typical control programs.

Software-level concurrency is absent from such control software (no threads, nor processes). However, the systems are often concurrent: different components can be running at the same time. We assume that changes in power draw of these components can only occur after an explicit component call in the controlling software. With this assumption, we can analyse these systems. However, this assumption may not hold for all systems. In such a case, our method can be used for approximations of the power draw.

% >>>> Wrt transformations made by the compiler, e.g. the various optimisations applied, such as loop unrolling, inlining, etc, how does the proposed analysis technique handle these? Real control applications will be optimised during compilation, creating a significant difference between the source code and the code that is actually being executed. How does the technique cope with that, i.e. how is the SECA representation of the code aligned with the actual control application executable? How does that impact on analysability, in particular state-space explosion?
Optimizations and transformations of compilers can be a source of different behaviour when doing source code analysis. In practice, we assume that those optimizations and transformations are not changing the order nor the number of component calls. As only component calls can change the power draw in our analysis, the attribution of power draw changes to component calls remains consistent. Note that optimizations and transformations that change the execution time do not affect our analysis, as long as the order and number of component calls remain the same. Our analysis does not track execution time, and is not impacted by changes in execution time (the source code line number remains the same).

% TODO: Can the SECA language description be “derived” automatically from source code written in more commonly used languages in the given domain, e.g. C?
Most control software is written in C. We designed our SECA language so that transforming a control software program written in C is straightforward, as the control flow, execution behaviour, and keywords are the same. There are still a couple of things to do. First, the \verb|#include| statements should be stripped and implementations of library calls should be provided (so they can be symbolically evaluated). Next, the hardware interactions should be made explicit. There are numerous ways that control software can interact with hardware, which is highly dependent on the processor and operating system used. This can include writing to special memory location or registers, and therefore specific domain knowledge is needed. These interactions should be converted to a component call. And for every component call, both a power draw value and an implementation of the effect should be made available. The effect is needed, as component calls can return values, and change the state and behaviour of future component calls.

While it would be simple to allow arguments to the component calls for the concrete component models, it would require significant changes to our prototype implementation. This would complicate the symbolic execution: instead of concrete hardware component models, symbolic hardware component models are needed. Symbolically executing a component function can result in multiple symbolic values with different path constrains, each resulting in different power draw. This complicates the visualization too as skylines can be split during component calls, which the authors consider not intuitive.
For this paper we have decided to keep our approach and prototype implementation simple and support no arguments for component calls. We intend to explore this in the future.

%Power vs energy: the difference between power and energy is not sufficiently clear – they appear to be used interchangeably? E.g. the symbolic execution engine is said to track the *power draw* of components controlled by the program, resulting in a graph that correlates source code lines with *energy* consumption, but the x-axis of the graph is labelled “power draw” and measured in mW. Where does the execution time get considered to get from power to energy? And, does it track average or peak power?


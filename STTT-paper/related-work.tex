\section{Related Work}
\label{sec:relatedWork}

\newcommand{\term}[1]{#1}
\newcommand{\uppaal}{\term{Uppaal}}
\newcommand{\jouletrack}{\term{JouleTrack}}
\newcommand{\wattch}{\term{Wattch}}
\newcommand{\seprof}{\term{SEProf}}
\newcommand{\raml}{\term{RAML}}
\newcommand{\cegar}{\term{CEGAR}}

Directly related are the second author's previous works \citep{GastelKE2015,DBLP:journals/corr/GastelE17} describing static energy analyses for the language ECA.
The first derives energy bounds for a specific input scenario; the second is a symbolic analysis that over-approximates all possible paths.
These works do not use skylines.
They do use hardware models that also support incidental one-time energy costs.
This incidental energy cost can be useful for approximating energy consumption that varies over time.
Also closely related is the first author's previous work \citep{KlinikJP2017}, which introduces skylines, but also overapproximates since it estimates resource use over time.

Most publications on energy efficiency of software approach the problem on a high level, defining programming and design patterns for writing energy-efficient code, for example \citep{Albers10,Ranga10,Saxe10}.
\citet{Cohen:energytypes} and \citet{Sampson:typeslowconsumption2011}, divide a program into \emph{phases} describing similar behaviour.
Based on the behaviour of the software, design-level optimizations are proposed to achieve lower energy consumption.
Petri-net-based energy modelling techniques for embedded systems are proposed by \citet{Oliveira06,Nogueira11}.

A general analysis for resource consumption is described by \citet{DBLP:conf/jtres/KerstenSGME12}.
There are generic resource consumption analyses, built on techniques such as solving recurrence relations \citep{Albert2008}, amortized analysis \citep{HAH11}, separation logic \citep{Atkey10}, and a Vienna Development Method style program logic \citep{Aspinall2007}.
Contrary to these approaches, our method has an explicit hardware model and a context in the form of component states. This enables the inclusion of state-dependent energy consumption.

\citet{JayaseelanML06} and \citet{energy-XMOS-2013} analyse energy consumption of the processor running embedded software for specific architectures (SimpleScalar and XMOS ISA-level models, respectively), while our approach is hardware-parametric and focuses on external hardware.
Several tools perform a static analysis of the energy consumption of the CPU based on per-instruction measurements, such as in \citep{jouletrack,wattch}.
Symbolic execution of hardware properties has been done in \citep{DBLP:conf/vlsi/GastelVS14}, which derives channel types of communication channels in hardware. These channel types are used to check for correctness and absence of deadlocks.

\citet{LiG2016} present an energy analysis for Java programs based on low-level \emph{energy operations}.
Energy operations are atomic for their analysis, but can themselves be composed of multiple hardware instructions. Examples for energy operations are assignments, arithmetic operations, method invocations, or array lookups.
They measure the energy costs of energy operations, but also of blocks of source code of the program under analysis, by running it many times with different inputs, using the on-board sensors of a mobile phone development board to measure CPU voltage and current.
The energy consumption hotspots thus identified give hints as to where the greatest optimization potential lies.
They manage to halve the energy consumption of their example program, a game engine for mobile devices.
The two biggest differences with our method is that first, they analyse the energy cost of the algorithmic structure of the software under analysis, while we are focused on control software, where CPU cycles are negligible in comparison to the hardware components they control.
Second, their analysis is not static, but requires running the program under analysis many times to ensure good coverage of all code paths.
Our analysis uses symbolic execution to analyse all possible code paths without running the program.

\citet{NouriBMLB2016} describe a system to analyse the time performance of parallel algorithms.
Their system ASTROLABE allows probabilistic proofs of properties in temporal logic, for example that the overall execution time of a given algorithm is bounded, or that the variability of the execution times of a subroutine is bounded.
ASTROLABE is based on modelling the algorithm under analysis with a network of labelled transition systems.
Each labelled transition system represents a function running on one CPU core, and they can communicate with each other using FIFOs and semaphores, which represent buses connecting the cores.
At first, those models don't have any timing information.
To annotate transitions with timing information, they generate instrumented executable code from the model, which they then run on a real multi-core embedded chip.
Running the code produces traces with time profiling, which they then use to infer probability distributions for some of the transitions in their model.
Once the models are equipped with timing information, they use statistical model checking to prove the desired properties.
They write that energy consumption analysis would be possible with ASTROLABE, in their section on future work.
The difference between ASTROLABE and SECA is that they analyse properties of the algorithm itself, while we analyse how the given program drives external hardware components.
Furthermore, SECA does not require running code generated from the model on real hardware, to obtain good path coverage.
Instead, we run the model in a symbolic execution semantics, to cover all possible paths.

\citet{DalsgaardOTHL2010} present a worst-case execution time (WCET) analysis for programs compiled to embedded systems.
Their analysis takes instruction pipelines and caching into account.
Their system, METAMOC, is based on the model checker UPPAAL and UPPAAL's built-in ability to infer WCET for timed automata.
In METAMOC, the processor pipeline, the cache, and the memory are modelled as UPPAAL timed automata.
The processor pipeline and memory model have to be written by hand.
The cache model is derived from a high-level specification.
The program under analysis must be given as an executable for the modelled processor's architecture, for example ARM7.
METAMOC comes with a disassembler, value analysis, and compiler that turns real executables into UPPAAL automata, such that memory accesses of the real program map to synchronizations with the memory model.
All those parts are then combined into one single UPPAAL model.
Using the built-in WCET analysis from UPPAAL, they are able to infer timings for almost all example programs in a well-known benchmark suite.
The difference with SECA is that METAMOC infers a property of the algorithm itself, while SECA analyses how the external hardware components change during program execution.
Their results are impressive, but since METAMOC uses UPPAAL's bulit-in WECT analysis, it is hard to imagine how it can be adapted to analyse other kinds of resources, like power consumption.

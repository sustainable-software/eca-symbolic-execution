\section{Methodology}
\label{sec:methodology}

\newcommand{\bigpicturefigure}{
\begin{figure*}
\center%
\begin{minipage}{1.0\linewidth}%
\center%
\subfloat[]{%
  \begin{minipage}{0.22\columnwidth}%
  \lstinputlisting[language=SECA,numbers=left,xleftmargin=2em]{programs/split-and-merge.eca}%
  \end{minipage}%
  \label{fig:splitAndMergeCode}%
}%
\subfloat[]{%
  \begin{minipage}{0.22\columnwidth}%
  \def\svgwidth{1.0\textwidth}%
  \input{split-and-merge-main-no-merge.pdf_tex}%
  \end{minipage}%
  \label{fig:splitAndMergeNoMergee}%
}%
\subfloat[]{%
  \begin{minipage}{0.22\columnwidth}%
  \def\svgwidth{1.0\textwidth}%
  \input{split-and-merge-main-yes-merge.pdf_tex}%
  \end{minipage}%
  \label{fig:splitAndMergeYesMerge}%
}
\caption{\protect\subref{fig:splitAndMergeCode} A program with two execution paths
\protect\subref{fig:splitAndMergeNoMergee} Its skylines
\protect\subref{fig:splitAndMergeYesMerge} Its merged skylines
\CK{Note that the paper would look better if this was one page earlier (that would put the figure on the same page where it is discussed).
(Even more beautiful if we didn't get the ugly 3 lines of this section on the next page, but hey, we can't have everything.)}
}%
\end{minipage}%
\end{figure*}}

Given a program in the SECA language (defined in \cref{sec:ecaLanguage}), our system performs symbolic execution to examine all possible execution paths.
For each path, the symbolic execution engine tracks the power draw of all components that the program controls, resulting in a graph that relates program points to power draw (\cref{sec:skylines}).
%Program points are individual statements and parts of expressions, which are handled as a whole in a programming language (discussed in detail below).
We call such graphs \emph{skylines}.
The result of symbolic execution is a set of skylines for every function, considering all calls to a function, across all execution paths (\cref{sec:symbolicExecution}).
Our system then condenses these skylines into a summary of the power behaviour of the program by \emph{merging} common segments, to emphasize where skylines differ (\cref{sec:merging}).
This approach is applied to a real-world example: a line following robot is analysed (\cref{sec:robot}).
The merged skylines are rendered as \emph{skyline diagrams}, with line numbers on the vertical axis and power draw on the horizontal axis.
We integrated the tooling in the Visual Studio Code IDE (\cref{sec:ide}): these skyline diagrams are displayed alongside the code in which the line numbers of the skylines and code are aligned. The skylines are automatically adjusted when the code is changed.
This presentation of both the code and skylines together makes it easier to understand these skylines.
Finally, we describe the application domain and its limitations (\cref{sec:applications}),
related work (\cref{sec:relatedWork}),
and ideas for future work (\cref{sec:futureWork}).

\bigpicturefigure

\topicSentence{The domain we focus on is control software, whose main purpose is to control hardware like sensors and actuators like motors.}
Embedded systems in this domain typically use low-powered microprocessors, which have a negligible energy use compared to the software-controlled hardware.
Control software has two key characteristics.
First, it has low algorithmic complexity.
We aim to analyse programs that, for example, regulate a central heating installation, not those that calculate square roots.
Common control programming guidelines prohibit or discourage recursion, nested loops, unbounded loops or loops with changing stop conditions, and dynamic memory allocations. See the next section for details about the coding styles.
If programs have parts with high algorithmic complexity, which would overextend the capabilities of symbolic execution, such parts could be hidden in library calls and left out of the analysis.
Second, control software contains statements that interact with hardware components.
These \emph{component calls} are the focus of our system, as we seek to find how their invocation influences the power behaviour of the embedded system running the program.

SECA represents the behaviour of hardware components in a model similar to the one by \citet{Gastel2017}; essentially every sensor and actuator is modelled by a separate labelled transition system where every state has a power draw, and state changes can only be initiated by the code. The power draw of the whole system is the power draw of the state of every component combined.

\topicSentence{Resources other than power, such as gas or water, can also be modelled, as long as they can be summed up.}
The analysis does not care; it sees resource consumption as a unitless number.
We assume that components have rectangular power profiles: there is no ramp-up when switching them on.

\topicSentence{Symbolic execution \citep{King1976} is a program semantics that traces all possible program execution paths.}
Whenever a program asks for input, for example from a sensor or terminal, a symbolic input variable $\id{any}_i$ is created.
When conditionals are encountered, execution splits into two paths: one for evaluating the condition to true, one to false.
Each path is coupled with the constraint on the symbolic inputs that must hold for this path to be followed.
To illustrate the idea, consider the following program.
\begin{SECA}
    x = TEMP.readInt();
    if (x < 5) { y=7; }
    else { y=2*x+1; }
\end{SECA}
Symbolic execution yields two paths, one through the \seca{then}- and one through the \seca{else}-branch.
The first terminates with global state $[x \mapsto \id{any}_0, y \mapsto 7]$ and path constraint $\id{any}_0 < 5$;
the second with $[x \mapsto \id{any}_0, y \mapsto 2\id{any}_0+1]$ and path constraint $\neg(\id{any}_0 < 5)$.
Path constraints can be given to an SMT (Satisfiability Modulo Theories) solver, to prune infeasible paths% with conflicting path constraints
, and calculate example values for the $\id{any}$s.

\topicSentence{Symbolic execution does not terminate if there is a path that loops indefinitely.}
To bypass this problem, our system exits loops after a pre-defined number of iterations, and generates a warning if this limit is reached.

%\bigpicturefigure

In previous work \citep{KlinikJP2017} we analysed resource consumption over time.
This has several advantages, but does not clearly show which parts of the program contribute to which parts of a skyline.
\topicSentence{Here, we give up the time aspect and instead relate resource use directly to lines in the source code.}
This requires certain coding conventions; for example, there may be only one non-trivial expression or statement per line, and closing braces must be on their own line.
The results are diagrams with a natural control flow from top to bottom with occasional jumps, which clearly relate parts of the program to their power draw.
The result of our analysis is no longer energy consumption, which requires the notion of time, but power draw, which expresses the rate of power intake at a given moment.
Consider for example the program in \cref{fig:splitAndMergeCode}.
Symbolic execution results in the two skylines in \cref{fig:splitAndMergeNoMergee}, which show the hotspots in lines 5 and 8.

Symbolic execution results in many skylines, one for every execution path.
These skylines often have identical parts, only differing after or up to a certain point.
\topicSentence{To emphasize where skylines differ, and de-emphasize where they are identical, we present an algorithm we call \emph{merging}.}
Sometimes a skyline is equal to a second one for a few lines and then becomes equal to a third.
This effect is common in loops, where a piece of code is executed repeatedly.
The program in \cref{fig:splitAndMergeCode} has two execution paths that only differ during the execution of the conditional (lines 4--10).
\Cref{fig:splitAndMergeYesMerge} shows its skylines after merging.
Up to line 4 they are drawn as a single skyline.  At line 4 is a \emph{split point}, after which they are drawn separately.
At line 11 they come together again, and continue so until the end of the function.
Our system aims to give programmers an idea of the power behaviour of their programs, so that they gain insight where the hotspots lie.
Merging comes at the cost of information loss about the exact number of runs, and losing the ability to fully trace individual runs.
We argue that for our goal it is not required that skyline diagrams convey all information about all runs.
Instead, we condense information with merging such that unexpected spikes can be clearly identified.

To support exploring power behaviour in-depth, we have created a plugin for an IDE (see \cref{sec:ide}), that allows users to focus on specific scenarios.
%By hovering over a line, the IDE will show the conditions that resulted in this power draw.
%This allows a programmer to write a test scenario, and validate and fix the scenario.
The integration in an IDE can even be extended to a visual debugger. This enables programmers to interactively explore the conditions and control flow of a program, by setting breakpoints, and constraining the symbolic execution on these breakpoints. A description of what is needed can be found in \cref{sec:ide}.

\begin{figure*}
\subfloat[]{
\begin{minipage}{0.32\textwidth}
\lstinputlisting
  [ language=SECA
  , numbers=left
  , xleftmargin=2em
  , lastline=29]
  {programs/line-follower/robot-literal.eca}
\end{minipage}%
\label{fig:lineFollowerCode}
}
\subfloat[]{
  \begin{minipage}{0.32\textwidth}
  \includegraphics[width=1\textwidth]{robot-literal-loop-loop.pdf}%
  \end{minipage}
  \label{fig:lineFollowerLoop}
}
\subfloat[]{
  \begin{minipage}{0.32\textwidth}
  \includegraphics[width=1\textwidth]{robot-literal-MoveForward-MoveForward.pdf}%
  \end{minipage}%
  \label{fig:lineFollowerMoveForward}
}
\caption{\protect\subref{fig:lineFollowerCode} Excerpt of the line follower program
\protect\subref{fig:lineFollowerLoop} The skyline of the loop function
\protect\subref{fig:lineFollowerMoveForward} The skyline of MoveForward
}
\end{figure*}

\section{A Real-World Example: Line-Following Robot}
\label{sec:robot}

\topicSentence{In this section we demonstrate how to apply our analysis to an existing real-world example, written in C.}
The program is simple enough that there is no potential to optimise power consumption, but nonetheless our analysis gives some insight into the program's power behaviour.
We chose a random ``simple line follower'' project from the Arduino project database \citep{ArduinoProjectHub}.
This robot has two motors and two sensors, and uses them to follow a black line on the floor.
It works as follows.
The sensors are positioned to the left and right of the line.
If only the left sensor sees the line, the robot turns left.
Symmetrically for the right sensor.
If neither sensor sees the line, the robot moves forward.
If both sensors see the line, the robot stops.
The code has potential for refactoring, as it contains some unnecessary repetition;
however our goal was not to find the most elegant line follower robot, but to apply our method to a real-world example.

The original source code, written in C, is almost valid SECA.
\topicSentence{We made two changes to the code for our parser to accept it.}
First, we defined the constants \texttt{LOW} and \texttt{HIGH}, and the function \texttt{delay}, which for our purpose is empty.
Second, we replaced the statements that write to output pins and read from input pins with component calls.
\Cref{fig:lineFollowerCode} shows an excerpt of the code after these adjustments.
We then created the component models for motors and sensors in the source code of our analysis engine.
The simulated motors have three states, \emph{forward}, \emph{backward}, and \emph{stop}, and corresponding component calls.
In the forward and backward states, motors have an average power draw of 150mW each. This was measured for two Lego Mindstorms NXT motors we have experimented with: 2.5V (measured with a voltage divider with a precision of 0.05V) at 0.06A (measured with ACS712 sensors with a precision of 0.058A) results in 150mW. This power draw depends on the weight of the robot, and the motor used. Another set of motors we tested used 3V at 0.08A. The difference is attributed to variance in production runs and the inprecise sensors used. For this experiment, we used the motors that measured 150mW of power draw.
We assume that the sensors have no power draw, and their \texttt{read} component call returns a symbolic value in $\{0,1\}$.

\topicSentence{The analysis result for the robot is shown in \cref{fig:lineFollowerLoop,fig:lineFollowerMoveForward} as skyline diagrams for the functions \emph{loop} and \emph{MoveForward}.}
The function \emph{loop} has many behaviours, depending on the executed conditional.
There are the cases where the power draw stays 0 or 300 mW, or it can increase in lines 10, 18, or 22, or it can decrease in line 14.
The diagram for \emph{MoveForward} shows that the function has two behaviours, one where the power draw increases in two steps from 0 to 300 mW (if the motors were off), and one where the power draw stays constant at 300 mW (if the motors were already spinning).
The functions \emph{TurnLeft} and \emph{TurnRight}, not shown here, look similar (as both motors are turned on, although in different directions).
The function \emph{Stop}, not shown, has the opposite behaviour: the power draw decreases in two steps.

\medskip\noindent\textbf{Discussion.}
Symbolic execution has a high computational complexity, and our algorithm took longer to analyse the original robot program than we were willing to wait.
The source of the high complexity lies in the function \emph{loop}, which includes several branching points due to the various conditionals.
We had to set the iteration limit to 2 for the analysis to terminate within 20 seconds on a ten year old laptop.
This may seem very low, but note that it was enough: the merged diagram would not change with more iterations.

The high complexity occurs because firstly the four conditionals can be entered independently, and secondly the sensors are read in each condition, %causing the execution engine to miss that the conditionals are mutually exclusive.
making the conditionals not mutually exclusive.
This results in 16 possible executions of the function.
Refactoring the program either by reading the sensors once at the start of \seca{loop()} or by nesting the conditionals, %so they are mutually exclusive,
reduces the number of possible executions to 4, making the analysis terminate in 2.4 seconds with iteration limit 2.
An improved version of the robot program together with its skyline diagrams can be found on the project website \citep{SecaHomepage}.


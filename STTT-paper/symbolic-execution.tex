\section{Resource-Aware Symbolic Execution}
\label{sec:symbolicExecution}

\topicSentence{In practice we are not interested in the skylines of \emph{one} run through the
program, but rather in \emph{all possible} skylines.}  This can give developers an idea of the power consumption
of their program also in cases that do not occur during testing.  To achieve this, we use symbolic
execution.
We formally do so in the form of a program semantics which tracks path constraints and power skylines for
each execution path through the program.
The result is a set of skylines for each function together with their path constraints.

\topicSentence{Under symbolic execution, expressions no longer evaluate to values, but to \emph{symbolic values}.}
Symbolic values \textrm{SVal} are syntax trees whose leaves are constants or \emph{symbolic inputs} $\id{any}_i$, special variables that stand for an arbitrary integer.
\textrm{SVal} is given by the grammar:
\begin{IEEEeqnarray*}{c}
\id{sv} ::= \seca{true} \mid \seca{false} \mid i \mid \id{any}_i \mid \id{sv}\mathbin{\id{op}}\id{sv} \mid \id{un}\ \id{sv}
\end{IEEEeqnarray*}

Correspondingly in symbolic execution, variables are mapped to symbolic values.
Let $\id{SPState}$ denote the type $\id{Var} \to \id{SVal}$ of mappings from variable names to symbolic values.
The functions $\id{lookup}$ and $\id{assign}$ are defined in the same way as in \cref{def:scopingRules}.

\medskip
\topicSentence{In the symbolic semantics it is undesirable for component calls to perform I/O, because
exploring all execution paths causes component calls to be executed multiple times.}
Moreover, we want to explore paths that rely on distinct input from components like sensors.
The symbolic semantics therefore uses component models where component calls return symbolic values with constraints.

\begin{definition}
A \emph{symbolic component model} $\tuple{S, L, \delta, o}$ consists of a set of states $S$, a finite set of
labels $L$, a transition function $\delta : L \times S \to \textrm{SVal} \times \textrm{SVal} \times S$, and
a power draw function $o : S \to \N$.
As opposed to the concrete models in \cref{sec:semantics}, $\delta$ cannot perform I/O.
\end{definition}

Note that $S$, $L$ and $o$ are the same as in concrete component models.
The only difference is the transition function $\delta$.
The first returned $\textrm{SVal}$ of $\delta$ is typically a constant or symbolic input, and the second is a constraint on that input.
For example, where the concrete model of \seca{TERM.readInt()} asks for input and returns the user's answer, the symbolic model returns a fresh symbolic input $\id{any}_j$, with the constraint \seca{true}, as this input can be any integer.
A temperature sensor in a cold room might return a fresh $\id{any}_j$, together with a constraint $13 \leq \id{any}_j \wedge \id{any}_j \leq 17$.
A symbolic LED returns constant $0$, with the constraint \seca{true}.

\subsection{The Resource-Aware Symbolic Execution Semantics}

The resource-aware symbolic execution semantics is an extension of the standard semantics, in two directions.
The first extension is that the resource-aware symbolic execution semantics constructs \emph{skylines}, assembling them step-by-step as execution proceeds, as sketched in \cref{sec:skylines}.
The rules keep track of the current skyline, and each rule adds one or more segments.
Every function call starts a new current skyline for that function, and every return statement adds the current skyline to a register of the function's skylines.

The second extension is to support symbolic execution.
This is done through non-deterministic rules for the step semantics.
In particular, the rules \textbf{[rss-if]} and \textbf{[rss-while]} for conditionals and loops respectively are not right-unique, so the same left-hand side may be related to multiple right-hand sides.
%This non-determinism is propagated through the other rules.
This is the essence of symbolic execution, where one program can have multiple executions.
The set of successor states of a statement $s$ can be recovered by finding all right-hand sides that $s$ relates to.
This process can be repeated to recover the tree of possible execution paths of a program.

The full process yields a skyline for each path through the program and each function executed during that pass,
together with the path constraint that leads to the skyline.

Like the standard semantics, the resource-aware symbolic semantics is defined by three relations:
\emph{resource-aware symbolic step} ($\esstep$), defined in \cref{fig:secaEnergyAwareSymbolicSemantics},
\emph{resource-aware symbolic evaluation} ($\esevaluate$), defined in \cref{fig:secaEnergyAwareSymbolicExprSemantics},
and \emph{resource-aware symbolic execution} ($\esexecute$), defined in \cref{fig:secaEnergyAwareSymbolicExecuteSemantics}.

\begin{figure*}

\begin{prooftree}
\AxiomC{$
  \id{e},\id{pst},\id{cst},\varphi,\id{sky} \mdoubleplus [F(l)],\id{skies}
  \esevaluate
  \id{sv},\id{pst}',\id{cst}',\varphi',\id{sky}',\id{skies}'
$}
\LeftLabel{\rulename{[rss-assign]}}
\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
  \id{x}_l = \id{e},\id{pst},\id{cst},\many{\id{pc}},\varphi,\id{sky},\id{skies}
\\ \esstep
  \id{assign}(x, \id{sv}, \id{pst}'),\id{cst}',\many{\id{pc}},\varphi',\id{sky}',\id{skies}'
\end{IEEEeqnarraybox*}
}
\end{prooftree}

\begin{prooftree}
\AxiomC{$
  \id{e},\id{pst},\id{cst},\varphi,\id{sky} \mdoubleplus [F(l)],\id{skies}
  \esevaluate
  \id{sv},\id{pst}',\id{cst}',\varphi',\id{sky}',\id{skies}'
$}
\LeftLabel{\rulename{[rss-if]}}
\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
  \seca{if}_l(\id{e})\ \{\ \many{s_1}\ \}\ \seca{else}\ \{\ \many{s_2}\ \},\id{pst},\id{cst},\many{\id{pc}},\varphi,\id{sky},\id{skies}
\\ \esstep
  \id{pst}',\id{cst}',(\many{s_1} \mdoubleplus \many{\id{pc}}),\varphi' \wedge \id{sv},\id{sky}',\id{skies}'
\\ \esstep
  \id{pst}',\id{cst}',(\many{s_2} \mdoubleplus \many{\id{pc}}),\varphi' \wedge \neg \id{sv},\id{sky}',\id{skies}'
\end{IEEEeqnarraybox*}
}
\end{prooftree}

\begin{prooftree}
\AxiomC{$
  \id{e},\id{pst},\id{cst},\varphi,\id{startSky},\id{skies}
  \esevaluate
  \id{sv},\id{pst}',\id{cst}',\varphi',\id{sky}',\id{skies}'
$}
\LeftLabel{\rulename{[rss-while]}}
\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
  \seca{while}_l(\id{e})\ \{\ \many{s}\ \}_m\ i,\id{pst},\id{cst},\many{\id{pc}},\varphi,\id{sky},\id{skies}
\\ \esstep
  \id{pst}',\id{cst}',\many{\id{pc}}',\varphi' \wedge \id{sv},\id{sky}',\id{skies}'
\\ \esstep
  \id{pst}',\id{cst}',\many{\id{pc}},\varphi' \wedge \neg \id{sv},\id{sky}',\id{skies}'
\\ \text{where}
\\ \many{\id{pc}}' = \many{s} \mdoubleplus [\seca{while}(\id{e})\ \{\ \many{s}\ \}\ (i+1)] \mdoubleplus \many{\id{pc}}
\\ \id{startSky} =
      \begin{cases}
      \id{sky} \mdoubleplus [F(l)]       & \textrm{if } i = 0 \\
      \id{sky} \mdoubleplus [F(m), J(l)] & \textrm{if } i > 0
      \end{cases}
\end{IEEEeqnarraybox*}
}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e,\id{pst},\id{cst},\varphi,\id{sky} \mdoubleplus F(l),\id{skies} \esevaluate \id{sv},\id{pst}',\id{cst}',\varphi',\id{sky}',\id{skies}'$}
\LeftLabel{\rulename{[rss-return]}}
\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
  \seca{return}_l\ \id{e},\id{pst},\id{cst},\many{\id{pc}},\varphi,\id{sky},\id{skies}
\\ \esstep
  \id{assign}(\id{\#return},\id{sv},\id{pst}'),\id{cst}',[],\varphi',\id{sky}',\id{skies}'
\end{IEEEeqnarraybox*}
}
\end{prooftree}


\begin{prooftree}
\AxiomC{$
  \id{e},\id{pst},\id{cst},\varphi,\id{sky},\id{skies}
  \esevaluate
  \id{sv},\id{pst}',\id{cst}',\varphi',\id{sky}',\id{skies}'
$}
\LeftLabel{\rulename{[rss-expr]}}
\UnaryInfC{$
  \id{e},\id{pst},\id{cst},\many{\id{pc}},\varphi,\id{sky},\id{skies}
  \esstep
  \id{pst}',\id{cst}',\many{\id{pc}},\varphi',\id{sky}',\id{skies}'
$}
\end{prooftree}

\caption{Resource-aware symbolic execution semantics ($\esstep$) for statements}
\label{fig:secaEnergyAwareSymbolicSemantics}
\end{figure*}

\begin{figure*}

\begin{prooftree}
\AxiomC{}
\LeftLabel{\rulename{[rse-const]}}
\UnaryInfC{$\id{v},\id{pst},\id{cst},\varphi,\id{sky},\id{skies} \esevaluate \id{v},\id{pst},\id{cst},\varphi,\id{sky},\id{skies}$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$\id{lookup}(x,\id{pst}) = \id{sv}$}
\LeftLabel{\rulename{[rse-var]}}
\UnaryInfC{$x,\id{pst},\id{cst},\varphi,\id{sky},\id{skies} \esevaluate \id{sv},\id{pst},\id{cst},\varphi,\id{sky},\id{skies}$}
\end{prooftree}

\begin{prooftree}
\AxiomC{
\begin{IEEEeqnarraybox*}{rCl}
  \id{e}_1,\id{pst},\id{cst},\varphi,\id{sky},\id{skies}
  & \esevaluate &
  \id{sv}_1,\id{pst}',\id{cst}',\varphi',\id{sky}',\id{skies}' \\
%
  \id{e}_2,\id{pst}',\id{cst}',\varphi',\id{sky}',\id{skies}'
  & \esevaluate &
  \id{sv}_2,\id{pst}'',\id{cst}'',\varphi'',\id{sky}'',\id{skies}''
\end{IEEEeqnarraybox*}
}
\LeftLabel{\rulename{[rse-add]}}
\UnaryInfC{$
  \id{e}_1 + \id{e}_2,\id{pst},\id{cst},\varphi,\id{sky},\id{skies}
  \esevaluate
  \id{sv}_1 + \id{sv}_2,\id{pst}'',\id{cst}'',\varphi'',\id{sky}'',\id{skies}''$}
\end{prooftree}


\begin{prooftree}
\AxiomC{
  \begin{IEEEeqnarraybox*}{c}
%
    \many{\id{e}},\id{pst},\id{cst},\varphi,\id{sky},\id{skies}
    \mathrel{\seqesevaluate}
    \many{\id{sv}},\tuple{\id{lst}',\id{gst}'},\id{cst}',\varphi',\id{sky}',\id{skies}' \\
%
    \tuple{\id{fst},\id{gst}'},\id{cst}',\varphi',\many{s},[S(m,d')],\id{skies}'
    \esexecute
    \tuple{\id{fst}',\id{gst}''},\id{cst}'',\varphi'',\id{fsky},\id{skies}'' \\
%
    \id{lookup}(\id{\#return},\id{fst}') = \id{sv}
%
  \end{IEEEeqnarraybox*}
}
\LeftLabel{\rulename{[rse-func-call]}}
\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
  f( \many{\id{e}} )_l,\id{pst},\id{cst},\varphi,\id{sky},\id{skies}
  \esevaluate
  \id{sv},\tuple{\id{lst}',\id{gst}''},\id{cst}'',\varphi'',\id{sky}'',\id{skies}'''
\\ \text{where}
\\ f(\many{x}) \{_m\ \many{s}\ \}_n \text{ is a defined function}
\\ \id{fst} = [\many{x} \mapsto \many{sv}] \text{ is the initial local state for $f$}
\\ d' = \id{curDraw}(cst') \text{ is the power draw after evaluating the arguments}
\\ d'' = \id{curDraw}(cst'') \text{ is the power draw after executing $f$}
\\ \id{sky}'' = \id{sky}' \mdoubleplus [F(l), E(d'')]
\\ \id{skies}''' = \id{skies}''[f \mapsto \id{skies}''(f) \cup \set{\id{fsky} \mdoubleplus [F(n)]}]
\end{IEEEeqnarraybox*}
}
\end{prooftree}


\begin{prooftree}
\AxiomC{$\delta_c(f,s_c) = \tuple{\id{sv},\varphi',s_c'}$}
\AxiomC{$\id{cst}' = \id{cst}[c \mapsto s_c']$}
\AxiomC{$d = \id{curDraw}(\id{cst}')$}
\LeftLabel{\rulename{[rse-comp-call]}}
\TrinaryInfC{
\begin{IEEEeqnarraybox*}{l}
  c.f()_l,\id{pst},\id{cst},\varphi,\id{sky},\id{skies}
  \esevaluate
  \id{sv},\id{pst},\id{cst}',\varphi \wedge \varphi',\id{sky}',\id{skies}
\\ \text{where } \id{sky}' = \id{sky} \mdoubleplus [F(l), E(d)]
\end{IEEEeqnarraybox*}
}
\end{prooftree}

\caption{Resource-aware symbolic evaluation of expressions ($\esevaluate$)}
\label{fig:secaEnergyAwareSymbolicExprSemantics}
\end{figure*}

\begin{figure*}

\begin{center}
\begin{minipage}{0.45\textwidth}
\begin{prooftree}
\AxiomC{}
\UnaryInfC{$\id{pst},\id{cst},[],\varphi,\id{sky},\id{skies} \esexecute \id{pst},\id{cst},\varphi,\id{sky},\id{skies}
$}
\end{prooftree}
\end{minipage}%
\begin{minipage}{0.45\textwidth}
\begin{prooftree}
\AxiomC{
  \begin{IEEEeqnarraybox*}{c}
%
    s,\id{pst},\id{cst},\many{\id{pc}},\varphi,\id{sky},\id{skies}
    \esstep
    \id{pst}',\id{cst}',\many{\id{pc}}',\varphi',\id{sky}',\id{skies}' \\
%
    \id{pst}',\id{cst}',\many{\id{pc}}',\varphi',\id{sky}',\id{skies}'
    \esexecute
    \id{pst}'',\id{cst}'',\varphi'',\id{sky}'',\id{skies}''
%
  \end{IEEEeqnarraybox*}
}
\UnaryInfC{$
  \id{pst},\id{cst},(s:\many{\id{pc}}),\varphi,\id{sky},\id{skies}
  \esexecute
  \id{pst}'',\id{cst}'',\varphi'',\id{sky}'',\id{skies}''
  $}
\end{prooftree}
\end{minipage}
\end{center}

\caption{Big-step resource-aware symbolic execution semantics ($\esexecute$) for statement lists}
\label{fig:secaEnergyAwareSymbolicExecuteSemantics}
\end{figure*}


\medskip\noindent\textbf{Resource-aware symbolic execution.}
We explain the semantics for statement lists first (\cref{fig:secaEnergyAwareSymbolicExecuteSemantics}),
since it serves well to explain how the non-determinism is propagated.
This relation has the following form:
\begin{IEEEeqnarray*}{Cl}
           & \id{SPState}^2,\id{CState},\many{\id{Stmt}},\id{SVal},\id{Skyline},\id{Skylines}\\
\esexecute & \id{SPState}^2,\id{CState},\id{SVal},\id{Skyline},\id{Skylines}
\end{IEEEeqnarray*}
Here, $\id{SPState}^2$ are the local and global program states respectively,
$\id{CState}$ is the component state,
$\many{\id{Stmt}}$ is the list of statements to be executed,
$\id{SVal}$ the path constraint, $\id{Skyline}$ is the thus-far assembled skyline of the function we are currently executing,
and $\id{Skylines}$ is a finite mapping from function names to lists of skylines.
CState is as defined before, except now it refers to states of symbolic component models.
The semantics executes all statements in the list, and returns the updated program state, component state, path constraint, current skyline and skyline mapping in the end.
This semantics is given in \cref{fig:secaEnergyAwareSymbolicExecuteSemantics}.

Note that \cref{fig:secaEnergyAwareSymbolicExecuteSemantics} looks very similar to
\cref{fig:secaStandardExecute}: the only apparent difference is that the step relation also updates
the skyline information, which the execute relation passes on.
However, another important difference is that the step relation is not right-unique, and consequently the
execute relation is not right-unique either.
In an implementation where we wish to identify \emph{all} outcomes for a given list of statements, we
might think of the step relation as defining a set
$\mathit{Step}(s,\id{pst},\id{cst},\many{\id{pc}},\varphi,\mathit{sky},\mathit{skies})$
containing all tuples $(\id{pst}',\id{cst}',\many{\id{pc}}',\varphi',\mathit{sky}',\mathit{skies}')$
that may be reached by executing $s$.
We might then correspondingly define the result of $\esexecute$ recursively as the function:
$\mathit{Exec}(\id{pst},\id{cst},\many{\id{pc}},\varphi,\mathit{sky},\mathit{skies}) =$
\[
\left\{
\begin{array}{ll}
\{ (\id{pst},\id{cst},\varphi,\mathit{sky},\mathit{skies}) \} & \text{if}\ \many{\id{pc}} = [] \\
\bigcup_{\text{tuple}\in X} \mathit{Exec}(\text{tuple}) & \text{if}\ \many{\id{pc}} = (s : \many{\id{pc}}')\ \text{and}\ X = \\
  & \mathit{Step}(s,\id{pst},\id{cst},\many{\id{pc}}',\varphi,\mathit{sky},\mathit{skies})
\end{array}
\right.
\]
In this way, a non-deterministic semantics still defines a deterministic evaluation result.

\medskip\noindent\textbf{The resource-aware symbolic step semantics.}
resource-aware symbolic stepping ($\esstep$) executes single statements while tracking their power skylines.
It is a relation with judgements of the following form.
\begin{IEEEeqnarray*}{Cll}
        & \id{Stmt}, & \id{SPState}^2,\id{CState},\many{\id{Stmt}},\id{SVal},\id{Skyline},\id{Skylines}\\
\esstep &            & \id{SPState}^2,\id{CState},\many{\id{Stmt}},\id{SVal},\id{Skyline},\id{Skylines}
\end{IEEEeqnarray*}
Here, the first $\id{Stmt}$ is the statement to be executed, $\id{SPState}^2$ are the local and global
program states respectively, $\id{CState}$ is the component state, $\many{\id{Stmt}}$ is the program
counter, $\id{SVal}$ the path constraint, $\id{Skyline}$ is the thus-far assembled skyline of the function
currently being executed, and $\id{Skylines}$ is a finite mapping from function names to lists of skylines.

We explain \cref{fig:secaEnergyAwareSymbolicSemantics} below.
The rule names are prefixed with \emph{rss}, which stands for \emph{resource-aware symbolic step}.

\begin{description}
\item[\rulename{[rss-assign]}]
This first rule handles a statement $\id{x} = \id{e}$, where the variable $\id{x}$ is on line $l$; this is indicated in the semantics as $\id{x}_l = \id{e}$.
The rule extends the current skyline with a forward segment $F(l)$ to the line $l$ of the variable name $x$ of the assignment statement.
This extended skyline is passed to the evaluation of $\id{e}$.
The order is important, because $\id{e}$ itself can have side effects and extend the skyline.
The pattern of first extending the skyline and then evaluating sub-expressions is used in the other rules as well.
The result of evaluating $\id{e}$ is then used to update the SPState $\id{pst'}$according to the scoping rules.

Note that, because expression evaluation is not right-unique, whenever an expression can evaluate to multiple values, this rule generates just as many successor states.
The same holds for all rules that evaluate expressions.

\item[\rulename{[rss-if]}]
Conditionals extend the skyline with a forward segment $F(l)$ to the location $l$ of the \seca{if} keyword, before evaluating the condition $e$ to a symbolic value $\id{sv}$.
There are \emph{two} ways to continue, indicated by two separate $\esstep$ arrows in the rule's conclusion.
Formally there are two rules with identical premises, which we have presented in one rule here.
The first way is to continue as if the condition was true with the then-branch $\many{s_1}$ and the path constraint $\varphi' \wedge \id{sv}$.
The second way is to continue as if the condition was false with the else-branch $\many{s_2}$ and the path constraint $\varphi' \wedge \neg \id{sv}$.

These two choices are at the heart of symbolic execution.
Note that the rule matches any conditional, and the two options produce successor states for the then-branch and the else-branch respectively.

\item[\rulename{[rss-while]}]
The most complex rule in the stepping semantics is the one for while-loops.
Here, $\seca{while}_l(\id{e})\ \{\ \many{s}\ \}_m$ indicates that the $\seca{while}$ statement is on line $l$, and the closing brace of the loop on line $m$.
This rule makes use of the loop counter $i$, because the generated skyline must be different if the loop is encountered for the first time or the second or later times.
If $i = 0$, the loop is executed for the first time, and the skyline $\id{sky}$ comes from outside the loop.
In this case $\id{sky}$ is extended with a forward segment $F(l)$ of the $\seca{while}$ keyword.
If $i > 0$, then the loop has been executed before, and $\id{sky}$ comes from inside the loop body.
In this case, $\id{sky}$ is extended with a forwards line $F(m)$ to the end of the loop, and then jumps $J(l)$ back to the beginning of the loop.
The extended skyline $\id{startSky}$ is then used for evaluation of the condition $\id{e}$.

Similar to the rule for conditionals, there actually are two rules with identical premises, shown as one.
The rule matches any \seca{while} statement, but produces two successor states:
one for looping with positive condition, and one for skipping the loop with negated condition.

\item[\rulename{[rss-return]}]
This rule extends $\id{sky}$ to the location $l$ of the \seca{return} keyword, and then evaluates the return value $\id{sv}$.
It assigns this value to the return register of the current function's SPState $\id{pst}'$, and clears the program counter.

\item[\rulename{[rss-expr]}]
This rule lifts evaluation of expressions into the semantics for statements.
It just evaluates the expression $\id{e}$ for its side effects, and discards the result value.
\end{description}

It is worth noting that the first choice for \rulename{[ss-while]} always appends the while-loop itself to the program counter,
thereby creating infinite execution paths for any program that contains a while-loop.
This is not a problem for the mathematical definition of the semantics.
However, an \emph{implementation} must take precautions to avoid running indefinitely.
Our implementation takes two measures for this.
First, it employs an SMT solver to prune infeasible paths, which terminates loops if the condition can no longer be satisfied.
Second, it limits the number of times a loop is executed, and forcibly cuts execution when the limit is reached.
This guarantees that execution terminates.

\medskip\noindent\textbf{The resource-aware symbolic evaluate semantics.}
The semantics for resource-aware symbolic expression evaluation ($\esevaluate$) is defined by the rules in \cref{fig:secaEnergyAwareSymbolicExprSemantics}.
The rule names are prefixed with \emph{rse}, which stands for \emph{resource-aware symbolic evaluate}.
The semantics has judgements of the following form.
\begin{IEEEeqnarray*}{Cll}
            & \id{Expr}, & \id{SPState}^2,\id{CState},\id{SVal},\id{Skyline},\id{Skylines}\\
\esevaluate & \id{SVal},  & \id{SPState}^2,\id{CState},\id{SVal},\id{Skyline},\id{Skylines}
\end{IEEEeqnarray*}
The semantics evaluates an expression $\id{Expr}$ to a symbolic value $\id{SVal}$ in the context of a program state $\id{SPState}^2$, a component state $\id{CState}$, a path constraint $\id{SVal}$, the current skyline $\id{Skyline}$, and the global skyline register $\id{Skylines}$.

\begin{description}
\item[\rulename{[rse-const]}]
Constants evaluate to themselves.
Every standard value is also a symbolic value.

\item[\rulename{[rse-var]}]
Variables are evaluated by looking up their value in the program state.

\item[\rulename{[rse-add]}]
Adding two symbolic expressions $\id{e}_1$ and $\id{e}_2$ requires evaluating them to symbolic values $\id{sv}_1$ and $\id{sv}_2$ first.
As with all symbolic rules, both premises can match multiple times, in which case the conclusion is applied accordingly.
The result is not a single number, but the compound symbolic value $\id{sv}_1 + \id{sv}_2$.

\item[\rulename{[rse-func-call]}]
A function call first evaluates all actual parameters $\many{e}$ with the state-chaining extension ($\seqesevaluate$) of ($\esevaluate$).
This results in symbolic values $\many{sv}$ and a skyline $\id{sky}'$.
There may be multiple distinct outcomes, each resulting in a function call with different arguments.
The rule then executes the function body using a new skyline initialized with a start segment $[S(m,d')]$
where $m$ is the line where $f$ starts, and $d'$ is the total power draw of all components after evaluation of the actual parameters.
When the call to $f$ returns with skyline $\id{fsky}$, we know that $\id{fsky}$ ends at the return statement that caused $f$ to return.
$\id{fsky}$ is extended with a forwards segment $F(n)$ to the end of $f$.
This skyline is added to the set of skylines of all calls to $f$, $\id{skies}'''$.
Finally, the skyline of the calling function is extended with an edge $[F(l), E(d'')]$ at the call site $l$, where $d''$ is the power draw after executing $f$.

\item[\rulename{[rse-comp-call]}]
Component calls are evaluated by calling the transition function of the component on its current state $s_c$.
This results in a symbolic value $\id{sv}$, a boolean predicate $\varphi'$ on the value, and a new component state $s_c'$.
The component call evaluates to the return value and the path constraint $\varphi \wedge \varphi'$, together with the skyline extended with an edge $[F(l),E(d)]$ where $l$ is the location of the component call, and $d$ is the power draw after the component call.
\end{description}

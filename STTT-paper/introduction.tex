\section{Introduction}
\label{sec:introduction}

% context
Software that controls hardware is found in many places; for example washing machines, smartphones or self-driving cars.
The software running in such devices is in charge of orchestrating the hardware components, like sensors, motors, displays, or radios.
Formal analysis of such devices is hard, because hardware and software have to be analysed together.
To optimize energy consumption of such devices, especially when they are battery-powered, it is useful for programmers to have a prediction of the power-behaviour of all the components when running their program, ideally integrated into their development environment.
Simulation or actual measurement of running devices can give some insight, but only for one specific scenario and hardware configuration.

% goal
We develop a static analysis based on symbolic execution that can visualize the power behaviour of all possible executions of a program at once, which can be incorporated into an integrated development environment.
This allows programmers to quickly assess the power impact of a change, already during development.
Our method is parametrized with hardware models, so that programmers can swap components and explore different hardware configurations.

% scope
In the domain of embedded systems and control software, the power draw of the processor is often negligible.
We therefore limit our scope to the power draw of the hardware components controlled by the software.
If desired, programmers can bypass this restriction by modelling the processor as a hardware component and switching between its power states explicitly with corresponding component calls.
Modelling processors as hardware components is possible because they often have approximately constant power draw in their different power states. For example the popular ATmega328P, used on the Arduino UNO, has an amperage of 0.2 mA in Active Mode, 0.75 $\mu$A in Power-save Mode, and 0.1 $\mu$A in Power-down Mode (operating at 1 Mhz/1.8V) \citep{ATmegaDataSheet}.

\begin{figure}
\subfloat[]{%
  \begin{minipage}{0.49\columnwidth}%
  \lstinputlisting[language=SECA,numbers=left,xleftmargin=2.0em,lastline=15]{programs/bug.eca}%
  \label{lst:bug}%
  \end{minipage}%
}%
\subfloat[]{%
  \begin{minipage}{0.49\columnwidth}%
  \includegraphics[width=1\textwidth]{bug-main.pdf}%
  \label{fig:bug}%
  \end{minipage}%
}%
\caption{\protect\subref{lst:bug} Sensor input controls a lamp
\protect\subref{fig:bug} All possible runs of the program.
  One run does not end at power draw zero, which indicates a bug.}
\label{firstexample}
\end{figure}

% approach
We illustrate our approach with an example.
The program in \cref{lst:bug} reads a sensor, and switches on either LED1 or LED2.
It has a bug in line 9, where \seca{<} is used instead of \seca{<=}.
There are three possible executions, one of which does not end with a power draw of zero, as there exists a sensor value where LED1 is not switched off.
The skyline diagram in \cref{fig:bug} shows a merged view of the three executions.
The horizontal axis shows power draw, and the vertical axis line numbers.
Skylines that would be drawn on top of each other are shifted by a small offset.
In this view, programmers can see that some component still consumes power at the end of the function, and can start investigating the issue.

% contribution
This paper brings together two distinct lines of earlier work: the energy consumption analysis by \citet{Gastel2017} and the skyline diagrams by \citet{KlinikJP2017};
it is the extended version of an earlier conference paper \cite{KlinikGKE2020}.
Our contribution is threefold.
First, we introduce a symbolic execution engine that tracks hardware state and works with the programming language SECA (Symbolic Energy Consumption Analysis).
Second, we define visualization rules for the results of the symbolic execution as diagrams of power draw over points in the source code.
Third, we define an algorithm to reduce the number of plotted graphs, hiding redundant information, to make the diagrams more concise.
Our proof-of-concept implementation is available online \citep{SecaSourceCodeRepository}.

The present paper is an extension of the conference paper \cite{KlinikGKE2020} in various ways.
\begin{itemize}
\item It includes the complete semantic rules (\cref{sec:semantics,sec:symbolicExecution,sec:seca-complete-semantics}), which required a thorough revision of the whole paper,
\item describes our integration of the symbolic execution engine in Visual Studio Code (\cref{sec:ide}),
\item presents first steps towards a visual debugger, based on the Visual Studio integration (\cref{sec:ide}),
\item includes a section about applications, extensions to other resource types, and validity (\cref{sec:applications}),
\item extends discussion about related work with in-depth comparison with two more works (\cref{sec:relatedWork})
\item and revives numerous explanatory paragraphs across all sections that fell victim to the conference paper's page limit.
\end{itemize}

A note on terminology.
Technically speaking, the ``E'' in SECA is a misnomer, because it does not analyse energy consumption, but power draw. We keep the name to emphasize the connection with ECA, on which SECA builds.

%Disclaimer
\topicSentence{Our goal is to explore the idea of drawing power skylines over source lines; not (yet) to make an industry-ready tool.}
To focus on this goal, our method considers a C-like language that lacks the complexity of C itself.
Likewise, the well-known problem of exponential state-space explosion, and reduction techniques that
may be used to manage this problem, are not in our scope.
%Thus, we do not currently consider programs with thousands of lines.

\clearpage
\section{The Standard Resource-aware Semantics}
\label{sec:seca-complete-semantics}

SECA comes with three semantics: the \emph{standard semantics}, the \emph{standard resource-aware semantics} and the \emph{symbolic resource-aware semantics}.
Each of them is defined by three evaluation relations: \emph{execute} for whole programs, \emph{step} for statements, and \emph{evaluate} for expressions.
\Cref{fig:three-seca-semantics} shows an overview.

\begin{table}
\center
\begin{tabular}{lccc}
\toprule
          & programs       & statements  & expressions \\
semantics & \emph{execute} & \emph{step} & \emph{evaluate} \\
\midrule
standard                & $\execute$   & $\step$   & $\evaluate$ \\
standard resource-aware & $\eexecute$  & $\estep$  & $\eevaluate$ \\
symbolic res.-aware & $\esexecute$ & $\esstep$ & $\esevaluate$ \\
\bottomrule
\end{tabular}
\caption{The three semantics of SECA, each consisting of three evaluation relations}
\label{fig:three-seca-semantics}
\end{table}

We have discussed the standard semantics and the resource-aware symbolic semantics in the text.
For completeness, we provide the standard resource-aware semantics here, which has already been explained by example in \cref{sec:skylines}.

\topicSentence{The resource-aware semantics of SECA is an extension of the standard semantics.}
It has similar reduction rules, but additionally keeps track of the power consumption of each function, represented by a skyline over program points.
For each relation of the standard semantics ($\step$, $\execute$, $\evaluate$) there exists an resource-aware extension ($\estep$, $\eexecute$, $\eevaluate$).

\topicSentence{The resource-aware semantics for statements ($\estep$) assembles skylines as execution proceeds.}
The rules keep track of the current skyline, and every rule adds one or more segments.
Every function call starts a new current skyline for that function, and every return statement adds the current skyline to a register of the function's skylines.
Skylines are lists of skyline segments, as defined in \cref{sec:semantics}.
The resource-aware semantics for statements ($\estep$) has judgements of the following form.
\begin{IEEEeqnarray*}{Cll}
          & \id{Stmt}, & \id{PState}^2,\id{CState},\id{World},\many{\id{Stmt}},\id{Skyline},\id{Skylines}
\\ \estep &            & \id{PState}^2,\id{CState},\id{World},\many{\id{Stmt}},\id{Skyline},\id{Skylines}
\end{IEEEeqnarray*}
$\id{PState}^2,\id{CState},\id{World},\many{\id{Stmt}}$ are as in ($\step$).
$\id{Skyline}$ is the so-far assembled skyline of the currently executed function.
$\id{Skylines}$ is a finite mapping from function names to lists of skylines.
It registers for every function the skylines that result from all calls to that function.
The rules of $\estep$ are shown in \cref{fig:SECAEnergyAwareSemantics}.
For statements $s$, we write $s_l$ to indicate that $s$ occurs on line $l$ in the source file.
For example, $\seca{x}_l = e$ is an assignment statement to variable \seca{x} where the variable name occurs on line $l$.

The rules of ($\estep$) have the same reduction behaviour for programs as the rules for ($\step$).
In addition, they construct skylines that represent the power behaviour of the program execution.
%Skylines are extended in reverse order for notational convenience.
The rule names are prefixed with \emph{rs}, which stands for \emph{resource-aware step}.

\begin{description}
\item[\rulename{[rs-assign]}]
This rule first extends the skyline with a forwards line $F(l)$ to the line of the variable name \seca{x} of the assignment statement.
This extended skyline is passed to the evaluation of $e$.
The order is important, because $e$ itself can have side effects and extend the skyline.
The pattern of first extending the skyline and then evaluating subexpressions is used in the other rules as well.

\item[\rulename{[rs-if-true]}, \rulename{[rs-if-false]}]
Conditionals extend the skyline to the line of the \seca{if} keyword, before evaluating the condition $e$.
The rule \rulename{[rs-if-false]} does the same, and is not shown.

\item[\rulename{[rs-while]}]
The rule for while-loops is the most complicated one.
We have collapsed the cases when the condition is \seca{true} and \seca{false} in one rule, because the power behaviour is the same.
The only difference is the resulting continuation, which is identical to \rulename{[s-while-true]} and \rulename{[s-while-false]}.

This rule makes use of the loop counter $i$, because the generated skyline must be different if the loop is encountered for the first time.
If $i = 0$ the loop is executed for the first time, and the skyline $\id{sky}$ comes from outside the loop.
In this case $\id{sky}$ is extended with a forwards line to the location $l$ of the \seca{while} keyword.
If $i > 0$ the loop has been executed before, and $\id{sky}$ comes from inside the loop body.
In this case $\id{sky}$ is extended with a forwards line $F(m)$ to the end of the loop, and then jumps $J(l)$ back to the beginning of the loop.
The extended skyline $\id{startSky}$ is then used for evaluation of the condition $e$.

\item[\rulename{[rs-return]}]
The rule for return statements first extends the current skyline $\id{sky}$ with a forwards line $F(l)$ to the location of the \seca{return} keyword, and then evaluates $e$.

\item[\rulename{[rs-expr]}]
This rule lifts evaluation of expressions into the semantics for statements.
It just evaluates the expression for its side effect, and discards the returned value.
\end{description}


\begin{figure*}

\begin{prooftree}
\AxiomC{$
  e,\id{pst},\id{cst},w,\id{sky} \mdoubleplus [F(l)],\id{skies}
  \eevaluate
  v,\id{pst}',\id{cst}',w',\id{sky}',\id{skies}'
$}
\LeftLabel{\rulename{[rs-assign]}}
\UnaryInfC{$
  \seca{x}_l \seca{= e},\id{pst},\id{cst},w,\many{\id{pc}},\id{sky},\id{skies}
  \estep
  \id{assign}(\seca{x}, v, \id{pst}'),
  \id{cst}',
  w',
  \id{sky}',
  \id{skies}'
$}
\end{prooftree}



\begin{prooftree}
\AxiomC{
  \begin{IEEEeqnarraybox*}{c}
    e,
    \id{pst},
    \id{cst},
    w,
    \id{sky} \mdoubleplus [F(l)],
    \id{skies}
  \eevaluate
    \seca{true},
    \id{pst}',
    \id{cst}',
    w',
    \id{sky}',
    \id{skies}'
  \end{IEEEeqnarraybox*}
}
\LeftLabel{\rulename{[rs-if-true]}}
\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
  \seca{if}_l(e)\ \{\ \many{s_1}\ \}\ \seca{else}\ \{\ \many{s_2}\ \},
    \id{pst},
    \id{cst},
    w,
    \many{\id{pc}},
    \id{sky},
    \id{skies}
\\ \estep
    \id{pst}',
    \id{cst}',
    w',
    (\many{s_1} \mdoubleplus \many{\id{pc}}),
    \id{sky}',
    \id{skies}'
\end{IEEEeqnarraybox*}
}
\end{prooftree}




\begin{prooftree}

\AxiomC{
\begin{IEEEeqnarraybox*}{c}
  e,\id{pst},\id{cst},w,\id{startSky},\id{skies}
  \eevaluate
  v,\id{pst}',\id{cst}',w',\id{sky}',\id{skies}'
\end{IEEEeqnarraybox*}
}

\LeftLabel{\rulename{[rs-while]}}

\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
   s
  ,\id{pst}
  ,\id{cst}
  ,w
  ,\many{\id{pc}}
  ,\id{sky}
  ,\id{skies}
\estep
   \id{cst}'
  ,\id{pst}'
  ,w'
  ,\many{pc}'
  ,\id{sky}'
  ,\id{skies}'
\\ \text{where}
\\ s = \seca{while}_l(e)\ \{\ \many{s_1}\ \}_m\ i
\\ \id{startSky} =
      \begin{cases}
      \id{sky} \mdoubleplus [F(l)] & \textrm{if } i = 0 \\
      \id{sky} \mdoubleplus [F(m), J(l)] & \textrm{if } i > 0
      \end{cases}
\\ \many{pc}' =
      \begin{cases}
      \many{s_1} \mdoubleplus [\seca{while}_l(e)\ \{\ \many{s_1}\ \}_m\ (i+1)] \mdoubleplus \many{pc} & \textrm{ if } v = \seca{true}\\
      \many{pc} & \textrm{ if } v = \seca{false}
      \end{cases}
\end{IEEEeqnarraybox*}
}
\end{prooftree}


\begin{prooftree}
\AxiomC{$
  e,\id{pst},\id{cst},w,\id{sky} \mdoubleplus [F(l)],\id{skies}
  \eevaluate
  v,\id{pst}',\id{cst}',w',\id{sky}',\id{skies}'
$}

\LeftLabel{\rulename{[rs-return]}}

\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
  \seca{return}_l\ e,\id{pst},\id{cst},w,\many{\id{pc}},\id{sky},\id{skies}
\\ \estep
  \id{assign}(\id{\#return},v,\id{pst}'),\id{cst}',w',[],\id{sky}',\id{skies}'
\end{IEEEeqnarraybox*}
}
\end{prooftree}


\begin{prooftree}
\AxiomC{$
  e,\id{pst},\id{cst},w,\id{sky},\id{skies}
  \eevaluate
  v,\id{pst}',\id{cst}',w',\id{sky}',\id{skies}'
$}
\LeftLabel{\rulename{[rs-expr]}}
\UnaryInfC{$
  e,\id{pst},\id{cst},w,\many{\id{pc}},\id{sky},\id{skies}
  \estep
  \id{pst}',\id{cst}',w',\many{\id{pc}},\id{sky}',\id{skies}'
$}
\end{prooftree}



\caption{Resource-aware small-step semantics ($\estep$) for statements}
\label{fig:SECAEnergyAwareSemantics}
\end{figure*}


The resource-aware semantics for evaluating expressions ($\eevaluate$) is a big-step semantics, based on ($\evaluate$).
It has judgements of the form
\begin{IEEEeqnarray*}{Cll}
              & \id{Expr}, & \id{PState}^2,\id{CState},\id{World},\id{Skyline},\id{Skylines}
\\ \eevaluate & \id{Val},  & \id{PState}^2,\id{CState},\id{World},\id{Skyline},\id{Skylines}
\end{IEEEeqnarray*}
where $\id{PState}^2,\id{CState},\id{World}$ are as for $(\evaluate)$.
The semantics is defined by the rules in \cref{fig:SECAEnergyAwareExprSemantics}.
Most of the rules are the same as in $(\evaluate)$, and just additionally pass on the skylines unchanged.
We omit those rules.
The only interesting rules are \rulename{[re-func-call]} and \rulename{[re-comp-call]}.
The rule names are prefixed with \emph{re}, which stands for \emph{resource-aware evaluate}.

\begin{description}
\item[\rulename{[re-func-call]}]
A function call first evaluates all actual parameters $\many{e}$ with the state-chaining extension ($\seqeevaluate$) of ($\eevaluate$).
This results in a skyline $\id{sky}'$.
The rule then executes the function body using a new skyline initialized with a start segment $[S(m,d')]$ where $m$ is the line where $f$ starts, and $d'$ is the total power draw of all components after evaluation of the actual parameters.
When the call to $f$ returns with skyline $\id{fsky}$, we know that $\id{fsky}$ ends at the return statement that caused $f$ to return.
$\id{fsky}$ is extended with a forwards segment $F(n)$ to the end of $f$.
This skyline is then added to the register of the skylines of all calls to $f$.

The calling function finally extends its own skyline with an edge $[F(l), E(d'')]$ where $l$ is the location of the call site, and $d''$ the total power draw after the call to $f$.

\item[\rulename{[re-comp-call]}]
Component calls \seca{c.f()} generate edges in the following way.
First, the $f$-transition of the current state of $c$ is calculated, resulting in a return value $r$, a new state $s_c'$, and a new world $w'$.
Second, The CState is updated so that $c$ is now in state $s$.
Third, The power draw $d$ is calculated in the new CState $\id{cst}'$.
Finally the skyline is extended with an edge $[F(l), E(d)]$ at the location $l$ of the component call, to power draw $d$.
\end{description}

\begin{figure*}

\begin{prooftree}
\AxiomC{
  \begin{IEEEeqnarraybox*}{c}
%
  \many{e},\id{pst},\id{cst},w,\id{sky},\id{skies}
  \mathrel{\seqeevaluate}
  \many{v},\tuple{\id{lst}',\id{gst}'},\id{cst}',w',\id{sky}',\id{skies}' \\
%
  \tuple{\id{fst},\id{gst}'},\id{cst}',w',\many{s},[S(m,d')],\id{skies}'
  \eexecute
  \tuple{\id{fst}',\id{gst}''},\id{cst}'',w'',[],\id{fsky},\id{skies}'' \\
%
  \id{lookup}(\id{\#return},\id{fst}') = r
%
  \end{IEEEeqnarraybox*}
}

\LeftLabel{\rulename{[re-func-call]}}

\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
   f( \many{e} )_l,\id{pst},\id{cst},w,\id{sky},\id{skies}
   \eevaluate
   r,\tuple{\id{lst}',\id{gst}''},\id{cst}'',w'',\id{sky}'',\id{skies}'''
\\ \text{where}
\\ f(\many{x}) \{_m\ \many{s}\ \}_n \text{ is a defined function}
\\ \id{fst} = [\many{x} \mapsto \many{v}] \textrm{ is the initial local state for $f$}
\\ d' = \id{curDraw}(cst') \textrm{ is the power draw after evaluating the arguments}
\\ d'' = \id{curDraw}(cst'') \textrm{ is the power draw after executing $f$}
\\ \id{sky}'' =\id{sky}' \mdoubleplus [F(l), E(d'')]
\\ \id{skies}''' = \id{skies}''[f \mapsto \id{skies}''(f) \cup \set{\id{fsky} \mdoubleplus [F(n)]}]
\end{IEEEeqnarraybox*}
}
\end{prooftree}


\begin{prooftree}
\AxiomC{$\delta_c(f,s_c,w) = \tuple{r,s_c',w'}$}
\AxiomC{$\id{cst}' = \id{cst}[c \mapsto s_c']$}
\AxiomC{$d = \id{curDraw}(\id{cst}')$}
\LeftLabel{\rulename{[re-comp-call]}}
\TrinaryInfC{$
  c.f()_l,\id{pst},\id{cst},w,\id{sky},\id{skies}
  \eevaluate
  r,\id{pst},\id{cst}',w',\id{sky} \mdoubleplus [F(l), E(d)],\id{skies}
$}
\end{prooftree}

\caption{Excerpt of the resource-aware expression evaluation semantics ($\eevaluate$)}
\label{fig:SECAEnergyAwareExprSemantics}
\end{figure*}

The resource-aware execution semantics ($\eexecute$) is defined similarly to ($\execute$), and not shown here.

%  \subsection{The Symbolic Execution Semantics}
%  
%  In the text, we have discussed the energy-aware symbolic execution semantics, which combines symbolic execution with
%  energy consumption analysis.  Here, we will present an intermediate step: the symbolic execution variation of the standard
%  semantics.  The energy-aware symbolic execution semantics can then be seen as a combination of the current appendix and
%  \cref{app:energy-aware-appendix}.
%  
%  As before, we use three relations: \emph{symbolic step} ($\sstep$) for symbolic execution of statements,
%  \emph{symbolic evaluation} ($\sevaluate$) for expressions, and \emph{symbolic execution} ($\sexecute$) for
%  whole programs.
%  We are using double arrows to hint that one program can have multiple symbolic executions.
%  
%  Symbolic execution of statements ($\sstep$) is a relation with judgements of the form:
%  \begin{IEEEeqnarray*}{c}
%  \id{Stmt},\id{SPState}^2,\id{SCState},\many{\id{Stmt}},\id{SVal} \sstep \id{SPState}^2,\id{SCState},\many{\id{Stmt}},\id{SVal}
%  \end{IEEEeqnarray*}
%  where the first Stmt is the statement to be executed, $\id{SPState}^2$ are program states for variables holding symbolic values, $\id{SCState}$ is a symbolic component state, $\many{\id{Stmt}}$ the program counter, and $\id{SVal}$ the path constraint.
%  
%  The relation is defined by the rules in \cref{fig:secaSymbolicStep}.
%  The rule names are prefixed with \emph{ss}, which stands for \emph{symbolic step}.
%  This relation is not right-unique, which means that the same left-hand side can be related to more than one right-hand side.
%  This is the essence of symbolic execution, where one program can have multiple executions.
%  The set of successor states of a statement $s$ can be recovered by finding all right-hand sides that $s$ relates to.
%  This process can be repeated to recover the tree of possible execution paths of a program.
%  
%  \begin{figure*}
%  
%  \begin{prooftree}
%  \AxiomC{$
%    \id{se},\id{pst},\id{cst},\varphi
%    \sevaluate
%    \id{sv},\id{pst}',\id{cst}',\varphi'
%  $}
%  \LeftLabel{\rulename{[ss-assign]}}
%  \UnaryInfC{$
%    \seca{x} = \id{se},\id{pst},\id{cst},\many{\id{pc}},\varphi
%    \sstep
%    \id{assign}(x, \id{sv}, \id{pst}'),\id{cst}',\many{\id{pc}},\varphi'
%  $}
%  \end{prooftree}
%  
%  
%  \begin{prooftree}
%  \AxiomC{$
%    \id{se},\id{pst},\id{cst},\varphi
%    \sevaluate
%    \id{sv},\id{pst}',\id{cst}',\varphi'
%  $}
%  \LeftLabel{\rulename{[ss-if-true]}}
%  \UnaryInfC{$
%    \seca{if}(\id{se})\ \{\ \many{s_1}\ \}\ \seca{else}\ \{\ \many{s_2}\ \},\id{pst},\id{cst},\many{\id{pc}},\varphi
%    \sstep
%    \id{pst}',\id{cst}',(\many{s_1} \mdoubleplus \many{\id{pc}}),\varphi' \wedge \id{sv}
%  $}
%  \end{prooftree}
%  
%  
%  \begin{prooftree}
%  \AxiomC{$
%    \id{se},\id{pst},\id{cst},\varphi
%    \sevaluate
%    \id{sv},\id{pst}',\id{cst}',\varphi'
%  $}
%  \LeftLabel{\rulename{[ss-if-false]}}
%  \UnaryInfC{$
%    \seca{if}(\id{se})\ \{\ \many{s_1}\ \}\ \seca{else}\ \{\ \many{s_2}\ \},\id{pst},\id{cst},\many{\id{pc}},\varphi
%    \sstep
%    \id{pst}',\id{cst}',(\many{s_2} \mdoubleplus \many{\id{pc}}),\varphi' \wedge \neg \id{sv}
%  $}
%  \end{prooftree}
%  
%  
%  \begin{prooftree}
%  \AxiomC{$
%    \id{se},\id{pst},\id{cst},\varphi
%    \sevaluate
%    \id{sv},\id{pst}',\id{cst}',\varphi'
%  $}
%  \LeftLabel{\rulename{[ss-while-true]}}
%  \UnaryInfC{
%  \begin{IEEEeqnarraybox*}{l}
%     \seca{while}(\id{se})\ \{\ \many{s}\ \}\ i,\id{pst},\id{cst},\many{\id{pc}},\varphi
%     \sstep
%     \id{pst}',\id{cst}',\many{\id{pc}}',\varphi' \wedge \id{sv}
%  \\ \text{where}
%  \\ \many{\id{pc}}' = \many{s} \mdoubleplus [\seca{while}(\id{se})\ \{\ \many{s}\ \}\ (i+1)] \mdoubleplus \many{\id{pc}}
%  \end{IEEEeqnarraybox*}
%  }
%  \end{prooftree}
%  
%  
%  \begin{prooftree}
%  \AxiomC{$
%    \id{se},\id{pst},\id{cst},\varphi
%    \sevaluate
%    \id{sv},\id{pst}',\id{cst}',\varphi'
%  $}
%  \LeftLabel{\rulename{[ss-while-false]}}
%  \UnaryInfC{$
%    \seca{while}(\id{se})\ \{\ \many{s}\ \}\ i,\id{pst},\id{cst},\many{\id{pc}},\varphi
%    \sstep
%    \id{pst}',\id{cst}',\many{\id{pc}},\varphi' \wedge \neg \id{sv}
%  $}
%  \end{prooftree}
%  
%  
%  \begin{prooftree}
%  \AxiomC{$se,\id{pst},\id{cst},\varphi \sevaluate \id{sv},\id{pst}',\id{cst}',\varphi'$}
%  \LeftLabel{\rulename{[ss-return]}}
%  \UnaryInfC{$
%    \seca{return}\ \id{se},\id{pst},\id{cst},\many{\id{pc}},\varphi
%    \sstep
%    \id{assign}(\id{\#return},\id{sv},\id{pst}'),\id{cst}',[],\varphi'
%  $}
%  \end{prooftree}
%  
%  
%  \begin{prooftree}
%  \AxiomC{$
%    \id{se},\id{pst},\id{cst},\varphi
%    \sevaluate
%    \id{sv},\id{pst}',\id{cst}',\varphi'
%  $}
%  \LeftLabel{\rulename{[ss-expr]}}
%  \UnaryInfC{$
%    \id{se},\id{pst},\id{cst},\many{\id{pc}},\varphi
%    \sstep
%    \id{pst}',\id{cst}',\many{\id{pc}},\varphi'
%  $}
%  \end{prooftree}
%  
%  
%  \caption{Symbolic small-step semantics ($\sstep$) for statements}
%  \label{fig:secaSymbolicStep}
%  \end{figure*}
%  
%  
%  \begin{description}
%  \item{\rulename{[ss-assign]}}
%  Evaluates the symbolic expression $\id{se}$ to a symbolic value $\id{sv}$, and updates the SPState according to the scoping rules.
%  Note that, because expression evaluation is also not right-unique, whenever an expression can evaluate to multiple values, this rule generates just as many successor states.
%  The same holds for all rules that evaluate expressions.
%  
%  \item{\rulename{[ss-if-true]}, \rulename{[ss-if-false]}}
%  These rules are the heart of symbolic execution.
%  Both rules always match any conditional, and they produce successor states for the then-branch and the else-branch respectively.
%  The path constraint in \rulename{[ss-if-true]} is extended with the condition for the then-branch, and in \rulename{[ss-if-false]} with the negated condition for the else-branch.
%  
%  \item{\rulename{[ss-while-true]}, \rulename{[ss-while-false]}}
%  Similar to the rules for conditionals, both rules always match, producing successor states for skipping the loop with negated condition, and looping with positive condition.
%  Note that \rulename{[ss-while-true]} always appends the while-loop itself to the program counter, thereby creating infinite execution paths for any program that contains a while-loop.
%  This is not a problem for the mathematical definition of the semantics.
%  An implementation however must take precautions to not run indefinitely.
%  Our implementation takes two measures for this.
%  First, it employs an SMT solver to prune infeasible paths, which automatically terminates loops if the condition can no longer be satisfied.
%  Second, it limits the number of times a loop is executed, and forcibly cuts execution when the limit is reached.
%  This guarantees execution to terminate, by whichever measure comes into action first.
%  
%  
%  
%  \item{\rulename{[ss-return]}}
%  This rule works as in the standard semantics, emptying the program counter $\many{\id{pc}}$, and updating the $\id{\#return}$ register in the program state.
%  
%  \item{\rulename{[ss-expr]}}
%  This rule, as in the standard semantics, evaluates the expression for its side effects and discards the result value.
%  \end{description}
%  
%  
%  The symbolic execution semantics for evaluating expressions ($\sevaluate$) is defined by the rules in \cref{fig:secaSymbolicEvaluate}.
%  The semantics has judgements of the following form.
%  \begin{IEEEeqnarray*}{c}
%  \id{SExpr},\id{SPState}^2,\id{SCState},\id{SVal} \sevaluate \id{SVal},\id{SPState}^2,\id{SCState},\id{SVal}
%  \end{IEEEeqnarray*}
%  The semantics relates symbolic expressions $\id{SExpr}$ to symbolic values $\id{SVal}$ in the context of a symbolic program state $\id{SPState}^2$, a symbolic component state $\id{SCState}$, and a path constraint $\id{SVal}$.
%  The rule names are prefixed with \emph{se}, which stands for \emph{symbolic evaluate}.
%  
%  
%  \begin{description}
%  \item[\rulename{[se-const]}]
%  Constants evaluate to themselves, just like in the standard semantics.
%  
%  \item[\rulename{[se-var]}]
%  Variables are evaluated by looking up their value in the program state.
%  
%  \item[\rulename{[se-add]}]
%  Adding two symbolic expressions $\id{se}_1$ and $\id{se}_2$ requires evaluating them to symbolic values $\id{sv}_1$ and $\id{sv}_2$ first.
%  As with all symbolic rules, both premises can match multiple times, in which case the conclusion is applied accordingly.
%  The result is not a single number, but the compound symbolic value $\id{sv}_1 + \id{sv}_2$.
%  
%  \item[\rulename{[se-func-call]}]
%  Symbolic function calls work just like standard function calls, except that all premises can match multiple times, each resulting in a function call with different arguments.
%  Evaluating the arguments $\many{\id{se}}$ requires sequential evaluation of each argument while threading the state through all these evaluations.
%  This operation is denoted by $\seqsevaluate$.
%  
%  \item[\rulename{[se-comp-call]}]
%  Symbolic component calls yield a symbolic value $\id{sv}$, possibly containing symbolic inputs, together with a boolean predicate $\varphi'$ over those symbolic inputs.
%  The component call evaluates to $\id{sv}$, and the path constraint $\varphi$ is extended with $\varphi'$.
%  \end{description}
%  
%  \begin{figure*}
%  
%  \begin{prooftree}
%  \AxiomC{}
%  \LeftLabel{\rulename{[se-const]}}
%  \UnaryInfC{$\id{sv},\id{pst},\id{cst},\varphi \sevaluate \id{sv},\id{pst},\id{cst},\varphi$}
%  \end{prooftree}
%  
%  \begin{prooftree}
%  \AxiomC{$\id{lookup}(x,\id{pst}) = \id{sv}$}
%  \LeftLabel{\rulename{[se-var]}}
%  \UnaryInfC{$x,\id{pst},\id{cst},\varphi \sevaluate \id{sv},\id{pst},\id{cst},\varphi$}
%  \end{prooftree}
%  
%  \begin{prooftree}
%  \AxiomC{$
%    \id{se}_1,\id{pst},\id{cst},\varphi
%    \sevaluate
%    \id{sv}_1,\id{pst}',\id{cst}',\varphi'
%  $}
%  \AxiomC{$
%    \id{se}_2,\id{pst}',\id{cst}',\varphi'
%    \sevaluate
%    \id{sv}_2,\id{pst}'',\id{cst}'',\varphi''
%  $}
%  \LeftLabel{\rulename{[se-add]}}
%  \BinaryInfC{$
%    \id{se}_1 + \id{se}_2,\id{pst},\id{cst},\varphi
%    \sevaluate
%    \id{sv}_1 + \id{sv}_2,\id{pst}'',\id{cst}'',\varphi''$}
%  \end{prooftree}
%  
%  
%  \begin{prooftree}
%  \AxiomC{
%    \begin{IEEEeqnarraybox*}{c}
%  %
%      \many{\id{se}},\id{pst},\id{cst},\varphi
%      \mathrel{\seqsevaluate}
%      \many{\id{sv}},\tuple{\id{lst}',\id{gst}'},\id{cst}',\varphi' \\
%  %
%      \tuple{\id{fst},\id{gst}'},\id{cst}',\varphi',\many{s}
%      \sexecute
%      \tuple{\id{fst}',\id{gst}''},\id{cst}'',\varphi'',[] \\
%  %
%      \id{lookup}(\id{\#return},\id{fst}') = r
%  %
%    \end{IEEEeqnarraybox*}
%  }
%  \LeftLabel{\rulename{[se-func-call]}}
%  \UnaryInfC{
%  \begin{IEEEeqnarraybox*}{l}
%     f( \many{\id{se}} ),\id{pst},\id{cst},\varphi
%     \sevaluate
%     r,\tuple{\id{lst}',\id{gst}''},\id{cst}'',\varphi''
%  \\ \text{where}
%  \\ f(\many{x}) \{\ \many{s}\ \} \text{ is a defined function}
%  \\ \id{fst} = [\many{x} \mapsto \many{v}] \text{ is the initial local state for $f$}
%  \end{IEEEeqnarraybox*}
%  }
%  \end{prooftree}
%  
%  \begin{IEEEeqnarraybox*}{c}
%  \end{IEEEeqnarraybox*}
%  
%  
%  \begin{prooftree}
%  \AxiomC{$\delta_c(f,s_c) = \tuple{\id{sv},\varphi',s_c'}$}
%  \AxiomC{$\id{cst}' = \id{cst}[c \mapsto s_c']$}
%  \LeftLabel{\rulename{[se-comp-call]}}
%  \BinaryInfC{$
%    c.f(),\id{pst},\id{cst},\varphi
%    \sevaluate
%    \id{sv},\id{pst},\id{cst}',\varphi \wedge \varphi'
%  $}
%  \end{prooftree}
%  
%  
%  \caption{Excerpt of the symbolic expression evaluation semantics ($\sevaluate$)}
%  \label{fig:secaSymbolicEvaluate}
%  \end{figure*}
%  
%  
%  The symbolic execution semantics ($\sexecute$) for statement lists is defined similarly to ($\execute$), and is not shown here.



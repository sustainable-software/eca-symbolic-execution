\section{Skylines}\label{sec:skylines}

\topicSentence{Power consumption of program executions is captured using \emph{skylines}.}
A skyline traces the total power consumption of all active hardware components in every step of a function.
Skylines are visualized in \emph{skyline diagrams}.
Formally, a skyline is defined as follows:

\begin{definition}
A \emph{skyline segment} is one of the following:
\begin{itemize}
\item a \emph{forwards line} $F(l)$ with $l \in \N$;
\item a \emph{backwards jump} $J(l)$ with $l \in \N$;
\item an \emph{edge} $E(p)$ with $p \in \N$.
\end{itemize}
In addition, a \emph{start point} has the form $S(l,p)$ with $l,p \in \N$.

A \emph{skyline} is a list $sl[0],\dots,sl[n]$ where $sl[0]$ is a start point and $sl[1],\dots,sl[n]$ are skyline segments.
\end{definition}

\newcommand{\skylineconstruction}{
\begin{figure*}
  \centering
  \begin{minipage}{0.21\textwidth}
  \lstinputlisting[language=SECA,numbers=left,xleftmargin=2em]{programs/example.eca}
  \end{minipage}
\subfloat[]{%
  \begin{minipage}{0.170\textwidth}
  \includegraphics[width=1\textwidth]{example-main-1.pdf}%
  \label{fig:exampleMain1}%
  \end{minipage}
}%
\subfloat[]{%
  \begin{minipage}{0.170\textwidth}
  \includegraphics[width=1\textwidth]{example-main-2.pdf}%
  \label{fig:exampleMain2}%
  \end{minipage}
}%
\subfloat[]{%
  \begin{minipage}{0.170\textwidth}
  \includegraphics[width=1\textwidth]{example-main-3.pdf}%
  \label{fig:exampleMain3}%
  \end{minipage}
}%
\subfloat[]{%
  \begin{minipage}{0.170\textwidth}
  \includegraphics[width=1\textwidth]{example-main-4.pdf}%
  \label{fig:exampleMain4}%
  \end{minipage}
}%
\caption{Construction of a skyline
\protect\subref{fig:exampleMain1} After switching on LED1
\protect\subref{fig:exampleMain2} After the first loop iteration
\protect\subref{fig:exampleMain3} After the second iteration
\protect\subref{fig:exampleMain4} The final skyline
}
\label{fig:exampleMain}
\end{figure*}}
\invisible{\skylineconstruction}

Intuitively, a skyline is a construct that traces a single program run and records at every step
the current program location (as a line number $l$) and the current power draw (as a unitless number $p$).
Each segment is interpreted relative to its predecessor: the skyline starts at a fixed line and power draw,
and then a forwards line or backward jump changes the line but keeps the power draw the same, while an edge
changes the power draw but keeps the line number the same.

\begin{example}
Consider the following skyline:
\begin{center}
$S(1,0),F(2),E(10),F(3),F(4),E(0),F(5)$
\end{center}
This skyline starts at line 1 with power draw 0.  It then moves to line 2, where the power draw is increased to
10.  The program continues to lines 3 and 4% (still with power draw 10)
, where the power draw drops back to 0.
Finally, the program continues to line 5 where it ends with power draw 0.
This might correspond for example to the program below%in \cref{fig:tiny-example}
, where the LED has a power draw of 10
in its \emph{on} state, and 0 in its \emph{off} state.
We visualize skylines in diagrams with line numbers on the vertical axis and power draw on the horizontal axis.
A corresponding skyline diagram is shown on the right% in \cref{fig:tiny-example}
.

%\begin{figure}[h]
  \centering
  \begin{minipage}{0.40\columnwidth}%
  \lstinputlisting[language=SECA,numbers=left,xleftmargin=2em]{programs/tiny-example.eca}
  \end{minipage}%
  \begin{minipage}{0.24\columnwidth}%
  \includegraphics[width=1\textwidth]{tiny-example-main.pdf}
  \end{minipage}%
%\caption{A tiny example program and its skyline.
%\CK{There really is no reason why the 0 should be included.}}
%\label{fig:tiny-example}%
%\end{figure}

\end{example}

This is not a perfect presentation: since there is no axis for \emph{time}, or \emph{moment in the program
execution}, a skyline diagram is typically ambiguous.  Segments may end up on top of each other when
the same lines of code are executed more than once, for example in loops.  However, this is not problematic for our
purposes: the diagram still gives a good overview of the expected total power draw for each line in the code.

One execution of a program results in multiple sets of skylines: one skyline for every pass through each function.
To obtain this, we use a function to calculate the total power draw in a given component state.

\begin{definition}
The function $\id{curDraw} : \id{CState} \to \N$ calculates the total current power draw by summing up the
current power draw of all components in the given CState.
\end{definition}

This function is used in a variation of the standard semantics of
\cref{fig:secaStandardStep}--\ref{fig:secaStandardExecute}, to generate skylines as the program is
executed.  In practice, we execute the program as expected, and construct its skyline on the way.
Whenever execution moves to the next statement, a forwards line $F(l)$ is added to the skyline, or a
backwards jump $J(l)$ when execution moves from the end of a while-loop to the start.  For function and component
calls, $\id{curDraw}$ is queried at the end of the call and an edge $E(p)$ added with the new draw $p$.

The full formal semantics is provided in \cref{sec:seca-complete-semantics}.
Since it is not really used in this paper other than as a stepping stone towards the resource-aware
symbolic execution semantics (\cref{sec:symbolicExecution}), we will not discuss it here.
Instead, we will explain by example how skylines are constructed.

\visible{\skylineconstruction}

\medskip\noindent\textbf{Generating skylines for programs with loops.}
\topicSentence{The program in \cref{fig:exampleMain} switches \seca{LED1} on five times in a loop, and then switches it off.}
The skyline fragments generated during execution are shown in \cref{fig:skylineFragmentsExample}.

\begin{figure}[h]
\center
\begin{IEEEeqnarraybox*}{l's}
   [S(1,0), F(2), F(3), F(4), E(10), & \subref{fig:exampleMain1}
\\ F(5), F(6), J(3),                 & \subref{fig:exampleMain2}
\\ F(4), E(10), F(5), F(6), J(3),    & \subref{fig:exampleMain3}
\\ F(4), E(10), F(5), F(6), J(3),
\\ F(4), E(10), F(5), F(6), J(3),
\\ F(4), E(10), F(5), F(6), J(3),
\\ F(7), E(0), F(8), F(9)]           & \subref{fig:exampleMain4}
\end{IEEEeqnarraybox*}%
\caption{Skyline fragments for \cref{fig:exampleMain}}
\label{fig:skylineFragmentsExample}
\end{figure}%

Execution of this program starts in a CState where LED1 is in state \emph{off}.
\Cref{fig:exampleMain1} shows the skyline just after executing line 4, which switches the LED on.
Lines 2 and 3 do not change the power draw, which yields two forward
segments from line 1 to 2 and from line 2 to 3.
LED1 is switched on in line 4, which extends the skyline with a forward segment from
3 to 4, followed by a rising edge to power draw 10.

\Cref{fig:exampleMain2} shows the skyline after one loop iteration, when the loop condition in line 3 has been executed a second time.
Line 5 has caused a forward segment from 4 to 5.
Execution of line 3 has caused a forward segment from the last
statement of the loop
in line 5 to the closing brace of the loop in line 6, followed by a backwards jump to line 3, which is not visible in the diagram.

\Cref{fig:exampleMain3} shows the skyline after two iterations.
The second iteration starts at line 3 with power draw 10, and gives three forward segments 3 to 4, 4 to 5, and 5 to 6.
In Figure \ref{fig:skylineFragmentsExample} we can see that there is also an edge E(10) at line 4,
but since this does not change the power draw (as the LED is already on), this is not visible in the diagram.
These segments overlap with the segments of the previous iteration, and are drawn on top of each other.
All subsequent iterations also generate identical segments.

After five loop iterations, the program exits the loop, with the skyline shown in \cref{fig:exampleMain4}.
Switching off the LED generates a falling edge to power draw 0 in line 7.
The return statement generates two forward segments, one for itself from 7 to 8 and one for exiting the function from 8 to 9.

\medskip\noindent\textbf{Generating skylines for multiple functions.}
To see what happens with function calls, consider the program in \cref{fig:funccalls}.
We assume that LED1 has power draw 10 in state \emph{on}, and LED2 has power draw 7.  Both have power draw 0 in state \emph{off}.
Program execution starts in a state with both LEDs turned off.
At the start of line 3, execution has generated the skyline (a) $S(1,0),F(2),E(10),F(3)$: LED1 has just been turned on, and the program is about to call the function.
At this point execution enters the $\id{helper}$ function.  We store the incomplete skyline (a) and start a new one.
Since LED1 is already turned on, this yields the skyline (b) $S(6,10),F(7),E(17),F(8)$.
Execution now returns from the function, and finishes skyline (a) by first adding $E(17)$ (since the function call increased the power draw to 17), and then $F(4),E(7),F(5)$.
The diagrams of the resulting skylines are given in \cref{fig:funccallsskylines}.

\begin{figure}[b]
\centering
\begin{minipage}{0.2\textwidth}
\lstset{language=SECA,numbers=left,xleftmargin=1em}
\begin{lstlisting}
  int main() {
    LED1.switchOn();
    helper();
    LED1.switchOff();
  }
\end{lstlisting}
\end{minipage}%
\begin{minipage}{0.2\textwidth}
\lstset{language=SECA,numbers=left,xleftmargin=1em,firstnumber=6}
\begin{lstlisting}
  void helper() {
    LED2.switchOn();
  }
\end{lstlisting}
\end{minipage}
\caption{A simple program with a function call}
\label{fig:funccalls}
\end{figure}

\begin{figure}[t]
  \centering
  \begin{minipage}{0.35\columnwidth}
  \includegraphics[width=1\textwidth]{simple-function-call-main-main.pdf}%
  \end{minipage}%
  \hspace{2em}%
  \begin{minipage}{0.35\columnwidth}
  \includegraphics[width=1\textwidth]{simple-function-call-helper-helper.pdf}%
  \end{minipage}
  \caption{Skylines for $\id{main}$ (left) and $\id{helper}$ (right) of \cref{fig:funccalls}.
  \CK{This could use an (a) and (b) label. Also, I find the size quite ugly!}
  }
  \label{fig:funccallsskylines}
\end{figure}

\begin{remark}
A limitation of our method is that using line numbers to identify program points requires the
source code to be formatted so that every skyline-producing program point is on its own line, to
avoid segments being drawn over each other. This concerns the left-hand side of assignments,
\seca{if} keywords, \seca{while} keywords, the closing brace of the body of while-loops,
\seca{return} keywords, and the closing parentheses of function- and component calls.
Expressions that contain function- or component calls as subexpressions should be split over multiple lines.
We have developed a prototype plugin for the Visual Studio Code IDE (see \cref{sec:ide}), which can be extended to automatically apply said formatting to aid the visualization.

\end{remark}

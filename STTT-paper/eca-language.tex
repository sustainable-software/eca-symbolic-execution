\section{The SECA Language}
\label{sec:ecaLanguage}

SECA (Symbolic Energy Consumption Analysis) is a small imperative programming language.
We designed SECA to look like a simple form of C, without features like raw memory access and pointer arithmetic.
Such features complicate the analysis and are not the focus of this paper.
Typically, C programs that control external devices adhere to strict coding styles limiting which language features can be used, and how they are used. An example of such a coding style is the MISRA C 2012 standard, which includes these limitations: disallowing aliasing (rule 19.2 and 21.12), disallowing recursion (rule 17.2), limiting for loops so they have predictable runtime (number of iterations can not be changed from within the loop body, rule 14.2), limit while loops so that the condition is not invariant (rule 14.3), and disallowing dynamic memory allocations (rule 21.3).
Therefore, we believe that with some engineering effort, the analysis can be extended to support the style of C programs common in embedded and safety-critical systems.
%
SECA is a variant of ECA \citep{GastelKE2015}, which is itself a variant of Nielson's \textbf{While} \citep{NielsonN1992}.
SECA programs can control external hardware through \emph{component calls}.
For example, the component call \seca{LED1.switchOn()} invokes the switchOn functionality of the component \seca{LED1}.
Component calls can perform I/O, and have return values but no arguments. See \cref{sec:applications} for a discussion about arguments to component calls.


\subsection{Syntax}

\topicSentence{A SECA program is a list of function and global variable declarations, including one function \seca{main}.}
The declarations follow the abstract syntax in \cref{fig:secaSyntax}.
Here, overlined symbols stand for lists of that symbol; for example $\many{s}$ is a list of statements.
Expressions are Boolean or integer constants, program variables, applications of binary or unary operators, function calls, and component calls.
Operators are the usual Boolean connectives, comparisons and arithmetic.
Function calls have a list of expressions, the parameters.
Component calls have the form \emph{name}.\emph{function}() and invoke the specified function of the named component.
Statements are conditionals, while-loops, assignments, returns, or expressions.
While-loops are annotated with a loop counter $i$, which the semantics uses to limit loop iterations, and to draw skylines differently in the first loop iteration.
This is further discussed in \cref{sec:semantics,sec:skylines}.
The loop counter is not part of the concrete syntax, and the programmer cannot access it; it is initialised with zero.
Assignments have a variable on the left-hand side and an expression on the right-hand side.
Return statements end the current function call, and yield the given expression as the function's return value.

\begin{figure}[h]
\centering
\begin{IEEEeqnarraybox*}{rCl}
e & ::= & \seca{true} \mid \seca{false} \mid i \mid x \mid e\ \id{op}\ e \mid \id{un}\ e \mid \id{id}( \many{e} ) \mid \id{id} . \id{id}()
%
\\ \id{op} & ::= & \seca{\&\&} \mid \seca{\|\|} \mid \seca{<=} \mid \seca{<} \mid + \mid - \mid \seca{*}
%
\\ \id{un} & ::= & - \mid \seca{!}
%
\\ s & ::= & \seca{if}(e)\,\{\,\many{s}\,\}\,\seca{else}\,\{\,\many{s}\,\} \mid \seca{while}(e)\,\{\,\many{s}\,\}\ i \mid x\ \seca{=}\ e \mid \seca{return}\ e \mid e
%
\\ \IEEEeqnarraymulticol{3}{l}{\id{funcDef} ::= \id{id}(\many{x}) \{\ \many{s}\ \}}
\\ \IEEEeqnarraymulticol{3}{l}{\id{globalVarDef} ::= x = e}
\end{IEEEeqnarraybox*}
\caption{Abstract syntax of SECA}
\label{fig:secaSyntax}
\end{figure}

\topicSentence{We make the following assumptions about SECA programs being well-formed.}
We provide no typing rules, but do require that programs are well-typed in the usual sense.
We assume that all code paths of a function end in a return statement of the correct type, no references to undefined variables occur, and programs run on devices with all occurring hardware components.
Void functions are allowed to end without return statements; in this case, the execution engine inserts an implicit return statement.

\subsection{Semantics}
\label{sec:semantics}

% Component models
\topicSentence{To define the formal semantics of the language, we will start with the semantics of component calls.}
To analyse the power consumption of programs, we need an estimation of how much power their hardware components require to operate.
Such an estimation is called a \emph{hardware component model}, or \emph{component model} for short.
Component models are labelled transition systems, not necessarily finite, where each state has a power draw.
Transitions are labelled with \emph{component functions} (e.g. \seca{switchOn}).
Component calls can affect, and read input from, the outside world, for instance by turning on a motor or reading a sensor.
To model this, we consider these calls as passing around a unique pointer to the real world.

\begin{definition}[Hardware Component Model]
\label{def:hardwareComponentModel}
A hardware component model consists of a set of states $S$, a finite set of labels $L$, a state transition function $\delta : L \times S \times \id{World} \to \Z \times S \times \id{World}$, and a power draw function $o : S \to \N$.
\end{definition}

A \emph{configuration} of a model is an element of $S$: the current state.
Every component has a start state.
Input-producing hardware like sensors or terminals use the return value $\Z$ to return the input.
Actuators like motors should return 0.
The power draw function $o$ specifies how much power the component consumes in each state.

\begin{figure}
\centering%
\begin{tikzpicture}
  [ ->
  , >=stealth'
  , shorten >=1pt
  , auto
  , node distance=2.1cm
  , semithick
  ]
  \tikzstyle{every state}=[draw]

  \node[state] (A)              {off/0};
  \node[state] (B) [right of=A] {on/10};

  \path (A) edge [bend left]  node[above=3pt] {switchOn} (B)
        (B) edge [bend left]  node[below=3pt] {switchOff} (A)
        (B) edge [loop right] node[above=7pt] {switchOn} (B)
        (A) edge [loop left]  node[above=7pt] {switchOff} (A)
        ;
\end{tikzpicture}
\caption{Hardware component model for an LED.
In state \emph{on} it has a power draw of 10, in state \emph{off} a power draw of 0.
The transitions correspond to component functions.
}
\label{fig:ledComponentModel}
\end{figure}
\begin{figure}
\centering%
\begin{tikzpicture}
  [ ->
  , >=stealth'
  , shorten >=1pt
  , auto
  , node distance=2.1cm
  , semithick
  ]
  \tikzstyle{every state}=[draw]

  \node[state] (base) {base/0};

  \path (base) edge [loop right] node[above=7pt] {read : 0--20} (base) ;
\end{tikzpicture}
\caption{Hardware component model for a sensor, with power draw 0.
The \emph{read} transition returns a value between 0 and 20.}
\label{fig:ledComponentModelSensor}
\end{figure}

An example of a component model is supplied in \cref{fig:ledComponentModel}.  Here,
LEDs have two states, \emph{on} and \emph{off}, and transitions \emph{switchOn} and \emph{switchOff} between them.
We have $o(\text{on}) = 10$ and $o(\text{off}) = 0$.
The transition $\delta(\text{switchOn},\text{off},w) = (0,\text{on},w')$, where the value
$0$ is returned because the component is an actuator.
A second example is given in \cref{fig:ledComponentModelSensor}.  Here, there is only one
state \emph{base}, and $\delta(\text{read},\text{base},w) = (i,\text{base},w')$ where $i$ can
be any integer between $0$ and $20$, and depends on the state of the world; for example, on the
temperature.

\topicSentence{SECA programs always run in contexts where a number of component models are present.}
Such contexts are called \emph{component states}, or CStates for short.
A CState is a partial mapping from names to configurations.
If the CState contains an LED, say under the name of \seca{LED1}, programs can contain the component calls \seca{LED1.switchOn()} and \seca{LED1.switchOff()}.

\begin{remark}
In our implementation \cite{SecaSourceCodeRepository}, we require the component models to be
implemented in Haskell.  The World parameter is modeled through Haskell's IO monad; this means that the
transition functions have a signature $L \times S \rightarrow \id{IO}(\Z,S)$.
\end{remark}

% Scoping rules
SECA has global and local variables, with the usual scoping rules,
as given by Definition \ref{def:scopingRules}.

\begin{definition}[Scoping rules]\label{def:scopingRules}
Let $\id{PState}$ denote the type of mappings from variable names to values (integer or
boolean in SECA).  Then we define $\id{lookup} : \id{Var} \times \id{PState}^2 \to \id{Val}$
and $\id{assign} : \id{Var} \times \id{Val} \times \id{PState}^2 \to \id{PState}^2$
as follows:
\[
  \id{lookup}(x,\id{locals},\id{globals}) =
  \left\{
  \begin{array}{ll}
    \id{locals}(x) & \text{if this is defined} \\
    \id{globals}(x) & \text{otherwise}
  \end{array}
  \right.
\]
\[
  \id{assign}(x,v,\id{loc},\id{glo}) =
  \left\{
  \begin{array}{ll}
    (\id{loc},\id{glo}[x\mapsto v]) & \text{if $\id{glo}(x)$ is} \\
    & \text{defined and} \\
    & \text{$\id{loc}(x)$ is not} \\
    (\id{loc}[x\mapsto v],\id{glo}) & \text{otherwise}
  \end{array}
  \right.
\]
\end{definition}

That is, when a variable is used in an expression, the program semantics first tries looking it
up locally, then globally if that fails.  We assume that all variables exist before they are used.
The same rules hold when a variable is assigned, except when the variable was not
previously known it is now declared as a local variable.

\medskip
Execution of SECA programs is exactly as one may intuitively expect, taking the above definitions
into account.  In order to formally clarify the language, we will provide the detailed formal
semantics of SECA below.
This semantics is defined by three relations:
\emph{step} ($\step$) for statements, defined in Figure \ref{fig:secaStandardStep},
\emph{evaluate} ($\evaluate$) for expressions, defined in Figure \ref{fig:secaStandardEvaluate}, and
\emph{execute} ($\execute$) for lists of statements, defined in Figure \ref{fig:secaStandardExecute}.

\newcommand{\stepfigure}{
\begin{figure*}

\begin{prooftree}
\AxiomC{$e,\id{pst},\id{cst},w \evaluate v,\id{pst}',\id{cst}',w'$}
\LeftLabel{\rulename{[s-assign]}}
\UnaryInfC{$
   \id{x} = \id{e}
  ,\id{pst}
  ,\id{cst}
  ,w
  ,\many{\id{pc}}
\step
   \id{assign}(x, v, \id{pst}')
  ,\id{cst}'
  ,w'
  ,\many{\id{pc}}
$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e,\id{pst},\id{cst},w \evaluate \seca{true},\id{pst}',\id{cst}',w'$}
\LeftLabel{\rulename{[s-if-true]}}
\UnaryInfC{$
   \seca{if}(e)\ \{\ \many{s_1}\ \}\ \seca{else}\ \{\ \many{s_2}\ \}
  ,\id{pst}
  ,\id{cst}
  ,w
  ,\many{\id{pc}}
\step
   \id{pst}'
  ,\id{cst}'
  ,w'
  ,\many{s_1} \mdoubleplus \many{\id{pc}}
$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e,\id{pst},\id{cst},w \evaluate \seca{false},\id{pst}',\id{cst}',w'$}
\LeftLabel{\rulename{[s-if-false]}}
\UnaryInfC{$
   \seca{if}(e)\ \{\ \many{s_1}\ \}\ \seca{else}\ \{\ \many{s_2}\ \}
  ,\id{pst}
  ,\id{cst}
  ,w
  ,\many{\id{pc}}
\step
   \id{pst}'
  ,\id{cst}'
  ,w'
  ,\many{s_2} \mdoubleplus \many{\id{pc}}
$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e,\id{pst},\id{cst},w \evaluate \seca{true},\id{pst}',\id{cst}',w'$}
\LeftLabel{\rulename{[s-while-true]}}
\UnaryInfC{\begin{IEEEeqnarraybox*}{l}
   \seca{while}(e)\ \{\ \many{s_1}\ \}\ i
  ,\id{pst}
  ,\id{cst}
  ,w
  ,\many{\id{pc}}
\step
   \id{pst}'
  ,\id{cst}'
  ,w'
  ,(\many{s_1} \mdoubleplus (\seca{while}(e)\ \{\ \many{s_1}\ \}\ (i+1) : \many{\id{pc}}))
\end{IEEEeqnarraybox*}
}
\end{prooftree}


\begin{prooftree}
\AxiomC{$e,\id{pst},\id{cst},w \evaluate \seca{false},\id{pst}',\id{cst}',w'$}
\LeftLabel{\rulename{[s-while-false]}}
\UnaryInfC{$
   \seca{while}(e)\ \{\ \many{s}\ \}\ i
  ,\id{pst}
  ,\id{cst}
  ,w
  ,\many{\id{pc}}
\step
   \id{pst}'
  ,\id{cst}'
  ,w'
  ,\many{\id{pc}}
$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e,\id{pst},\id{cst},w \evaluate v,\id{pst}',\id{cst}',w'$}
\LeftLabel{\rulename{[s-return]}}
\UnaryInfC{$
  \seca{return}\ e,\id{pst},\id{cst},w,\many{\id{pc}}
  \step
  \id{assign}(\id{\#return},v,\id{pst}'),\id{cst}',w',[]
$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e,\id{pst},\id{cst},w \evaluate v,\id{pst}',\id{cst}',w'$}
\LeftLabel{\rulename{[s-expr]}}
\UnaryInfC{$
  e,\id{pst},\id{cst},w,\many{\id{pc}}
  \step
  \id{pst}',\id{cst}',w',\many{\id{pc}}
$}
\end{prooftree}

\caption{Small-step standard semantics ($\step$) for statements}
\label{fig:secaStandardStep}
\end{figure*}
}
\newcommand{\evalfigure}{
\begin{figure*}

\begin{minipage}{0.3\textwidth}
\begin{prooftree}
\AxiomC{}
\LeftLabel{\rulename{[e-const]}}
\UnaryInfC{$v,\id{pst},\id{cst},w \evaluate v,\id{pst},\id{cst},w$}
\end{prooftree}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\begin{prooftree}
\AxiomC{$\id{lookup}(x,\id{pst}) = v$}
\LeftLabel{\rulename{[e-var]}}
\UnaryInfC{$x,\id{pst},\id{cst},w \evaluate v,\id{pst},\id{cst},w$}
\end{prooftree}
\end{minipage}
\begin{minipage}{0.36\textwidth}
\begin{prooftree}
\AxiomC{
\begin{IEEEeqnarraybox*}{c}
   e_1,\id{pst},\id{cst},w \evaluate i_1,\id{pst}',\id{cst}',w'
\\ e_2,\id{pst}',\id{cst}',w' \evaluate i_2,\id{pst}'',\id{cst}'',w''
\\ v = i_1 + i_2
\end{IEEEeqnarraybox*}
}
\LeftLabel{\rulename{[e-add]}}
\UnaryInfC{$e_1 + e_1,\id{pst},\id{cst},w \evaluate v,\id{pst}'',\id{cst}'',w''$}
\end{prooftree}
\end{minipage}

\begin{minipage}{0.49\textwidth}
\begin{prooftree}
\AxiomC{
\begin{IEEEeqnarraybox*}{c}
   e_1,\id{pst},\id{cst},w \evaluate i_1,\id{pst}',\id{cst}',w'
\\ e_2,\id{pst}',\id{cst}',w' \evaluate i_2,\id{pst}'',\id{cst}'',w''
\\ i_1 \leq i_2
\end{IEEEeqnarraybox*}
}
\LeftLabel{\rulename{[e-compare-true]}}
\UnaryInfC{$e_1 \seca{<=} e_1,\id{pst},\id{cst},w \evaluate \seca{true},\id{pst}'',\id{cst}'',w''$}
\end{prooftree}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\vspace{6pt}
\begin{prooftree}

\AxiomC{
  \begin{IEEEeqnarraybox*}{c}
%
    \many{e},\id{pst},\id{cst},w
    \mathrel{\seqevaluate}
    \many{v},\tuple{\id{lst}',\id{gst}'},\id{cst}',w'
%
 \\ \tuple{\id{fst},\id{gst}'},\id{cst}',w',\many{s}
    \execute
    \tuple{\id{fst}',\id{gst}''},\id{cst}'',w'' %,[]
%
 \\ \id{lookup}(\id{\#return},\id{fst}') = r
%
  \end{IEEEeqnarraybox*}
}

\LeftLabel{\rulename{[e-func-call]}}
\UnaryInfC{
\begin{IEEEeqnarraybox*}{l}
   f(\many{e}),\id{pst},\id{cst},w \evaluate r,\tuple{\id{lst}',\id{gst}''},\id{cst}'',w''
\\ \text{where}
\\ f(\many{x}) \{\ \many{s}\, \} \textrm{ is a defined function}
\\ \id{fst} = [\many{x} \mapsto \many{v}] \textrm{ is the initial local state for $f$}
\end{IEEEeqnarraybox*}
}
\end{prooftree}
\end{minipage}

\begin{prooftree}
\AxiomC{$\delta_c(f,s_c,w) = \tuple{r,s_c',w'}$}
\AxiomC{$\id{cst}' = \id{cst}[c \mapsto s_c']$}
\LeftLabel{\rulename{[e-comp-call]}}
\BinaryInfC{$
  c.f(),\id{pst},\id{cst},w
  \evaluate
  r,\id{pst},\id{cst}',w'
$}
\end{prooftree}

\caption{Excerpt of the big-step standard semantics ($\evaluate$) for expressions}
\label{fig:secaStandardEvaluate}
\end{figure*}
}
\newcommand{\execfigure}{
\begin{figure}

%\begin{minipage}{0.45\textwidth}
\begin{prooftree}
\AxiomC{}
\UnaryInfC{$\id{pst},\id{cst},w,[] \execute \id{pst},\id{cst},w
$}
\end{prooftree}
%\end{minipage}%
%\begin{minipage}{0.45\textwidth}
\begin{prooftree}
\AxiomC{
  \begin{IEEEeqnarraybox*}{c}
%
    s,\id{pst},\id{cst},w,\many{\id{pc}}
    \step
    \id{pst}',\id{cst}',w',\many{\id{pc}}' \\
%
    \id{pst}',\id{cst}',w',\many{\id{pc}}'
    \execute
    \id{pst}'',\id{cst}'',w''
%
  \end{IEEEeqnarraybox*}
}
\UnaryInfC{$
  \id{pst},\id{cst},w,(s:\many{\id{pc}})
  \execute
  \id{pst}'',\id{cst}'',w''
  $}
\end{prooftree}
%\end{minipage}
\caption{Big-step standard semantics ($\execute$) for statement lists}
\label{fig:secaStandardExecute}
\end{figure}
}

\stepfigure
\evalfigure
\execfigure

\medskip\noindent\textbf{The standard step semantics.}
The rules for the \emph{step} relation have judgements of the following form:
\begin{IEEEeqnarray*}{c}
\id{Stmt},\id{PState}^2,\id{CState},\id{World},\many{\id{Stmt}} \step \\
\id{PState}^2,\id{CState},\id{World},\many{\id{Stmt}}
\end{IEEEeqnarray*}

The first $\id{Stmt}$ in the relation is a statement as defined in \cref{fig:secaSyntax}.
It is the current statement to be executed.
The two PStates are program states, one for local variables and one for global variables.
%Local variables have function scope, and include the function's actual parameters and variables created anywhere in the function.
The CState is the component state, keeping track of all present hardware components.
The World is needed because component calls can ask for user input, and perform other kinds of I/O.
Only the rule \rulename{[e-comp-call]} makes use of it; all other rules just pass the World on unchanged.
Finally, there is a list of statements $\many{\id{Stmt}}$, which acts as the program counter.
The program counter contains all statements to be executed after the current one.

%The semantics essentially implements depth-first traversal of abstract syntax trees, where the program counter is a stack representing the traversal state.
%We track the traversal state explicitly, so that control flow operations like \emph{while} and \emph{return} can modify it.
We explain \cref{fig:secaStandardStep} below.  The rule names are prefixed with an \emph{s}, which stands for \emph{step}.
In all figures, $[]$ denotes the empty list and $(s:\many{\id{ss}})$ denotes the list with at least one element $s$ and rest $\many{\id{ss}}$.
Concatenation of lists is denoted with $(\mdoubleplus)$.



\begin{description}
\item{\rulename{[s-assign]}}
The statement $\id{x} = \id{e}$ evaluates expression $\id{e}$ in the current $\id{PState}^2$ $\id{pst}$ to a value $v$ and updates $\id{pst}$ according to the scoping rules.

\item{\rulename{[s-if-true]}, \rulename{[s-if-false]}}
The rules for conditionals first evaluate the condition expression $e$, and depending on the outcome prepend either the then-branch or the else-branch to the program counter.

\item{\rulename{[s-while-true]}}
If the condition $e$ evaluates to \seca{true}, this rule prepends the loop body $\many{s_1}$ and the loop itself to the program counter.
It also increments the iteration count $i$.

\item{\rulename{[s-while-false]}}
If the condition evaluates to \seca{false}, this rule leaves the program counter unchanged, causing the loop to be skipped and the rest of the program to be executed.

\item{\rulename{[s-return]}}
This rule has two effects.
First, it evaluates $e$ and assigns the result $v$ to a special variable %named
$\id{\#return}$ in the current local PState.
The calling function can use $\id{\#return}$ to access the returned value if required.
Second, the return statement discards the program counter and returns the empty list.
This stops execution of the function.

\item{\rulename{[s-expr]}}
This rule lifts evaluation of expressions into execution of statements.
The result value $v$ is discarded, only the result states are relevant.
\end{description}

\medskip\noindent\textbf{The standard evaluate semantics.}
The semantics for expressions, called \emph{evaluate} ($\evaluate$), is a big-step semantics, defined by the rules in \cref{fig:secaStandardEvaluate}, and some additional ones to support the remaining operators.
It has judgements of the %following
form:
\begin{IEEEeqnarray*}{c}
\id{Expr},\id{PState}^2,\id{CState},\id{World} \evaluate \id{Val},\id{PState}^2,\id{CState},\id{World}
\end{IEEEeqnarray*}
Expr is the current expression to be evaluated.
The other arguments are the same as in the step case, and are needed for function and component calls.
Most rules of ($\evaluate$) are %pretty
straight-forward, so we discuss only a couple of them here.
The rule names are prefixed with an \emph{e}, which stands for \emph{evaluate}.


\begin{description}
\item[\rulename{[e-const]}]
As usual for big-step semantics, there is an axiom for constants.
In our case constants $v$ are numbers and Boolean values.

\item[\rulename{[e-var]}]
Variables are evaluated by looking up their value according to the scoping rules, following \cref{def:scopingRules}.

\item[\rulename{[e-add]}]
Arithmetic addition is evaluated by first evaluating the argument expressions and then calculating the result value.
Similar rules exist for the other operators.

\item[\rulename{[e-compare-true]}]
Comparisons are evaluated by evaluating %the argument expressions,
both sides,
and yielding \seca{true} if the left-hand side is less than or equal to the right-hand side.
Similar rules exist for when the comparison is \seca{false}, and for \seca{<} and \seca{==}.

\item[\rulename{[e-func-call]}]
This rule handles function calls $f(\many{e})$.
It does two things.
First, it evaluates all actual parameter expressions $\many{e}$, in sequence from left to right, while threading the state through these evaluations.
This operation is denoted by $(\seqevaluate)$.
The result of evaluating the parameters is a list of values $\many{v}$, a new component state $\id{cst}'$, and new local and global PStates $\id{lst}'$ and $\id{gst}'$.

Second, it performs the actual function call.
We assume that a function definition with name $f$ exists, with formal parameters $\many{x}$ and body $\many{s}$.
Let $\id{fst}$ be a new local PState for $f$, where each variable $x$ is initialized with the corresponding %actual argument
value $v$.
The body $\many{s}$ is then executed in $\id{fst}$, with global CState $\id{cst}'$ and global PState $\id{gst}'$.
This execution ends when all statements of $\many{s}$ have been processed, resulting in a new CState $\id{cst}''$ and PStates $\id{fst}'$ and $\id{gst}''$.
The resulting local state $\id{fst}'$ is discarded.

The rule finally returns the global CState $\id{cst}''$ and PState $\id{gst}''$ resulting from the function call, and the local PState $\id{lst}'$ and program counter $\many{\id{pc}}$ from  before the function call.

When execution of the function body terminates, we know that the return register $\id{\#return}$ in the function's local PState holds the function's return value.

\item[\rulename{[e-comp-call]}]
A component call \seca{c.f()} causes component $c$ to make an $f$-transition.
The current state of $c$ is $s_c$.
The transition $\delta_c(f, s_c, w)$ results in a return value $r$, a new state $s_c'$, and an updated world $w'$.
The new CState $\id{cst}'$ is $\id{cst}$ where $c$ is now in state $s_c'$.
%We assume that the semantics knows what kind of component $c$ is to use the right $\delta_c$.
\end{description}

\medskip\noindent\textbf{The standard execute semantics.}
Finally, execution of statement lists ($\execute$) is defined as the repeated application of ($\step$) until the program counter is empty.
%When the program counter is empty,
Once this occurs,
($\execute$) can no longer make a step, and execution of the statements is done.
Formally: ($\execute$) is the smallest relation closed under the rules in \cref{fig:secaStandardExecute}.
It has judgements of the following form:
\begin{IEEEeqnarray*}{c}
\id{PState}^2,\id{CState},\id{World},\many{\id{Stmt}} \execute \id{PState}^2,\id{CState},\id{World}
\end{IEEEeqnarray*}

Execution of complete programs combines all relations seen so far: ($\step$), ($\evaluate$), and ($\execute$).
To execute a complete SECA program, we need the following ingredients.
\begin{itemize}[noitemsep]
\item A list of function definitions, one of them \seca{int main()}.
\item A list of global variable definitions.
\item An initial CState containing all components mentioned in the program, beginning in their start state.
\end{itemize}
The program is executed by first initializing the global variables, and then running the single statement \emph{main()} in this global PState, an empty local PState, and the initial CState.

Global variables can only be initialized with constant expressions, which may refer only to global variables defined earlier.
%Initializers of global variables
Their initializers are evaluated with a variation of ($\evaluate$) that does not support function or component calls.

terminal :: Component
terminal = Component
  { compState = ()
  , compPowerDraw = \_ -> 0
  , compTransition = \_ -> \_ -> do
      putStr "number? "
      hFlush stdout
      val <- (read <$> getLine :: IO Int)
      return (val,())
  }
